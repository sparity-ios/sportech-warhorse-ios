//
//  SPPromotionViewController.h
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Communication.h"
#import "SPPromotionTableCell.h"
@interface SPPromotionViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CommunicatonDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    
    IBOutlet SPPromotionTableCell *customTableViewCell;
}
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (strong,nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UIToolbar *pickerToolBar;
@property (strong, nonatomic) IBOutlet SPPromotionTableCell *customTableViewCell;
@property (strong, nonatomic) IBOutlet UIImageView *headerImageView;
@property (nonatomic,retain) NSMutableArray *imageArray;
- (IBAction)toggleOnClick:(id)sender;
- (IBAction)calenderOnClick:(id)sender;

@end
