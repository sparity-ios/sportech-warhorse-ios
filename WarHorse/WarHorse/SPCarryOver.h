//
//  SPCarryOver.h
//  WarHorse
//
//  Created by Ramya on 8/21/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SPCarryOver : NSObject

@property (strong, nonatomic) NSString *raceTrackTitle;
@property (strong, nonatomic) NSString *trackpriceLabel;
@property (strong, nonatomic) UIColor *mtpBgColor;
@property (strong, nonatomic) NSString *date;

@end
