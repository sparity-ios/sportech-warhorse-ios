//
//  SPCarryOverCustomCell.m
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "SPCarryOverCustomCell.h"
#import "SPCarryOver.h"

@implementation SPCarryOverCustomCell
@synthesize parkTitle;
@synthesize priceLabel;
@synthesize dateLbel;
@synthesize imageView;
@synthesize carryOver;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    
    }
    return self;
}

- (void) awakeFromNib
{
  
}

- (void) updateView
{
    NSLog(@"Carry Over Object is %@", carryOver);
    
    [self.contentView setBackgroundColor:self.carryOver.mtpBgColor];
    
    [self.parkTitle setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.parkTitle setTextColor:[UIColor colorWithRed:4.0/255.0f green:4.0/255.0f blue:4.0f/255.0f alpha:1.0]];
    self.parkTitle.text = self.carryOver.raceTrackTitle;
    [self.parkTitle setTextAlignment:NSTextAlignmentLeft];
    
    CGSize maximumparkTitleSize = CGSizeMake(299,9999);
    
    CGSize expectedparkTitleSize = [self.parkTitle.text sizeWithFont:self.parkTitle.font constrainedToSize:maximumparkTitleSize lineBreakMode:self.parkTitle.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect parkTitleNewFrame = self.parkTitle.frame;
    parkTitleNewFrame.size.height = expectedparkTitleSize.height;
    self.parkTitle.frame = parkTitleNewFrame;
    
    [self.priceLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:17]];
    [self.priceLabel setTextColor:[UIColor colorWithRed:4.0/255.0f green:4.0/255.0f blue:4.0f/255.0f alpha:1.0]];
    self.priceLabel.text=self.carryOver.trackpriceLabel;
    [self.priceLabel setTextAlignment:NSTextAlignmentRight];

    CGSize maximumpriceLabelSize = CGSizeMake(100,30);
    
    CGSize expectedpriceLabelSize = [self.priceLabel.text sizeWithFont:self.priceLabel.font constrainedToSize:maximumpriceLabelSize lineBreakMode:self.priceLabel.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect priceLabelNewFrame = self.priceLabel.frame;
    priceLabelNewFrame.size.height = expectedpriceLabelSize.height;
    self.priceLabel.frame = priceLabelNewFrame;
    
    [self.dateLbel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.dateLbel setTextColor:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0]];
    
    self.dateLbel.text=self.carryOver.date;
    
    CGSize maximumdateLbelSize = CGSizeMake(299,9999);
    
    CGSize expecteddateLbelSize = [self.dateLbel.text sizeWithFont:self.dateLbel.font constrainedToSize:maximumdateLbelSize lineBreakMode:self.dateLbel.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect dateLbelNewFrame = self.dateLbel.frame;
    dateLbelNewFrame.size.height = expecteddateLbelSize.height;
    self.dateLbel.frame = dateLbelNewFrame;
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
