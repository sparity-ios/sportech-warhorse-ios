//
//  ZLHomeScreenViewController.m
//  WarHorse
//
//  Created by Sparity on 7/4/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLHomeScreenViewController.h"
#import "ZLLoginViewController.h"

@interface ZLHomeScreenViewController ()

@end

@implementation ZLHomeScreenViewController
@synthesize loginButton=_loginButton;
@synthesize accountButton=_accountButton;
@synthesize accountLabel=_accountLabel;
@synthesize termsConditonsLabel=_termsConditonsLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden=YES;
    [self.accountLabel setText:@"Don,t have an account"];
    [self.accountLabel setTextColor:[UIColor colorWithRed:227.0/255.0f green:227.0/255.0f blue:227.0f/255.0f alpha:1.0]];
    [self.accountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];

    [self.termsConditonsLabel setText:@"By using WarHorse,you agree to our\n Terms  of Use and Privacy Policy"];
    [self.termsConditonsLabel setTextColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0f/255.0f alpha:1.0]];
    [self.termsConditonsLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
   
}


-(IBAction)login_Clicked:(id)sender{
    ZLLoginViewController *loginScreenViewCOntroller=[[ZLLoginViewController alloc]init];
    [self.navigationController pushViewController:loginScreenViewCOntroller animated:YES];
    loginScreenViewCOntroller=nil;
    
}



-(IBAction)account_Clicked:(id)sender
{
    
}


-(void)viewDidUnload
{
    self.loginButton=nil;
    self.accountButton=nil;
    self.accountLabel=nil;
    self.termsConditonsLabel=nil;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
