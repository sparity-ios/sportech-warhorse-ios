//
//  ZLMainScreenViewController.m
//  WarHorse
//
//  Created by Sparity on 7/19/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLMainScreenViewController.h"
#import "ZLHomeScreenViewController.h"
#import "ZLLoginViewController.h"
#import "AsyncImageView.h"
#import "ZLRegisterViewController.h"
#import "SPPromotionViewController.h"
#import "SPCarryOversViewController.h"
@interface ZLMainScreenViewController ()

@end

@implementation ZLMainScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBarHidden=YES;
    
    [self.mainScrollView setContentSize:CGSizeMake(320, 572)];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
    
    [self.collectionView registerClass:[ZLMainScreenCollectionCell class] forCellWithReuseIdentifier:@"MY_CELL"];

    
    _imagesArray=[[NSMutableArray alloc]init];
    
    self.collectionArray = [[NSMutableArray alloc] init];
    [self loadData];
    
    [self.imageScrollView setContentSize:CGSizeMake(self.imagesArray.count * self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
    [self.imageScrollView setPagingEnabled:YES];
    [self.imageScrollView setShowsHorizontalScrollIndicator:NO];
    [self.imageScrollView setDelegate:self];
    
    
    [self.pageControl setNumberOfPages:self.imagesArray.count];
	[self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    
    for (int i = 0; i < self.imagesArray.count; i++)
    {
        NSMutableDictionary *dic = [self.imagesArray objectAtIndex:i];
        NSString* imageURL =[NSString stringWithFormat:@"%@",[dic valueForKey:@"pageControlImage"]];
        
        NSURL *URL=[NSURL URLWithString:imageURL];
        
        AsyncImageView *_asyncImageView=[[AsyncImageView alloc]initWithFrame:CGRectMake(i * self.imageScrollView.frame.size.width, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
        _asyncImageView.y =_asyncImageView.frame.size.height/2;
		_asyncImageView.x =_asyncImageView.frame.size.width/2;
        [_asyncImageView loadImageFromURL:URL];
        [self.imageScrollView addSubview:_asyncImageView];
        _asyncImageView=nil;
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.imageTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerMethod:) userInfo:nil repeats:YES];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    if ([self.imageTimer isValid]) {
        [self.imageTimer invalidate];
        self.imageTimer = nil;
    }
}

- (void) timerMethod:(NSTimer *)timer
{
    int whichPage = self.pageControl.currentPage;
    
    whichPage ++;
    if (whichPage >= self.pageControl.numberOfPages) {
        whichPage = 0;
    }
    self.pageControl.currentPage = whichPage;
    [self.imageScrollView setContentOffset:CGPointMake(whichPage * self.imageScrollView.frame.size.width, 0)];
}

-(void)loadData
{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"Schedules" forKey:@"title"];
    [dic setValue:@"schdeules.png" forKey:@"icon"];
    [dic setObject:[UIColor colorWithRed:237.0/255 green:106.0/255 blue:62.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.collectionArray addObject:dic];
    
    NSMutableDictionary *dic2 = [NSMutableDictionary dictionary];
    [dic2 setValue:@"Carry Overs" forKey:@"title"];
    [dic2 setValue:@"carryovers.png" forKey:@"icon"];
    [dic2 setObject:[UIColor colorWithRed:69.0/255 green:138.0/255 blue:242.0/255 alpha:1.0] forKey:@"backgroundColor"];

    [self.collectionArray addObject:dic2];
    
    NSMutableDictionary *dic1 = [NSMutableDictionary dictionary];
    [dic1 setValue:@"SCR & Changes" forKey:@"title"];
    [dic1 setValue:@"scr&changes.png" forKey:@"icon"];
    [dic1 setObject:[UIColor colorWithRed:113.0/255 green:171.0/255 blue:1.0/255 alpha:1.0] forKey:@"backgroundColor"];

    [self.collectionArray addObject:dic1];
    
    
    NSMutableDictionary *dic3 = [NSMutableDictionary dictionary];
    [dic3 setValue:@"Promotions" forKey:@"title"];
    [dic3 setValue:@"promotions.png" forKey:@"icon"];
    [dic3 setObject:[UIColor colorWithRed:178.0/255 green:0.0/255 blue:36.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.collectionArray addObject:dic3];
    
    NSMutableDictionary *dic4 = [NSMutableDictionary dictionary];
    [dic4 setValue:@"Help/Support" forKey:@"title"];
    [dic4 setValue:@"help&support.png" forKey:@"icon"];
    [dic4 setObject:[UIColor colorWithRed:164.0/255 green:99.0/255 blue:165.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.collectionArray addObject:dic4];
    
    
    NSMutableDictionary *dic5 = [NSMutableDictionary dictionary];
    [dic5 setValue:@"Get Started" forKey:@"title"];
    [dic5 setValue:@"getstarted.png" forKey:@"icon"];
    [dic5 setObject:[UIColor colorWithRed:56.0/255 green:88.0/255 blue:149.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.collectionArray addObject:dic5];
    
    
    
    self.tableArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *tabledic1 = [NSMutableDictionary dictionary];
    [tabledic1 setValue:@"Selections" forKey:@"title"];
    [tabledic1 setValue:@"selections.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic1];
    
    NSMutableDictionary *tabledic2 = [NSMutableDictionary dictionary];
    [tabledic2 setValue:@"Feature Races" forKey:@"title"];
    [tabledic2 setValue:@"featureraces.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic2];
    
    NSMutableDictionary *tabledic3 = [NSMutableDictionary dictionary];
    [tabledic3 setValue:@"Reward Structure" forKey:@"title"];
    [tabledic3 setValue:@"rewardstructure.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic3];
    
    NSMutableDictionary *tabledic4 = [NSMutableDictionary dictionary];
    [tabledic4 setValue:@"Online Tip Sheet" forKey:@"title"];
    [tabledic4 setValue:@"onlinetipsheet.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic4];
    
    NSMutableDictionary *tabledic5 = [NSMutableDictionary dictionary];
    [tabledic5 setValue:@"About WarHorse" forKey:@"title"];
    [tabledic5 setValue:@"aboutwarhorse.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic5];
    
    NSMutableDictionary *tabledic6 = [NSMutableDictionary dictionary];
    [tabledic6 setValue:@"Terms/Conditions" forKey:@"title"];
    [tabledic6 setValue:@"terms&conditions.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic6];
    
    
    
    
    NSMutableDictionary *imageDic=[NSMutableDictionary dictionary];
    [imageDic setValue:@"https://dev.sparity.com/devphp/6.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic];
    
    NSMutableDictionary *imageDic1=[NSMutableDictionary dictionary];
    [imageDic1 setValue:@"https://dev.sparity.com/devphp/7.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic1];
    
    
    NSMutableDictionary *imageDic2=[NSMutableDictionary dictionary];
    [imageDic2 setValue:@"https://dev.sparity.com/devphp/8.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic2];
    
    NSMutableDictionary *imageDic3=[NSMutableDictionary dictionary];
    [imageDic3 setValue:@"hhttps://dev.sparity.com/devphp/11.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic3];
    
    NSMutableDictionary *imageDic4=[NSMutableDictionary dictionary];
    [imageDic4 setValue:@"https://dev.sparity.com/devphp/10.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic4];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.imageScrollView)
    {
        CGPoint point = scrollView.contentOffset;
        self.pageControl.currentPage = point.x/scrollView.frame.size.width;
    }
}


-(void)pageTurn:(UIPageControl *)aPageControl
{
	int WhichPage = aPageControl.currentPage;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.5];
	[self.imageScrollView setContentOffset:CGPointMake(WhichPage *self.imageScrollView.frame.size.width, 0)];
	[UIView commitAnimations];
}


-(IBAction)loginClicked:(id)sender{
    ZLLoginViewController *loginScreenViewCOntroller=[[ZLLoginViewController alloc]init];
    [self.navigationController pushViewController:loginScreenViewCOntroller animated:YES];
    loginScreenViewCOntroller=nil;

}
-(IBAction)accountCLicked:(id)sender{
    ZLRegisterViewController *registerViewController=[[ZLRegisterViewController alloc]init];
    [self.navigationController pushViewController:registerViewController animated:YES];
}


#pragma mark -
#pragma mark UICollectionViewDelegate Methods

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [self.collectionArray count];
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZLMainScreenCollectionCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    NSMutableDictionary *dic = [self.collectionArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = [dic valueForKey:@"title"];
    cell.iconImageView.image = [UIImage imageNamed:[dic valueForKey:@"icon"]];
    cell.backgroundColor = [dic valueForKey:@"backgroundColor"];
    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(151,37);
//}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==1){
        SPCarryOversViewController *carryOver = [[SPCarryOversViewController alloc]init];
        [self.navigationController pushViewController:carryOver animated:YES];
        
    }
    if (indexPath.row == 3) {
     SPPromotionViewController *promotion = [[SPPromotionViewController alloc]init];
    
    [self.navigationController pushViewController:promotion animated:YES];
    }
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.tableArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.objTableViewCell  = (ZLMainScreenTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.objTableViewCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLMainScreenTableViewCell" owner:self options:nil];
    }
    
    NSMutableDictionary *dic = [self.tableArray objectAtIndex:indexPath.row];
    
    [self.objTableViewCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.objTableViewCell.titleLabel setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
    self.objTableViewCell.titleLabel.text=[dic valueForKey:@"title"];
    self.objTableViewCell.iconImages.image=[UIImage imageNamed:[dic valueForKey:@"iconImages"]];
    
    return self.objTableViewCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


-(void)viewDidUnload{
    self.accountButton=nil;
    self.loginButton=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
