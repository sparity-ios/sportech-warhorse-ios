//
//  ZLMainGridViewController.m
//  WarHorse
//
//  Created by Sparity on 7/5/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLMainGridViewController.h"
#import "ZLMainTileCell.h"
#import "AsyncImageView.h"
#import "ZLWagerViewController.h"
#import "ZLLeftSideMenuViewController.h"
#import "RevealController.h"
#import "ZLCurrentBetTypeViewController.h"
#import "ZLWalletViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ZLResultsViewController.h"
#import "ZLQRCodeViewController.h"


#define NO_OF_ROWS 2
#define NO_OF_COLOMS 2
#define NOPAGES 3


@interface ZLMainGridViewController ()

@end

@implementation ZLMainGridViewController
@synthesize mainGridCollectionView=_mainGridCollectionView;
@synthesize numberArray=_numberArray;
@synthesize mainScrollView=_mainScrollView;
@synthesize mainTableView=_mainTableView;
@synthesize mainTableCustomCell=_mainTableCustomCell;
@synthesize tableArray=_tableArray;
@synthesize pageControl=_pageControl;
@synthesize imageScrollView=_imageScrollView;
@synthesize imagesArray=_imagesArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadView:)
                                                     name:@"LoadView"
                                                   object:nil];
    }
    return self;
}

- (void) viewDidUnload
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.mainScrollView=nil;
    self.mainGridCollectionView=nil;
    self.mainTableView=nil;
    self.pageView=nil;
    self.pageControl=nil;
    self.imageScrollView=nil;
    self.imagesArray=nil;
    self.numberArray=nil;
    self.mainTableCustomCell=nil;
    self.tableArray=nil;
    self.viewController=nil;
    
}


- (void) loadView:(NSNotification *) notification
{
    [self.navigationController popToViewController:self animated:NO];
    NSNumber *viewNumber = [notification.userInfo objectForKey:@"viewNumber"];
    [self pushToView:[viewNumber integerValue]];
    viewNumber=nil;
}

- (void) pushToView:(DashBoard)viewNumber
{
    switch (viewNumber)
    {
        case DashBoardHome:
            
            break;
        case DashBoardWager:
            
            [self loadWagerView];
            
            break;
        case DashBoardAlerts:
            
            break;
        case DashBoardMyBets:
            
            [self loadCurrentBetsView];
            
            break;
        case DashBoardWallet:
            [self loadWalletView];
            break;
        case DashBoardOddsBoard:
            
            break;
        case DashBoardQRCode:
            [self loadQRCodeScreen];
            break;
        case DashBoardVideoReplay:
            
            break;
        case DashBoardResults:
            [self loadResultsScreen];
            break;
        case DashBoardLogOut:
            [self.navigationController popToRootViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}

-(void)loadQRCodeScreen{
    ZLQRCodeViewController *qrCodeViewController=[[ZLQRCodeViewController alloc]init];
    ZLLeftSideMenuViewController *wagerLeftViewController=[[ZLLeftSideMenuViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:qrCodeViewController];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:wagerLeftViewController];
    self.viewController = revealController;
    [self.navigationController pushViewController:self.viewController animated:YES];
    
    qrCodeViewController=nil;
    wagerLeftViewController=nil;
    navigationController=nil;
    revealController=nil;
    

}





- (void) loadWagerView
{
    ZLWagerViewController *wagerViewController=[[ZLWagerViewController alloc]init];
    ZLLeftSideMenuViewController *wagerLeftViewController=[[ZLLeftSideMenuViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:wagerViewController];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:wagerLeftViewController];
    self.viewController = revealController;
    [self.navigationController pushViewController:self.viewController animated:YES];
    
    wagerViewController=nil;
    wagerLeftViewController=nil;
    navigationController=nil;
    revealController=nil;
    
}

- (void) loadCurrentBetsView
{
    ZLCurrentBetTypeViewController *objCurrentBets = [[ZLCurrentBetTypeViewController alloc] init];
    ZLLeftSideMenuViewController *wagerLeftViewController=[[ZLLeftSideMenuViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objCurrentBets];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:wagerLeftViewController];
    self.viewController = revealController;
    [self.navigationController pushViewController:self.viewController animated:YES];
    
    
    objCurrentBets=nil;
    wagerLeftViewController=nil;
    navigationController=nil;
    revealController=nil;
    
}

- (void) loadWalletView
{
    ZLWalletViewController *objWallet = [[ZLWalletViewController alloc] init];
    ZLLeftSideMenuViewController *wagerLeftViewController=[[ZLLeftSideMenuViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:objWallet];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:wagerLeftViewController];
    self.viewController = revealController;
    [self.navigationController pushViewController:self.viewController animated:YES];
    
    objWallet=nil;
    wagerLeftViewController=nil;
    navigationController=nil;
    revealController=nil;

}

- (void) loadResultsScreen
{
    ZLResultsViewController *resultView=[[ZLResultsViewController alloc]init];
    ZLLeftSideMenuViewController *wagerLeftViewController=[[ZLLeftSideMenuViewController alloc]init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:resultView];
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:wagerLeftViewController];
    self.viewController = revealController;
    [self.navigationController pushViewController:self.viewController animated:YES];
    
    resultView=nil;
    wagerLeftViewController=nil;
    navigationController=nil;
    revealController=nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    
    [self.mainScrollView setContentSize:CGSizeMake(320, 560)];
    [self.mainGridCollectionView registerClass:[ZLMainTileCell class] forCellWithReuseIdentifier:@"MY_CELL"];
    _numberArray=[[NSMutableArray alloc]init];
    
    [self loadData];
    
  

    for (int i = 0; i < self.imagesArray.count; i++)
    {
        NSMutableDictionary *dic = [self.imagesArray objectAtIndex:i];
        NSString* imageURL =[NSString stringWithFormat:@"%@",[dic valueForKey:@"pageControlImage"]];
       
        NSURL *URL=[NSURL URLWithString:imageURL];

        AsyncImageView *_asyncImageView=[[AsyncImageView alloc]initWithFrame:CGRectMake(i * self.imageScrollView.frame.size.width, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
        _asyncImageView.y =_asyncImageView.frame.size.height/2;
		_asyncImageView.x =_asyncImageView.frame.size.width/2;
        [_asyncImageView loadImageFromURL:URL];
        [self.imageScrollView addSubview:_asyncImageView];
        _asyncImageView=nil;
    }

    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.imageScrollView.frame.origin.y + self.imageScrollView.frame.size.height - 24, self.imageScrollView.frame.size.width, 36)];
    self.pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:31.0/256 green:111.0/256 blue:137.0/256 alpha:1.0];
    self.pageControl.numberOfPages = self.imagesArray.count;
    self.pageControl.currentPage = 0;
    [self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    [self.pageView addSubview:self.pageControl];
}



-(void)loadData
{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"Wager" forKey:@"title"];
    [dic setValue:@"1.png" forKey:@"icon"];
    //[dic setObject:nil forKey:@"badgeNumber"];
    [dic setObject:[UIColor colorWithRed:69.0/255 green:138.0/255 blue:242.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.numberArray addObject:dic];
    
    
    NSMutableDictionary *dic1 = [NSMutableDictionary dictionary];
    [dic1 setValue:@"Alert" forKey:@"title"];
    [dic1 setValue:@"2.png" forKey:@"icon"];
    [dic1 setValue:@"4" forKey:@"badgeNumber"];
    [dic1 setObject:[UIColor colorWithRed:113.0/255 green:170.0/255 blue:0.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.numberArray addObject:dic1];
    
    NSMutableDictionary *dic2 = [NSMutableDictionary dictionary];
    [dic2 setValue:@"My Bets" forKey:@"title"];
    [dic2 setValue:@"3.png" forKey:@"icon"];
    [dic2 setValue:@"3" forKey:@"badgeNumber"];
    [dic2 setObject:[UIColor colorWithRed:237.0/255 green:106.0/255 blue:62.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.numberArray addObject:dic2];
    
    
    NSMutableDictionary *dic3 = [NSMutableDictionary dictionary];
    [dic3 setValue:@"Wallet" forKey:@"title"];
    [dic3 setValue:@"4.png" forKey:@"icon"];
    [dic3 setValue:@"Balance: $4320" forKey:@"badgeNumber"];
    [dic3 setObject:[UIColor colorWithRed:164.0/255 green:99.0/255 blue:165.0/255 alpha:1.0] forKey:@"backgroundColor"];
    [self.numberArray addObject:dic3];
    
    
    _tableArray=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *tabledic3 = [NSMutableDictionary dictionary];
    [tabledic3 setValue:@"Live videos/Replays" forKey:@"title"];
    [tabledic3 setValue:@"video.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic3];
    
    NSMutableDictionary *tabledic2 = [NSMutableDictionary dictionary];
    [tabledic2 setValue:@"QR Code" forKey:@"title"];
    [tabledic2 setValue:@"qrcode.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic2];
    
    NSMutableDictionary *tabledic4 = [NSMutableDictionary dictionary];
    [tabledic4 setValue:@"Results/PayOffs" forKey:@"title"];
    [tabledic4 setValue:@"win.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic4];
    
    NSMutableDictionary *tabledic1 = [NSMutableDictionary dictionary];
    [tabledic1 setValue:@"Odds Board" forKey:@"title"];
    [tabledic1 setValue:@"odds.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic1];
    
    NSMutableDictionary *tabledic5 = [NSMutableDictionary dictionary];
    [tabledic5 setValue:@"Settings" forKey:@"title"];
    [tabledic5 setValue:@"settings_dasboard.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic5];
    
    NSMutableDictionary *tabledic6 = [NSMutableDictionary dictionary];
    [tabledic6 setValue:@"Logout" forKey:@"title"];
    [tabledic6 setValue:@"logout_dasboard.png" forKey:@"iconImages"];
    [self.tableArray addObject:tabledic6];
    
    
    _imagesArray=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *imageDic=[NSMutableDictionary dictionary];
    [imageDic setValue:@"https://dev.sparity.com/devphp/6.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic];
    
    NSMutableDictionary *imageDic1=[NSMutableDictionary dictionary];
    [imageDic1 setValue:@"https://dev.sparity.com/devphp/2.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic1];
    
    
    NSMutableDictionary *imageDic2=[NSMutableDictionary dictionary];
    [imageDic2 setValue:@"https://dev.sparity.com/devphp/3.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic2];
    
    NSMutableDictionary *imageDic3=[NSMutableDictionary dictionary];
    [imageDic3 setValue:@"https://dev.sparity.com/devphp/4.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic3];
    
    NSMutableDictionary *imageDic4=[NSMutableDictionary dictionary];
    [imageDic4 setValue:@"https://dev.sparity.com/devphp/5.png" forKey:@"pageControlImage"];
    [self.imagesArray addObject:imageDic4];
    
    [self.imageScrollView setContentSize:CGSizeMake(self.imagesArray.count * self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
    [self.imageScrollView setPagingEnabled:YES];
    [self.imageScrollView setShowsHorizontalScrollIndicator:NO];
    [self.imageScrollView setDelegate:self];
    
    
    [self.pageControl setNumberOfPages:self.imagesArray.count];
	[self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.imageTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timerMethod:) userInfo:nil repeats:YES];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    if ([self.imageTimer isValid]) {
        [self.imageTimer invalidate];
        self.imageTimer = nil;
    }
}

- (void) timerMethod:(NSTimer *)timer
{
    int whichPage = self.pageControl.currentPage;
    
    whichPage ++;
    if (whichPage >= self.pageControl.numberOfPages) {
        whichPage = 0;
    }
    self.pageControl.currentPage = whichPage;
    [self.imageScrollView setContentOffset:CGPointMake(whichPage * self.imageScrollView.frame.size.width, 0)];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.imageScrollView)
    {
        CGPoint point = scrollView.contentOffset;
        self.pageControl.currentPage = point.x/scrollView.frame.size.width;
    }	
}


-(void)pageTurn:(UIPageControl *)aPageControl{
	int WhichPage = aPageControl.currentPage;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	[UIView setAnimationDuration:0.5];
	[self.imageScrollView setContentOffset:CGPointMake(WhichPage *self.imageScrollView.frame.size.width, 0)];
	[UIView commitAnimations];
}


#pragma mark -
#pragma mark UICollectionViewDelegate Methods

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return 4;
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZLMainTileCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    NSMutableDictionary *dic = [self.numberArray objectAtIndex:indexPath.row];
    cell.iconImageView.image=[UIImage imageNamed:[dic valueForKey:@"icon"]];
    cell.backgroundColor = [dic objectForKey:@"backgroundColor"];
    cell.titleLabel.text = [dic valueForKey:@"title"];
    NSString *badgeNumber = [dic valueForKey:@"badgeNumber"];
    
    if (badgeNumber)
    {
        [cell.badgeButton setTitle:badgeNumber forState:UIControlStateNormal];
    }
    else
    {
        cell.badgeButton.hidden = YES;
    }
    
    cell.badgeButton.userInteractionEnabled = NO;

    if ([cell.titleLabel.text isEqualToString:@"Wallet"])
    {
        cell.badgeButton.frame = CGRectMake(cell.badgeButton.frame.origin.x - 100, cell.badgeButton.frame.origin.y, cell.badgeButton.frame.size.width + 100, cell.badgeButton.frame.size.height);

//        [cell.badgeButton addTarget:self action:@selector(dolorButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        cell.badgeButton.userInteractionEnabled = YES;
    }
    return cell;
}

- (void) dolorButtonClicked:(UIButton *)sender
{
    if ([sender isSelected])
    {
        [sender setSelected:NO];
        sender.frame = CGRectMake(sender.frame.origin.x + 100, sender.frame.origin.y, sender.frame.size.width - 100, sender.frame.size.height);
        [sender setTitle:@"$" forState:UIControlStateNormal];

    }
    else
    {
        [sender setSelected:YES];
        sender.frame = CGRectMake(sender.frame.origin.x - 100, sender.frame.origin.y, sender.frame.size.width + 100, sender.frame.size.height);
        [sender setTitle:@"Balance: $4320" forState:UIControlStateNormal];

    }
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake((self.mainGridCollectionView.frame.size.width-20) / NO_OF_COLOMS, (self.mainGridCollectionView.frame.size.height-20) / NO_OF_ROWS);
//}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 0, 0, 0);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
   // ZLMainTileCell *cell = (ZLMainTileCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        [self pushToView:DashBoardWager];
    }
    else if(indexPath.row == 2)
    {
        [self pushToView:DashBoardMyBets];
    }
    else if (indexPath.row == 3)
    {
        [self pushToView:DashBoardWallet];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Title" message:@"Under Progress" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
   
    return [self.tableArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.mainTableCustomCell  = (ZLMainTableCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.mainTableCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLMainTableCustomCell" owner:self options:nil];
        
    }
    
    NSMutableDictionary *dic = [self.tableArray objectAtIndex:indexPath.row];

    [self.mainTableCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.mainTableCustomCell.titleLabel setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
    self.mainTableCustomCell.titleLabel.text=[dic valueForKey:@"title"];
    self.mainTableCustomCell.iconImages.image=[UIImage imageNamed:[dic valueForKey:@"iconImages"]];
    
    return self.mainTableCustomCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        [self pushToView:DashBoardVideoReplay];
    }
    else if (indexPath.row==1)
    {
        [self pushToView:DashBoardQRCode];
    }
    else if (indexPath.row == 2)
    {
        [self pushToView:DashBoardResults];
    }
    else if (indexPath.row == 3)
    {
        [self pushToView:DashBoardOddsBoard];
    }
    else if (indexPath.row == 4)
    {
        [self pushToView:DashBoardSettings];
    }
    else if (indexPath.row == 5)
    {
        [self pushToView:DashBoardLogOut];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
