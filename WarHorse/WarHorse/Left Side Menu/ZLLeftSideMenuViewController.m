//
//  ZLWagerLeftViewController.m
//  WarHorse
//
//  Created by Sparity on 7/5/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLLeftSideMenuViewController.h"

@interface ZLLeftSideMenuViewController ()

@end

@implementation ZLLeftSideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Left_Menu_BG.png"]]];
    
    [self.nameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];

    
    self.itemsArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"Home" forKey:@"title"];
    [dic setObject:[UIImage imageNamed:@"home.png"] forKey:@"logo"];
    [dic setObject:[NSNumber numberWithInt:DashBoardHome] forKey:@"enumValue"];
    [self.itemsArray addObject:dic];
    
    NSMutableDictionary *dic1 = [NSMutableDictionary dictionary];
    [dic1 setValue:@"Wager" forKey:@"title"];
    [dic1 setObject:[UIImage imageNamed:@"wagerLogo.png"] forKey:@"logo"];
    [dic1 setObject:[NSNumber numberWithInt:DashBoardWager] forKey:@"enumValue"];
    [self.itemsArray addObject:dic1];
    
    NSMutableDictionary *dic2 = [NSMutableDictionary dictionary];
    [dic2 setValue:@"My Bets" forKey:@"title"];
    [dic2 setObject:[UIImage imageNamed:@"currentbets.png"] forKey:@"logo"];
    [dic2 setObject:[NSNumber numberWithInt:DashBoardMyBets] forKey:@"enumValue"];
    [self.itemsArray addObject:dic2];
    
    NSMutableDictionary *dic11 = [NSMutableDictionary dictionary];
    [dic11 setValue:@"Alerts" forKey:@"title"];
    [dic11 setObject:[UIImage imageNamed:@"currentbets.png"] forKey:@"logo"];
    [dic11 setObject:[NSNumber numberWithInt:DashBoardAlerts] forKey:@"enumValue"];
    [self.itemsArray addObject:dic11];

    NSMutableDictionary *dic3 = [NSMutableDictionary dictionary];
    [dic3 setValue:@"Wallet" forKey:@"title"];
    [dic3 setObject:[UIImage imageNamed:@"wallet.png"] forKey:@"logo"];
    [dic3 setObject:[NSNumber numberWithInt:DashBoardWallet] forKey:@"enumValue"];
    [self.itemsArray addObject:dic3];
    
    NSMutableDictionary *dic4 = [NSMutableDictionary dictionary];
    [dic4 setValue:@"Videos/Replays" forKey:@"title"];
    [dic4 setObject:[UIImage imageNamed:@"videos.png"] forKey:@"logo"];
    [dic4 setObject:[NSNumber numberWithInt:DashBoardVideoReplay] forKey:@"enumValue"];
    [self.itemsArray addObject:dic4];
    
    NSMutableDictionary *dic6 = [NSMutableDictionary dictionary];
    [dic6 setValue:@"QR Code" forKey:@"title"];
    [dic6 setObject:[UIImage imageNamed:@"leftQRCode.png"] forKey:@"logo"];
    [dic6 setObject:[NSNumber numberWithInt:DashBoardQRCode] forKey:@"enumValue"];
    [self.itemsArray addObject:dic6];
    
    NSMutableDictionary *dic7 = [NSMutableDictionary dictionary];
    [dic7 setValue:@"Results/PayOffs" forKey:@"title"];
    [dic7 setObject:[UIImage imageNamed:@"results.png"] forKey:@"logo"];
    [dic7 setObject:[NSNumber numberWithInt:DashBoardResults] forKey:@"enumValue"];
    [self.itemsArray addObject:dic7];
    
    NSMutableDictionary *dic8 = [NSMutableDictionary dictionary];
    [dic8 setValue:@"Odds Board" forKey:@"title"];
    [dic8 setObject:[UIImage imageNamed:@"oddsboard.png"] forKey:@"logo"];
    [dic8 setObject:[NSNumber numberWithInt:DashBoardOddsBoard] forKey:@"enumValue"];
    [self.itemsArray addObject:dic8];
    
    NSMutableDictionary *dic9 = [NSMutableDictionary dictionary];
    [dic9 setValue:@"Settings" forKey:@"title"];
    [dic9 setObject:[UIImage imageNamed:@"settings.png"] forKey:@"logo"];
    [dic9 setObject:[NSNumber numberWithInt:DashBoardSettings] forKey:@"enumValue"];
    [self.itemsArray addObject:dic9];
    
    NSMutableDictionary *dic10 = [NSMutableDictionary dictionary];
    [dic10 setValue:@"Logout" forKey:@"title"];
    [dic10 setObject:[UIImage imageNamed:@"logout.png"] forKey:@"logo"];
    [dic10 setObject:[NSNumber numberWithInt:DashBoardLogOut] forKey:@"enumValue"];
    [self.itemsArray addObject:dic10];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return self.itemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    self.objCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.objCell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"ZLLeftSideMenuCell" owner:self options:nil];
    }
    
    if (indexPath.row < self.itemsArray.count)
    {
        NSMutableDictionary *dic = [self.itemsArray objectAtIndex:indexPath.row];
        
        [self.objCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
        
        self.objCell.titleLabel.text = [dic valueForKey:@"title"];
        self.objCell.logoImageView.image = [dic objectForKey:@"logo"];
    }
   
    // Configure the cell...
    
    return self.objCell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.itemsArray.count)
    {
        NSMutableDictionary *dic = [self.itemsArray objectAtIndex:indexPath.row];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[dic objectForKey:@"enumValue"] forKey:@"viewNumber"]];
    }
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 37;
}

-(void)viewDidUnload{
    self.itemsArray=nil;
    self.objCell=nil;
    self.nameLabel=nil;
    self.objCell.titleLabel=nil;
    self.objCell.logoImageView=nil;
    
}

@end
