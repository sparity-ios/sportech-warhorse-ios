//
//  ZLResultsRootViewController.m
//  WarHorse
//
//  Created by Sparity on 01/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLResultsRootViewController.h"
#import "ZLResultTrackDetialViewController.h"

@interface ZLResultsRootViewController ()

@end

@implementation ZLResultsRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _resultArray=[[NSMutableArray alloc]init];
    [self loadData];
    
    
    
    self.scrollView.clipsToBounds = NO;
    self.scrollView.pagingEnabled = YES;
	self.scrollView.showsHorizontalScrollIndicator = NO;
	
    CGFloat contentOffset = 0.0f;
    
    for (int i=0;i<3; i++)
    {
        CGRect imageViewFrame = CGRectMake(contentOffset, 0.0f,  _scrollView.frame.size.width - 5,  _scrollView.frame.size.height);
        UILabel *label = [[UILabel alloc] initWithFrame:imageViewFrame];
        [label setText:[NSString stringWithFormat:@"Text %d",i]];
        contentOffset += label.frame.size.width + 5;
		[self.scrollView addSubview:label];
        
        self.scrollView.contentSize = CGSizeMake(contentOffset,  _scrollView.frame.size.height);
	}
}

-(void)loadData
{
    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setValue:@"Ajax Downs" forKey:@"trackName"];
    [dic1 setValue:@"Ajax, Ontario" forKey:@"address"];
    [self.resultArray addObject:dic1];
    
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setValue:@"Beulah Park" forKey:@"trackName"];
    [dic2 setValue:@"Grove City, Ohio" forKey:@"address"];
    [self.resultArray addObject:dic2];
    
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setValue:@"Fairmount Park" forKey:@"trackName"];
    [dic3 setValue:@"Collinsville,illinios" forKey:@"address"];
    [self.resultArray addObject:dic3];
    
    NSMutableDictionary *dic4=[NSMutableDictionary dictionary];
    [dic4 setValue:@"Finger Lakes" forKey:@"trackName"];
    [dic4 setValue:@"Farmington, New York" forKey:@"address"];
    [self.resultArray addObject:dic4];
    
    NSMutableDictionary *dic5=[NSMutableDictionary dictionary];
    [dic5 setValue:@"Fort Erie" forKey:@"trackName"];
    [dic5 setValue:@"Fort Erie, Ontario" forKey:@"address"];
    [self.resultArray addObject:dic5];
    
    NSMutableDictionary *dic6=[NSMutableDictionary dictionary];
    [dic6 setValue:@"Indiana Downs" forKey:@"trackName"];
    [dic6 setValue:@"Shelbyville,Indiana" forKey:@"address"];
    [self.resultArray addObject:dic6];
    
    NSMutableDictionary *dic7=[NSMutableDictionary dictionary];
    [dic7 setValue:@"Mountaineer" forKey:@"trackName"];
    [dic7 setValue:@"Chester, West Virginia" forKey:@"address"];
    [self.resultArray addObject:dic7];
    
    NSMutableDictionary *dic8=[NSMutableDictionary dictionary];
    [dic8 setValue:@"Parx Racing" forKey:@"trackName"];
    [dic8 setValue:@"Bensalem, Pennsylvania" forKey:@"address"];
    [self.resultArray addObject:dic8];
    
    NSMutableDictionary *dic9=[NSMutableDictionary dictionary];
    [dic9 setValue:@"Penn National" forKey:@"trackName"];
    [dic9 setValue:@"Grantville, pennsylvania" forKey:@"address"];
    [self.resultArray addObject:dic9];
    
    
    
    
    
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    //return [self.tableArray count];
    return [self.resultArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.resultCustomCell  = (ZLResultTableCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (self.resultCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLResultTableCustomCell" owner:self options:nil];
        [self.resultCustomCell.trackName_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
        [self.resultCustomCell.address_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
        [self.resultCustomCell.trackName_Label setTextColor:[UIColor colorWithRed:30.0/255 green:30.0/255 blue:30.0/255 alpha:1.0]];
        [self.resultCustomCell.address_Label setTextColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];

    }
    
    NSMutableDictionary *dic = [self.resultArray objectAtIndex:indexPath.row];
    
    self.resultCustomCell.trackName_Label.text=[dic valueForKey:@"trackName"];
    self.resultCustomCell.address_Label.text=[dic valueForKey:@"address"];
    
    return self.resultCustomCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 44.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.row==3) {
    
    static NSString *CellIdentifier = @"Cell";
    self.resultCustomCell  = (ZLResultTableCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    [self.resultCustomCell.contentView setBackgroundColor:[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1.0]];
    ZLResultTrackDetialViewController *trackDetialViewController=[[ZLResultTrackDetialViewController alloc]init];
    [self.navigationController pushViewController:trackDetialViewController animated:YES];
    
    //    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
