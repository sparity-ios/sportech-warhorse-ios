//
//  ZLCalendarScrollView.h
//  WarHorse
//
//  Created by Sparity on 02/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLCalendarScrollView : UIView <UIScrollViewDelegate>

@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDateFormatter *dateFormater;
@property (nonatomic, strong) NSMutableArray *dateLabelsArray;
@property (nonatomic, strong) UIScrollView *scrollView;

@end
