//
//  ZLResultTrackDetialViewController.h
//  WarHorse
//
//  Created by Sparity on 7/31/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLResultDetailCustomCell.h"
#import <MediaPlayer/MediaPlayer.h>


@interface ZLResultTrackDetialViewController : UIViewController
@property(nonatomic,retain) IBOutlet UITableView *raceDetailTableView;
@property(nonatomic,retain) IBOutlet ZLResultDetailCustomCell *resultDetailCustomCell;
@property(nonatomic,retain) NSMutableArray *betTypeArray;
@property(nonatomic,retain) NSMutableArray *wagerArray;
@property(nonatomic,retain) NSMutableArray *orderArray;
@property(nonatomic,retain) NSMutableArray *scratchesArray;
@property(nonatomic,retain) IBOutlet UIScrollView *_scrollView;
@property(nonatomic,retain) NSMutableArray *ColorViews_array;
@property(nonatomic,retain) IBOutlet UIView *backgroundView;
@property(nonatomic,retain) IBOutlet UIButton *raceButton;
@property(nonatomic,retain) IBOutlet UIButton *resultButton;
@property(strong,nonatomic)  MPMoviePlayerController *moviePlayer;


@property(nonatomic,retain)  UIView *trasparentView;

-(IBAction)raceReplayClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
- (IBAction)calendarButtonClicked:(id)sender;
- (IBAction)resultsChartButtonClicked:(id)sender;

@end
