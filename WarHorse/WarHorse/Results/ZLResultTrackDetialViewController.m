//
//  ZLResultTrackDetialViewController.m
//  WarHorse
//
//  Created by Sparity on 7/31/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLResultTrackDetialViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ZLPdfViewController.h"
#import <MediaPlayer/MediaPlayer.h>


@interface ZLResultTrackDetialViewController ()

@end

@implementation ZLResultTrackDetialViewController
@synthesize raceDetailTableView=_raceDetailTableView;
@synthesize betTypeArray=_betTypeArray;
@synthesize wagerArray=_wagerArray;
@synthesize orderArray=_orderArray;
@synthesize scratchesArray=_scratchesArray;
@synthesize ColorViews_array=_ColorViews_array;
@synthesize _scrollView;
@synthesize backgroundView=_backgroundView;
@synthesize moviePlayer=_moviePlayer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _betTypeArray=[[NSMutableArray alloc]init];
    _wagerArray=[[NSMutableArray alloc]init];
    _orderArray=[[NSMutableArray alloc]init];
    _scratchesArray=[[NSMutableArray alloc]init];
    _ColorViews_array=[[NSMutableArray alloc]init];
    
    
    
        
    //[self.backgroundView setFrame:CGRectMake(5, 89, 310, self.raceDetailTableView.frame.size.height)];
   // [self.backgroundView setBackgroundColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]];
    UIColor *uicolor = [UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0];
    CGColorRef color = [uicolor CGColor];
    [self.raceDetailTableView.layer setBorderWidth:1.0];
    [self.raceDetailTableView.layer setMasksToBounds:YES];
    [self.raceDetailTableView.layer setBorderColor:color];
    self.raceDetailTableView.separatorColor=[UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0];

    
    
    self.trasparentView=[[UIView alloc]init];
    [self.trasparentView setFrame:CGRectMake(5, 102, 310, 277)];
    [self.trasparentView setBackgroundColor:[UIColor whiteColor]];
    [self.trasparentView setAlpha:0.8];
    
    UIColor *uicolor1 = [UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0];
    CGColorRef color1 = [uicolor1 CGColor];
    [self.trasparentView.layer setBorderWidth:1.0];
    [self.trasparentView.layer setMasksToBounds:YES];
    [self.trasparentView.layer setBorderColor:color1];
    [self.view addSubview:self.trasparentView];
    
    [self.trasparentView setHidden:YES];

    
    
    
    //            [raceBut setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    //            [raceBut setTitle:@"RACE REPLAY" forState:UIControlStateNormal];
    //            [raceBut.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    //            [raceBut setFrame:CGRectMake(0, 30, 150, 38)];
    //            [self.resultDetailCustomCell.contentView addSubview:raceBut];
    
    
    
    [self.raceButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.raceButton setTitle:@"RACE REPLAY" forState:UIControlStateNormal];
    [self.raceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.raceButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.resultButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.resultButton setTitle:@"RESULTS CHART" forState:UIControlStateNormal];
    [self.resultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.resultButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];

    

    
    NSMutableDictionary *colorDict=[NSMutableDictionary dictionary];
   
    [colorDict setValue:@"Beulah Park" forKey:@"trackName"];
    [colorDict setValue:@"Race 8" forKey:@"raceNumber"];
    [colorDict setObject:[UIColor colorWithRed:69.0/255.0 green:138.0/255.0 blue:242.0/255.0 alpha:1.0] forKey:@"color"];
    [colorDict setValue:@"MTP\n3" forKey:@"MTPValue" ];
    [colorDict setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict setValue:@"13" forKey:@"result"];
    [colorDict setValue:@"Total Amount" forKey:@"total"];
    [colorDict setValue:@"$40" forKey:@"amount"];
    [self.ColorViews_array addObject:colorDict];
    
    NSMutableDictionary *colorDict1=[NSMutableDictionary dictionary];
   
    [colorDict1 setValue:@"Beulah Park" forKey:@"trackName"];
    [colorDict1 setValue:@"Race 1" forKey:@"raceNumber"];
    [colorDict1 setObject:[UIColor colorWithRed:164.0/255.0 green:99.0/255.0 blue:165.0/255.0 alpha:1.0] forKey:@"color"];
    [colorDict1 setValue:@"MTP\n1" forKey:@"MTPValue" ];
    [colorDict1 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict1 setValue:@"13" forKey:@"result"];
    [colorDict1 setValue:@"Total Amount" forKey:@"total"];
    [colorDict1 setValue:@"$54" forKey:@"amount"];
    [self.ColorViews_array addObject:colorDict1];
    
    NSMutableDictionary *colorDict2=[NSMutableDictionary dictionary];
    [colorDict2 setObject:@"green.png" forKey:@"ViewBackground"];
    [colorDict2 setValue:@"Beulah Park" forKey:@"trackName"];
    [colorDict2 setValue:@"Race 1" forKey:@"raceNumber"];
    [colorDict2 setObject:[UIColor colorWithRed:113.0/255.0 green:171.0/255.0 blue:1.0/255.0 alpha:1.0] forKey:@"color"];
    [colorDict2 setValue:@"MTP\n3" forKey:@"MTPValue" ];
    [colorDict2 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict2 setValue:@"10" forKey:@"result"];
    [colorDict2 setValue:@"Total Amount" forKey:@"total"];
    [colorDict2 setValue:@"$54" forKey:@"amount"];
    [self.ColorViews_array addObject:colorDict2];
    
    NSMutableDictionary *colorDict3=[NSMutableDictionary dictionary];
    [colorDict3 setObject:@"orange.png" forKey:@"ViewBackground"];
    [colorDict3 setValue:@"Beulah Park" forKey:@"trackName"];
    [colorDict3 setValue:@"Race: 2" forKey:@"raceNumber"];
    [colorDict3 setObject:[UIColor orangeColor] forKey:@"color"];
    [colorDict3 setValue:@"MTP\n7" forKey:@"MTPValue" ];
    [colorDict3 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict3 setValue:@"18" forKey:@"result"];
    [colorDict3 setValue:@"Total Amount" forKey:@"total"];
    [colorDict3 setValue:@"$54" forKey:@"amount"];
    
    [self.ColorViews_array addObject:colorDict3];
    
    
    
    _scrollView.clipsToBounds = NO;
    _scrollView.pagingEnabled = YES;
	_scrollView.showsHorizontalScrollIndicator = NO;
	
    CGFloat contentOffset = 0.0f;
    
    for (int i=0;i<[self.ColorViews_array count]; i++)
    {
        NSMutableDictionary *dic=[self.ColorViews_array objectAtIndex:i];

        CGRect imageViewFrame = CGRectMake(contentOffset, 0.0f,  _scrollView.frame.size.width - 5,  _scrollView.frame.size.height);
        
        UIView *view = [[UIView alloc] initWithFrame:imageViewFrame];
		view.backgroundColor = [dic valueForKey:@"color"];
        UILabel *raceNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, _scrollView.frame.size.width, 18)];
        //imageView.center=raceNameLabel.center;
        [raceNameLabel setTextColor:[UIColor whiteColor]];
        [raceNameLabel setTextAlignment:NSTextAlignmentCenter];
        [raceNameLabel setBackgroundColor:[UIColor clearColor]];
        [raceNameLabel setText:[dic valueForKey:@"trackName"]];
        [raceNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
        [view addSubview:raceNameLabel];
        
        UILabel *raceNumLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 29, _scrollView.frame.size.width - 5, 18)];
        [raceNumLabel setTextColor:[UIColor whiteColor]];
        [raceNumLabel setTextAlignment:NSTextAlignmentCenter];
        [raceNumLabel setBackgroundColor:[UIColor clearColor]];
        [raceNumLabel setText:[dic valueForKey:@"raceNumber"]];
        [raceNumLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
        [view addSubview:raceNumLabel];


        contentOffset += view.frame.size.width + 5;
       
        
		[ _scrollView addSubview:view];
        
        _scrollView.contentSize = CGSizeMake(contentOffset,  _scrollView.frame.size.height);
	}
    [self loadData];
    
    
}



-(void)loadData
{
    NSArray *array1=[NSArray arrayWithObjects:@"$4.20",@"$3.00",@"$2.60", nil];

    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setObject:[UIColor colorWithRed:245.0/255.0f green:235.0/255.0f blue:0.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [dic1 setValue:@"4" forKey:@"BetNumber"];
    [dic1 setObject:[UIColor blackColor] forKey:@"NumberColor"];
    [dic1 setValue:@"She's all yours" forKey:@"Title"];
    [dic1 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
    [dic1 setValue:@"$4.20" forKey:@"Dollar1"];
    [dic1 setValue:@"$3.00" forKey:@"Dollar2"];
    [dic1 setValue:@"$2.60" forKey:@"Dollar3"];
    [dic1 setObject:array1 forKey:@"labelValue"];
    [self.betTypeArray addObject:dic1];
    
    NSArray *array2=[NSArray arrayWithObjects:@"$7.00",@"$6.00", nil];
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setObject:[UIColor colorWithRed:69.0/255.0f green:244.0/255.0f blue:1.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [dic2 setValue:@"12" forKey:@"BetNumber"];
    [dic2 setObject:[UIColor blackColor] forKey:@"NumberColor"];
    [dic2 setValue:@"How do you find thered(IRE)" forKey:@"Title"];
    [dic2 setValue:@"$4.20" forKey:@"Dollar1"];
    [dic2 setValue:@"$3.00" forKey:@"Dollar2"];
    [dic2 setObject:array2 forKey:@"labelValue"];
    [dic2 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
    [self.betTypeArray addObject:dic2];
    
    NSArray *array3=[NSArray arrayWithObjects:@"$4.0", nil];
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setObject:[UIColor colorWithRed:0.0/255.0f green:26.0/255.0f blue:139.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [dic3 setValue:@"5" forKey:@"BetNumber"];
    [dic3 setObject:[UIColor whiteColor] forKey:@"NumberColor"];
    [dic3 setValue:@"Frannie's spirottoo" forKey:@"Title"];
    [dic3 setObject:array3 forKey:@"labelValue"];
    [dic3 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
    [self.betTypeArray addObject:dic3];
    
    
    
    NSMutableDictionary *wagerDict1=[NSMutableDictionary dictionary];
    [wagerDict1 setValue:@"$2.00 Exacta" forKey:@"WagerTitle"];
    [wagerDict1 setValue:@"4-1" forKey:@"WagerDetail"];
    [wagerDict1 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
     [wagerDict1 setObject:[UIColor colorWithRed:136.0/255.0f green:136.0/255.0f blue:136.0/255.0f alpha:1.0] forKey:@"detailTextClr"];
    [wagerDict1 setValue:@"$55.20" forKey:@"sideLabel"];
    [self.wagerArray addObject:wagerDict1];
    
    NSMutableDictionary *wagerDict2=[NSMutableDictionary dictionary];
    [wagerDict2 setValue:@"$2.00 Superfecta" forKey:@"WagerTitle"];
    [wagerDict2 setValue:@"4-1-3-2" forKey:@"WagerDetail"];
    [wagerDict2 setObject:[UIColor colorWithRed:136.0/255.0f green:136.0/255.0f blue:136.0/255.0f alpha:1.0] forKey:@"detailTextClr"];
    [wagerDict2 setValue:@"$153.60" forKey:@"sideLabel"];
    [self.wagerArray addObject:wagerDict2];

    NSMutableDictionary *wagerDict3=[NSMutableDictionary dictionary];
    [wagerDict3 setValue:@"$2.00 Superfecta" forKey:@"WagerTitle"];
    [wagerDict3 setValue:@"4-1-3-5" forKey:@"WagerDetail"];
    [wagerDict3 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
    [wagerDict3 setObject:[UIColor colorWithRed:136.0/255.0f green:136.0/255.0f blue:136.0/255.0f alpha:1.0] forKey:@"detailTextClr"];
    [wagerDict3 setValue:@"$155.40" forKey:@"sideLabel"];

    [self.wagerArray addObject:wagerDict3];

    NSMutableDictionary *wagerDict4=[NSMutableDictionary dictionary];
    [wagerDict4 setValue:@"$2.00 Trifecta" forKey:@"WagerTitle"];
    [wagerDict4 setValue:@"4-1-3" forKey:@"WagerDetail"];
    [wagerDict4 setObject:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0] forKey:@"TextColor"];
    [wagerDict4 setObject:[UIColor colorWithRed:136.0/255.0f green:136.0/255.0f blue:136.0/255.0f alpha:1.0] forKey:@"detailTextClr"];
    [wagerDict4 setValue:@"$131.80" forKey:@"sideLabel"];

    [self.wagerArray addObject:wagerDict4];
    

    
    
    NSMutableDictionary *orderDict1=[NSMutableDictionary dictionary];
    [orderDict1 setObject:[UIColor colorWithRed:245.0/255.0f green:235.0/255.0f blue:0.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [orderDict1 setValue:@"4" forKey:@"orderNumber"];
    [orderDict1 setObject:[UIColor blackColor] forKey:@"NumberColor"];
    [orderDict1 setValue:@"Mighty Albert" forKey:@"OrdeTitle"];
    [orderDict1 setValue:@"j:Octaivia Reyes | T: Alan D.White \n O: Alan D.White" forKey:@"OrderDetail"];
    [self.orderArray addObject:orderDict1];
    
    NSMutableDictionary *orderDict2=[NSMutableDictionary dictionary];
    [orderDict2 setObject:[UIColor colorWithRed:254.0/255.0f green:0.0/255.0f blue:0.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [orderDict2 setValue:@"1" forKey:@"orderNumber"];
    [orderDict2 setObject:[UIColor whiteColor] forKey:@"NumberColor"];
    [orderDict2 setValue:@"Peinado" forKey:@"OrdeTitle"];
    [orderDict2 setValue:@"j:Octaivia Reyes | T: Mathew Jacobson \n O: Alan D.White" forKey:@"OrderDetail"];
    [self.orderArray addObject:orderDict2];
    
    NSMutableDictionary *orderDict3=[NSMutableDictionary dictionary];
    [orderDict3 setObject:[UIColor colorWithRed:0.0/255.0f green:24.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [orderDict3 setValue:@"3" forKey:@"orderNumber"];
    [orderDict3 setObject:[UIColor whiteColor] forKey:@"NumberColor"];
    [orderDict3 setValue:@"Perfect Triangle" forKey:@"OrdeTitle"];
    [orderDict3 setValue:@"j:Octaivia Reyes | T: Enrique A.Calderon \n O: Alan D.White" forKey:@"OrderDetail"];
    [self.orderArray addObject:orderDict3];
    
    NSMutableDictionary *orderDict4=[NSMutableDictionary dictionary];
    [orderDict4 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [orderDict4 setValue:@"2" forKey:@"orderNumber"];
    [orderDict4 setObject:[UIColor blackColor] forKey:@"NumberColor"];
    [orderDict4 setValue:@"Johns Gold Prince" forKey:@"OrdeTitle"];
    [orderDict4 setValue:@"j:Octaivia Reyes | T: Enrique D.White \n O: Alan D.White" forKey:@"OrderDetail"];
    [self.orderArray addObject:orderDict4];
    
    NSMutableDictionary *orderDict5=[NSMutableDictionary dictionary];
    [orderDict5 setObject:[UIColor colorWithRed:0.0/255.0f green:182.0/255.0f blue:35.0/255.0f alpha:1.0] forKey:@"labelColor"];
    [orderDict5 setValue:@"5" forKey:@"orderNumber"];
    [orderDict5 setObject:[UIColor whiteColor] forKey:@"NumberColor"];
    [orderDict5 setValue:@"Jam Band" forKey:@"OrdeTitle"];
    [orderDict5 setValue:@"j:Octaivia Reyes | T: James D.White \n O: Alan D.White" forKey:@"OrderDetail"];
    [self.orderArray addObject:orderDict5];

    NSMutableDictionary *scratchDic1=[NSMutableDictionary dictionary];
    [scratchDic1 setValue:@"Baht(Veterinarian)" forKey:@"scratchTitle"];
    [self.scratchesArray addObject:scratchDic1];
    
    NSMutableDictionary *scratchDic2=[NSMutableDictionary dictionary];
    [scratchDic2 setValue:@"Unseen(Veterinarian)" forKey:@"scratchTitle"];
    [self.scratchesArray addObject:scratchDic2];

    NSMutableDictionary *scratchDic3=[NSMutableDictionary dictionary];
    [scratchDic3 setValue:@"Hope On the Rocks(Veterinarian)" forKey:@"scratchTitle"];
    [self.scratchesArray addObject:scratchDic3];

}
-(IBAction)raceReplayClicked:(id)sender
{
    
    if (![self.raceButton isSelected]==YES)
    {
        [self.trasparentView setHidden:NO];
        [self.raceButton setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1.0]];
        [self.raceButton setTitle:@"RESULTS CHART" forState:UIControlStateNormal];
        [self.raceButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.raceButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
              
        NSString *urlStr = @"http://www.ebookfrenzy.com/ios_book/movie/movie.mov";
        NSURL *url = [NSURL URLWithString:urlStr];
        self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [ self.moviePlayer play];
        self.moviePlayer.view.frame = CGRectMake(self.trasparentView.frame.origin.x+15, self.view.frame.origin.y+60, self.trasparentView.frame.size.width-40, self.trasparentView.frame.size.height-110);
        
        UIColor *uicolor1 = [UIColor blackColor];
        CGColorRef color1 = [uicolor1 CGColor];
        [self.moviePlayer.view.layer setBorderWidth:1.0];
        [self.moviePlayer.view.layer setMasksToBounds:YES];
        [self.moviePlayer.view.layer setBorderColor:color1];
        [self.self.trasparentView addSubview: self.moviePlayer.view];
        [self.raceButton setBackgroundColor:[UIColor whiteColor]];
        [self.raceButton setSelected:YES];
    }else
    {
      
        [self.raceButton setSelected:NO];
        [self.trasparentView setHidden:YES];
        [self.raceButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
        [self.raceButton setTitle:@"RACE REPLAY" forState:UIControlStateNormal];
        [self.raceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.raceButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    }
   
}


- (IBAction)backButtonClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)calendarButtonClicked:(id)sender
{
    
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(30,300,100,20)];
    tempView.backgroundColor=[UIColor clearColor];
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(5,5,300,20)];
    tempView.center=tempLabel.center;
    tempLabel.backgroundColor=[UIColor colorWithRed:217.0/255.0f green:217.0/255.0f blue:217.0/255.0f alpha:1.0];
    tempLabel.textAlignment=UITextAlignmentCenter;
    [tempLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    
    if (section==0) {
        tempLabel.text=@"Win,Place,Show Payoffs";
    }else if (section==1){
        tempLabel.text=@"Other Wager";
    }else if (section==2){
        tempLabel.text=@"Order of finish";
        
    }else{
        tempLabel.text=@"Scraches(Reason)";
        
    }
    
    
    [tempView addSubview:tempLabel];
    return tempView;
}




-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Win,Place,Show Payoffs";
    }else if(section == 1) {
        return @"Other Wagers";
    }else if (section == 2){
        return @"Order of finish";
    }else
        return @"Scratches(Reason)";
    
   }

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 0) {
        return self.betTypeArray.count;
    }else if(section == 1){
        return self.wagerArray.count;

    }else if (section == 2){
        return self.orderArray.count;
    }else if (section ==3){
        return self.scratchesArray.count;
    }
    

    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    self.resultDetailCustomCell  = (ZLResultDetailCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (self.resultDetailCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLResultDetailCustomCell" owner:self options:nil];
        
    }
    
    
    UIColor *uicolor = [UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
    CGColorRef color = [uicolor CGColor];
    [self.resultDetailCustomCell.numberLabel.layer setBorderWidth:1.0];
    [self.resultDetailCustomCell.numberLabel.layer setMasksToBounds:YES];
    [self.resultDetailCustomCell.numberLabel.layer setBorderColor:color];
   
    
    if (indexPath.section==0)
    {
        
        
        int i=260;
        int j=25;
        
        for (NSUInteger k = 0; k<[[[self.betTypeArray objectAtIndex:indexPath.row] valueForKey:@"labelValue"]count]; k++)
        {
            UILabel *betType_label=[[UILabel alloc] init];
            [betType_label setFrame:CGRectMake(i, j, 44, 19)];
            [betType_label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [betType_label setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0]];
            
            UIColor *uicolor = [UIColor colorWithRed:209.0/255.0 green:209.0/255.0 blue:209.0/255.0 alpha:1.0];
            CGColorRef color = [uicolor CGColor];
            [betType_label.layer setBorderWidth:1.0];
            [betType_label.layer setMasksToBounds:YES];
            [betType_label.layer setBorderColor:color];
            
            
            betType_label.layer.borderWidth = 1.0;
            [betType_label setTextAlignment:NSTextAlignmentCenter];
            [betType_label setText:[[[self.betTypeArray objectAtIndex:indexPath.row] valueForKey:@"labelValue"] objectAtIndex:k]];
            [self.resultDetailCustomCell addSubview:betType_label];
            i -= 49;
            
            
        }
        
        

        NSMutableDictionary *dic=[self.betTypeArray objectAtIndex:indexPath.row];
        
        [self.resultDetailCustomCell.titleLabel setFrame:CGRectMake(48, 0, 255, 30)];
        [self.resultDetailCustomCell addSubview:self.resultDetailCustomCell.numberLabel];
        [self.resultDetailCustomCell.numberLabel setFrame:CGRectMake(5, 10, 38, 34.5)];
        [self.resultDetailCustomCell.numberLabel setBackgroundColor:[dic valueForKey:@"labelColor"]];
        [self.resultDetailCustomCell.numberLabel setTextColor:[dic valueForKey:@"NumberColor"]];
        self.resultDetailCustomCell.numberLabel.text=[dic valueForKey:@"BetNumber"];
        self.resultDetailCustomCell.numberLabel.textAlignment=NSTextAlignmentCenter;
        self.resultDetailCustomCell.titleLabel.text=[dic valueForKey:@"Title"];
        [self.resultDetailCustomCell.titleLabel setTextColor:[dic valueForKey:@"TextColor"]];
        [self.resultDetailCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    }
    else if (indexPath.section==1)
    {
        NSMutableDictionary *dic=[self.wagerArray objectAtIndex:indexPath.row];
         [self.resultDetailCustomCell.titleLabel setFrame:CGRectMake(5, 0, 255, 30)];
         [self.resultDetailCustomCell.detailLabel setFrame:CGRectMake(5, self.resultDetailCustomCell.titleLabel.frame.size.height-15, 255, 30)];
        self.resultDetailCustomCell.titleLabel.text=[dic valueForKey:@"WagerTitle"];
        [self.resultDetailCustomCell.titleLabel setTextColor:[dic valueForKey:@"TextColor"]];
        [self.resultDetailCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
        self.resultDetailCustomCell.detailLabel.text=[dic valueForKey:@"WagerDetail"];
        [self.resultDetailCustomCell.detailLabel setTextColor:[dic valueForKey:@"detailTextClr"]];
        [self.resultDetailCustomCell.detailLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
        
        
        [self.resultDetailCustomCell.accessoryLabel setText:[dic valueForKey:@"sideLabel"]];
         [self.resultDetailCustomCell.accessoryLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
        
        
    }
    else if (indexPath.section==2)
    {
         NSMutableDictionary *dic=[self.orderArray objectAtIndex:indexPath.row];
        [self.resultDetailCustomCell.titleLabel setFrame:CGRectMake(55, 0, 200, 30)];
        [self.resultDetailCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
        [self.resultDetailCustomCell.detailLabel setFrame:CGRectMake(55, self.resultDetailCustomCell.titleLabel.frame.size.height-8, 255, 30)];
        [self.resultDetailCustomCell.detailLabel setTextColor:[UIColor colorWithRed:119.0/255.0f green:119.0/255.0f blue:119.0/255.0f alpha:1.0]];
        [self.resultDetailCustomCell.detailLabel setNumberOfLines:2];
        [self.resultDetailCustomCell.detailLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];

        [self.resultDetailCustomCell addSubview:self.resultDetailCustomCell.numberLabel];
        [self.resultDetailCustomCell.numberLabel setFrame:CGRectMake(5, 10,  38, 34.5)];
        [self.resultDetailCustomCell.numberLabel setBackgroundColor:[dic valueForKey:@"labelColor"]];
        [self.resultDetailCustomCell.numberLabel setTextColor:[dic valueForKey:@"NumberColor"]];
        [self.resultDetailCustomCell.numberLabel setText:[dic valueForKey:@"orderNumber"]];
        [self.resultDetailCustomCell.numberLabel setTextAlignment:NSTextAlignmentCenter];
        self.resultDetailCustomCell.titleLabel.text=[dic valueForKey:@"OrdeTitle"];
        self.resultDetailCustomCell.detailLabel.text=[dic valueForKey:@"OrderDetail"];;
    }
    else{
       NSMutableDictionary *dic=[self.scratchesArray objectAtIndex:indexPath.row];
        [self.resultDetailCustomCell.titleLabel setFrame:CGRectMake(2, 0, 255, 30)];
        self.resultDetailCustomCell.titleLabel.text=[dic valueForKey:@"scratchTitle"];
         [self.resultDetailCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
//        if (indexPath.row==self.scratchesArray.count-1) {
//            NSLog(@"enters.......");
//            UIButton *raceBut=[UIButton buttonWithType:UIButtonTypeCustom];
//            [raceBut setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
//            [raceBut setTitle:@"RACE REPLAY" forState:UIControlStateNormal];
//            [raceBut.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
//            [raceBut setFrame:CGRectMake(0, 30, 150, 38)];
//            [self.resultDetailCustomCell.contentView addSubview:raceBut];
//            UIButton *resultBut=[UIButton buttonWithType:UIButtonTypeCustom];
//             [resultBut setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
//            [resultBut setTitle:@"RESULTS CHART" forState:UIControlStateNormal];
//            [resultBut.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
//            [resultBut setFrame:CGRectMake(150, 30, 155, 38)];
//            [self.resultDetailCustomCell.contentView addSubview:resultBut];
//        }

        
}
    
    return self.resultDetailCustomCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section==1) {
        return 40;
    }else if (indexPath.section==2){
        return 55;
    }else if (indexPath.section==3){
        return 25;
    }
    return 49.5;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.row==3) {
    //        ZLResultsViewController *resultViewController=[[ZLResultsViewController alloc]init];
    //        [self.navigationController pushViewController:resultViewController animated:YES];
    //        
    //    }
    
}

- (IBAction)resultsChartButtonClicked:(id)sender
{
    ZLPdfViewController *objPdfViewController = [[ZLPdfViewController alloc] init];
    [self.navigationController pushViewController:objPdfViewController animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
