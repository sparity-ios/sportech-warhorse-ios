//
//  ZLCalendarScrollView.m
//  WarHorse
//
//  Created by Sparity on 02/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLCalendarScrollView.h"

@implementation ZLCalendarScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpSubView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUpSubView];
    }
    return self;
}

- (void) setUpSubView
{
    [self setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Left_Menu_BG.png"]]];
    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.frame];
//    [imageView setImage:[UIImage imageNamed:@"Left_Menu_BG.png"]];
//    [self addSubview:imageView];
    
    
    
    CGFloat lineHeight = 4;
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/3, self.frame.size.height - lineHeight, self.frame.size.width/3, lineHeight)];
    [line setBackgroundColor:[UIColor colorWithRed:67.0/255 green:160.0/255 blue:201.0/255 alpha:1.0]];
    [self addSubview:line];
    line = nil;
    
    self.selectedDate = [NSDate date];
    self.dateFormater = [[NSDateFormatter alloc] init];
    [self.dateFormater setDateFormat:@"dd MMM yyyy"];
    
    self.dateLabelsArray = [[NSMutableArray alloc] init];
        
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
	[self.scrollView setContentSize:CGSizeMake(5 * self.scrollView.frame.size.width/3, self.scrollView.frame.size.height)];
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width/3, 0)];
	[self.scrollView setPagingEnabled:NO];
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setShowsHorizontalScrollIndicator:NO];
	[self.scrollView setDelegate:self];
	
	for (int i = 0; i < 5; i++)
    {
		UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i * (self.scrollView.frame.size.width/3), 0, self.scrollView.frame.size.width/3, self.scrollView.frame.size.height)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setBackgroundColor:[UIColor clearColor]];
        if (i == 2)
        {
            [label setTextColor:[UIColor whiteColor]];
        }
        else
        {
        [label setTextColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
        }
        [label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [self.dateLabelsArray addObject:label];
		[self.scrollView addSubview:label];
        
	}
	[self addSubview:self.scrollView];
    

    [self setDatesToLabels];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.scrollView addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseInOut
                     animations:^{
                         CGPoint touchPoint=[gesture locationInView:self.scrollView];
                         
                         if (touchPoint.x <= (self.scrollView.frame.size.width/3) * 2)
                         {
                             [self.scrollView setContentOffset:CGPointMake(0, 0)];
                             self.selectedDate = [NSDate dateWithTimeInterval:-24 * 60 *60 sinceDate:self.selectedDate];
                         }
                         else if (touchPoint.x >= (self.scrollView.frame.size.width/3) * 3)
                         {
                             [self.scrollView setContentOffset:CGPointMake((self.frame.size.width/3) *2, 0)];
                             self.selectedDate = [NSDate dateWithTimeInterval:24 * 60 *60 sinceDate:self.selectedDate];
                         }
                     }
                     completion:^(BOOL finished){
                         [self finishedAnimation];
                     }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self handleLabelAnimation];
    }
}

- (void) finishedAnimation
{
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width/3, 0)];
    [self setDatesToLabels];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self handleLabelAnimation];
}

- (void) handleLabelAnimation
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         if (self.scrollView.contentOffset.x < self.frame.size.width/3)
                         {
                             [self.scrollView setContentOffset:CGPointMake(0, 0)];
                             self.selectedDate = [NSDate dateWithTimeInterval:-24 * 60 *60 sinceDate:self.selectedDate];
                         }
                         else if (self.scrollView.contentOffset.x > self.frame.size.width/3)
                         {
                             [self.scrollView setContentOffset:CGPointMake((self.frame.size.width/3) *2, 0)];
                             self.selectedDate = [NSDate dateWithTimeInterval:24 * 60 *60 sinceDate:self.selectedDate];
                         }
                     }
                     completion:^(BOOL finished){
                         [self finishedAnimation];
                     }];
    
    //[NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(finishedAnimation) userInfo:nil repeats:NO];
}

- (void) setDatesToLabels
{
    int day = -2;
    
    for (UILabel *label in self.dateLabelsArray) {
        [label setText:[self.dateFormater stringFromDate:[NSDate dateWithTimeInterval:day * 24 * 60 * 60 sinceDate:self.selectedDate]]];
        day ++;
    }
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
@end
