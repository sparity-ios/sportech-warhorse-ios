//
//  ZLResultDetailCustomCell.h
//  WarHorse
//
//  Created by Sparity on 7/31/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLResultDetailCustomCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UILabel *titleLabel;
@property(nonatomic,retain) IBOutlet UILabel *detailLabel;
@property(nonatomic,retain) IBOutlet UILabel *numberLabel;
@property(nonatomic,retain) IBOutlet UILabel *accessoryLabel;

@end
