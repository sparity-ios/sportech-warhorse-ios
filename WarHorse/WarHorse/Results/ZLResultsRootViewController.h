//
//  ZLResultsRootViewController.h
//  WarHorse
//
//  Created by Sparity on 01/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLResultTableCustomCell.h"

@interface ZLResultsRootViewController : UIViewController

@property(nonatomic,retain) IBOutlet UITableView *resultTableView;
@property(nonatomic,retain) IBOutlet ZLResultTableCustomCell *resultCustomCell;
@property(nonatomic,retain) NSMutableArray *resultArray;
@property(nonatomic,retain) IBOutlet UIScrollView *scrollView;

@end
