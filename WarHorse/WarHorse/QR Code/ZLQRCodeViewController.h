//
//  ZLQRCodeViewController.h
//  WarHorse
//
//  Created by Sparity on 18/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLQRCodeViewController : UIViewController
@property (nonatomic, strong) IBOutlet UIButton *toggleButton;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;
@property (nonatomic, strong) IBOutlet UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UILabel *topQrLabel;
@property(nonatomic,retain) IBOutlet UILabel *terminalQrCodeLabel;

- (IBAction)wagerButtonClicked:(id)sender;
- (IBAction)amountButtonClicked:(id)sender;

@end
