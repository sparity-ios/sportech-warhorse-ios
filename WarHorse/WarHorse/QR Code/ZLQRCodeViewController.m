//
//  ZLQRCodeViewController.m
//  WarHorse
//
//  Created by Sparity on 18/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLQRCodeViewController.h"

@interface ZLQRCodeViewController ()

@end

@implementation ZLQRCodeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:YES];
    [self prepareTopView];
}

- (void) prepareTopView
{
    
    [self.toggleButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.titleLabel setText:@"QR Code"];
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.topQrLabel setText:@"Unique QR Code for your Account"];
    [self.topQrLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.terminalQrCodeLabel setText:@"Please present the QR Code                                             \n  at the terminal Scanner"];
    [self.terminalQrCodeLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];

    
}

- (void)amountButtonClicked:(id)sender
{
    CGRect rect = ((UIButton *)sender).frame;
    
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        
        rect.origin.x += 30;
        rect.size.width -= 30;
        [self.amountButton setFrame:rect];
        
    }
    else{
        [self.amountButton setSelected:YES];
        
        rect.origin.x -= 30;
        rect.size.width += 30;
        [self.amountButton setFrame:rect];
        //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}
- (IBAction)wagerButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
