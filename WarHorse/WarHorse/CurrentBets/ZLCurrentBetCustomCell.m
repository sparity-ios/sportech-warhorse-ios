//
//  ZLCurrentBetCustomCell.m
//  WarHorse
//
//  Created by Sparity on 7/11/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLCurrentBetCustomCell.h"

@implementation ZLCurrentBetCustomCell
@synthesize amountDollar_Label=_amountDollar_Label;
@synthesize betType_Label=_betType_Label;
@synthesize betDollar_Label=_betDollar_Label;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state

}

@end
