//
//  ZLCurrentBetTypeViewController.m
//  WarHorse
//
//  Created by Sparity on 7/11/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLCurrentBetTypeViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AKMTPControl.h"

#define LabelHeight 20

@interface ZLCurrentBetTypeViewController ()


@end

@implementation ZLCurrentBetTypeViewController
@synthesize wagerTableView=_wagerTableView;
@synthesize moviePlayer=_moviePlayer;
@synthesize wagerArray=_wagerArray;
@synthesize currentBetCustomCell=_currentBetCustomCell;
@synthesize _scrollView;
@synthesize ColorViews_array=_ColorViews_array;
@synthesize backGroundView=_backGroundView;
@synthesize tagButton=_tagButton;
@synthesize resultButton=_resultButton;
@synthesize replayButton=_replayButton;
@synthesize finalArray=_finalArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    _wagerArray=[[NSMutableArray alloc]init];
    _ColorViews_array=[[NSMutableArray alloc]init];
    _finalArray=[[NSMutableArray alloc]init];
    
    [self loadData];
    [self.wagerTableView reloadData];

    
    self.navigationController.navigationBarHidden = YES;
    [self prepareTopView];
    
    _tagButton=[[UIButton alloc] init];
    
    [self.inPlayBtn setTitle:@"IN-PLAY" forState:UIControlStateNormal];
    
    _resultButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.resultButton setFrame:CGRectMake(self.wagerTableView.frame.origin.x-1, self.wagerTableView.frame.size.height+self.wagerTableView.frame.origin.y, self.wagerTableView.frame.size.width/2, 30)];
    [self.resultButton setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin];
    [self.resultButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.resultButton setTitle:@"RESULTS" forState:UIControlStateNormal];
    [self.resultButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.view addSubview:self.resultButton];
    [self.resultButton setHidden:YES];
    
    _replayButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [self.replayButton setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleRightMargin];
    [self.replayButton setFrame:CGRectMake(self.resultButton.frame.size.width+2, self.wagerTableView.frame.size.height+self.wagerTableView.frame.origin.y, self.wagerTableView.frame.size.width/2+5, 30)];
    [self.replayButton setTitle:@"REPLAY" forState:UIControlStateNormal];
    [self.replayButton setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.view addSubview:self.replayButton];
    [self.replayButton setHidden:YES];
    
    
    self.inPlayBtn.layer.borderWidth = 1;
    self.inPlayBtn.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
    
    self.finalBtn.layer.borderWidth = 1;
    self.finalBtn.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;

    
    
    
    
    AKMTPControl *mtpControl = [[AKMTPControl alloc] initWithFrame:CGRectMake(0, 80, 320, 47)];
    [self.view addSubview:mtpControl];
    
    
    _backGroundView=[[UIView alloc]initWithFrame:CGRectMake(self.wagerTableView.frame.origin.x, self.wagerTableView.frame.origin.y, self.wagerTableView.frame.size.width, self.wagerTableView.frame.size.height)];
    
    
    _scrollView.clipsToBounds = NO;
    _scrollView.pagingEnabled = YES;
	_scrollView.showsHorizontalScrollIndicator = NO;
	
	    
    [self colorViewData];
    
    
    
}


-(void)colorViewData{
    CGFloat contentOffset = 0.0f;
    
    
	for (int i=0;i<[self.ColorViews_array count]; i++)
    {
		CGRect imageViewFrame = CGRectMake(contentOffset, 0.0f,  _scrollView.frame.size.width,  _scrollView.frame.size.height);
        NSMutableDictionary *dic=[self.ColorViews_array objectAtIndex:i];
        
		UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageViewFrame];
		imageView.image = [UIImage imageNamed:[dic valueForKey:@"ViewBackground"]];
		imageView.contentMode = UIViewContentModeCenter;
        
        
        
        UILabel *raceNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, 0, 150, 20)];
        [raceNameLabel setTextColor:[UIColor whiteColor]];
        [raceNameLabel setBackgroundColor:[UIColor clearColor]];
        [raceNameLabel setText:[dic valueForKey:@"trackName"]];
        [raceNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [imageView addSubview:raceNameLabel];
        
        
        UILabel *raceNumLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, raceNameLabel.frame.size.height, 150, 20)];
        [raceNumLabel setTextColor:[UIColor whiteColor]];
        [raceNumLabel setBackgroundColor:[UIColor clearColor]];
        [raceNumLabel setText:[dic valueForKey:@"raceNumber"]];
        [raceNumLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [imageView addSubview:raceNumLabel];
        
            self.mtp_Label=[[UILabel alloc]initWithFrame:CGRectMake(raceNameLabel.frame.size.width+30, 5, 35, 28) ];
            [self.mtp_Label setTextColor:[UIColor whiteColor]];
            [self.mtp_Label setBackgroundColor:[UIColor redColor]];
            [self.mtp_Label setNumberOfLines:2];
            [self.mtp_Label setText:[dic valueForKey:@"MTPValue"]];
            [self.mtp_Label setTextAlignment:NSTextAlignmentCenter];
            [self.mtp_Label setFont:[UIFont fontWithName:@"Roboto-Bold" size:11]];
            [imageView addSubview:self.mtp_Label];
            
            
            
            self.totalBetLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, raceNumLabel.frame.size.height+25, 150, LabelHeight)];
            [ self.totalBetLabel setTextColor:[UIColor whiteColor]];
            [ self.totalBetLabel setBackgroundColor:[UIColor clearColor]];
            [ self.totalBetLabel setText:[dic valueForKey:@"betsTotal"]];
            [ self.totalBetLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [imageView addSubview: self.totalBetLabel];
            
            
           self.resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(8,  self.totalBetLabel.frame.size.height+ self.totalBetLabel.frame.origin.y, 150, LabelHeight)];
            [self.resultLabel setTextColor:[UIColor whiteColor]];
            [self.resultLabel setBackgroundColor:[UIColor clearColor]];
            [self.resultLabel setText:[dic valueForKey:@"result"]];
            [self.resultLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
            [imageView addSubview:self.resultLabel];
            
            
            self.amountLabel=[[UILabel alloc]initWithFrame:CGRectMake( self.totalBetLabel.frame.size.width-15,raceNumLabel.frame.size.height+25, 150, LabelHeight)];
            [self.amountLabel setTextColor:[UIColor whiteColor]];
            [self.amountLabel setBackgroundColor:[UIColor clearColor]];
            [self.amountLabel setText:[dic valueForKey:@"total"]];
            [self.amountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [imageView addSubview:self.amountLabel];
            
            
            self.dollarLabel=[[UILabel alloc]initWithFrame:CGRectMake( self.totalBetLabel.frame.size.width+42,self.amountLabel.frame.size.height+self.amountLabel.frame.origin.y, 150, LabelHeight)];
            [self.dollarLabel setTextColor:[UIColor whiteColor]];
            [self.dollarLabel setBackgroundColor:[UIColor clearColor]];
            [self.dollarLabel setText:[dic valueForKey:@"amount"]];
            [self.dollarLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
            [imageView addSubview:self.dollarLabel];
     
        if (self.tagButton.tag==2) {
            
            self.mtp_Label.text=@"";
            [self.mtp_Label removeFromSuperview];
            self.mtp_Label=[[UILabel alloc]initWithFrame:CGRectMake(raceNameLabel.frame.size.width+10, 5, 80, 20) ];
            [self.mtp_Label setTextColor:[UIColor whiteColor]];
            [self.mtp_Label setBackgroundColor:[UIColor clearColor]];
            [self.mtp_Label setNumberOfLines:2];
            [self.mtp_Label setText:[dic valueForKey:@"month"]];
            [self.mtp_Label setTextAlignment:NSTextAlignmentCenter];
            [self.mtp_Label setFont:[UIFont fontWithName:@"Roboto-Bold" size:11]];
            [imageView addSubview:self.mtp_Label];
            
            self.yearLable=[[UILabel alloc]initWithFrame:CGRectMake(raceNameLabel.frame.size.width+10, self.mtp_Label.frame.size.height, 80, 20) ];
            [self.yearLable setTextColor:[UIColor whiteColor]];
            [self.yearLable setBackgroundColor:[UIColor clearColor]];
            [self.yearLable setNumberOfLines:2];
            [self.yearLable setText:[dic valueForKey:@"year"]];
            [self.yearLable setTextAlignment:NSTextAlignmentCenter];
            [self.yearLable setFont:[UIFont fontWithName:@"Roboto-Bold" size:11]];
            [imageView addSubview:self.yearLable];
            
            
            
            self.totalBetLabel.text=@" ";
            self.totalBetLabel=[[UILabel alloc]initWithFrame:CGRectMake(8, raceNumLabel.frame.size.height+25, 150, LabelHeight)];
            [ self.totalBetLabel setTextColor:[UIColor whiteColor]];
            [ self.totalBetLabel setBackgroundColor:[UIColor clearColor]];
            [ self.totalBetLabel setText:[dic valueForKey:@"betAmountlbl"]];
            [ self.totalBetLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [imageView addSubview: self.totalBetLabel];
            
            self.resultLabel.text=@"";
            self.resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(8,  self.totalBetLabel.frame.size.height+ self.totalBetLabel.frame.origin.y, 150, LabelHeight)];
            [self.resultLabel setTextColor:[UIColor whiteColor]];
            [self.resultLabel setBackgroundColor:[UIColor clearColor]];
            [self.resultLabel setText:[dic valueForKey:@"BetAmout"]];
            [self.resultLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
            [imageView addSubview:self.resultLabel];
            
            self.amountLabel.text=@"";
           self.amountLabel=[[UILabel alloc]initWithFrame:CGRectMake( self.totalBetLabel.frame.size.width+15,raceNumLabel.frame.size.height+25, 150, LabelHeight)];
            [self.amountLabel setTextColor:[UIColor whiteColor]];
            [self.amountLabel setBackgroundColor:[UIColor clearColor]];
            [self.amountLabel setText:[dic valueForKey:@"WinLable"]];
            [self.amountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [imageView addSubview:self.amountLabel];
            
            
            self.dollarLabel.text=@"";
            self.dollarLabel=[[UILabel alloc]initWithFrame:CGRectMake( self.totalBetLabel.frame.size.width+42,self.amountLabel.frame.size.height+self.amountLabel.frame.origin.y, 150, LabelHeight)];
            [self.dollarLabel setTextColor:[UIColor whiteColor]];
            [self.dollarLabel setBackgroundColor:[UIColor clearColor]];
            [self.dollarLabel setText:[dic valueForKey:@"WinDolar"]];
            [self.dollarLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:13]];
            [imageView addSubview:self.dollarLabel];
        }
        
        
        
        
        
		[ _scrollView addSubview:imageView];
        
		contentOffset += imageView.frame.size.width;
        _scrollView.contentSize = CGSizeMake(contentOffset,  _scrollView.frame.size.height);
	}

}


- (void) prepareTopView
{
    
    [self.toggleButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.titleLabel setText:@"My Bets"];
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    
}

- (void)amountButtonClicked:(id)sender
{
    CGRect rect = ((UIButton *)sender).frame;
    
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        
        rect.origin.x += 30;
        rect.size.width -= 30;
        [self.amountButton setFrame:rect];
        
    }
    else{
        [self.amountButton setSelected:YES];
        
        rect.origin.x -= 30;
        rect.size.width += 30;
        [self.amountButton setFrame:rect];
        //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}

- (IBAction)wagerButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
}

- (void)timerMethod:(NSTimer *)timer
{
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        [self.amountButton setFrame:CGRectMake(240, 1, 44, 44)];
    }
}

-(void)loadData{
    
    
    NSMutableDictionary *wagerDict=[NSMutableDictionary dictionary];
    [wagerDict setValue:@"$1" forKey:@"dollar"];
    [wagerDict setValue:@"EXACTA-6 with 7,8" forKey:@"betType"];
    [wagerDict setValue:@"$2" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict];
    
    NSMutableDictionary *wagerDict1=[NSMutableDictionary dictionary];
    [wagerDict1 setValue:@"$1" forKey:@"dollar"];
    [wagerDict1 setValue:@"EXACTA BOX -6 thru 8" forKey:@"betType"];
    [wagerDict1 setValue:@"$6" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict1];
    
    NSMutableDictionary *wagerDict2=[NSMutableDictionary dictionary];
    [wagerDict2 setValue:@"$2" forKey:@"dollar"];
    [wagerDict2 setValue:@"WIN PLACE SH-2" forKey:@"betType"];
    [wagerDict2 setValue:@"$6" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict2];
    
    NSMutableDictionary *wagerDict3=[NSMutableDictionary dictionary];
    [wagerDict3 setValue:@"$1" forKey:@"dollar"];
    [wagerDict3 setValue:@"EXACTA-5,12" forKey:@"betType"];
    [wagerDict3 setValue:@"$20" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict3];
    
    NSMutableDictionary *wagerDict4=[NSMutableDictionary dictionary];
    [wagerDict4 setValue:@"$1" forKey:@"dollar"];
    [wagerDict4 setValue:@"EXACTA-5,7,12,13" forKey:@"betType"];
    [wagerDict4 setValue:@"$20" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict4];
    
    NSMutableDictionary *wagerDict5=[NSMutableDictionary dictionary];
    [wagerDict5 setValue:@"$1" forKey:@"dollar"];
    [wagerDict5 setValue:@"EXACTA-6 with 7,8" forKey:@"betType"];
    [wagerDict5 setValue:@"$2" forKey:@"betamount"];
    [self.wagerArray addObject:wagerDict5];
    
    
    
    
    NSMutableDictionary *finalDict=[NSMutableDictionary dictionary];
    [finalDict setValue:@"$1" forKey:@"dollar"];
    [finalDict setValue:@"EXACTA-6 with 7,8" forKey:@"betType"];
    [finalDict setValue:@"$2" forKey:@"betamount"];
    [finalDict setObject:[UIColor colorWithRed:0.0/255.0 green:141.0/255.0 blue:3.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [finalDict setObject:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKey:@"textColor"];
    [self.finalArray addObject:finalDict];
    
    NSMutableDictionary *finalDict1=[NSMutableDictionary dictionary];
    [finalDict1 setValue:@"$1" forKey:@"dollar"];
    [finalDict1 setValue:@"EXACTA BOX -6 thru 8" forKey:@"betType"];
    [finalDict1 setValue:@"$6" forKey:@"betamount"];
    [finalDict1 setObject:[UIColor colorWithRed:0.0/255.0 green:141.0/255.0 blue:3.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [finalDict1 setObject:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKey:@"textColor"];
    [self.finalArray addObject:finalDict1];
    
    NSMutableDictionary *finalDict2=[NSMutableDictionary dictionary];
    [finalDict2 setValue:@"$2" forKey:@"dollar"];
    [finalDict2 setValue:@"WIN PLACE SH-2" forKey:@"betType"];
    [finalDict2 setValue:@"$6" forKey:@"betamount"];
    [finalDict2 setObject:[UIColor colorWithRed:0.0/255.0 green:141.0/255.0 blue:3.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [self.finalArray addObject:finalDict2];
    
    NSMutableDictionary *finalDict3=[NSMutableDictionary dictionary];
    [finalDict3 setValue:@"$1" forKey:@"dollar"];
    [finalDict3 setValue:@"EXACTA-5,12" forKey:@"betType"];
    [finalDict3 setValue:@"-$20" forKey:@"betamount"];
    [finalDict3 setObject:[UIColor colorWithRed:246.0/255.0 green:2.0/255.0 blue:2.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [finalDict3 setObject:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKey:@"textColor"];
    [self.finalArray addObject:finalDict3];
    
    NSMutableDictionary *finalDict4=[NSMutableDictionary dictionary];
    [finalDict4 setValue:@"$1" forKey:@"dollar"];
    [finalDict4 setValue:@"WIN 7" forKey:@"betType"];
    [finalDict4 setValue:@"$20" forKey:@"betamount"];
    [finalDict4 setObject:[UIColor colorWithRed:0.0/255.0 green:141.0/255.0 blue:3.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [finalDict4 setObject:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKey:@"textColor"];
    [self.finalArray addObject:finalDict4];
    
    NSMutableDictionary *finalDict5=[NSMutableDictionary dictionary];
    [finalDict5 setValue:@"$1" forKey:@"dollar"];
    [finalDict5 setValue:@"Win 7" forKey:@"betType"];
    [finalDict5 setValue:@"$2" forKey:@"betamount"];
    [finalDict5 setObject:[UIColor colorWithRed:0.0/255.0 green:141.0/255.0 blue:3.0/255.0 alpha:1.0] forKey:@"betBGClr"];
    [finalDict5 setObject:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0] forKey:@"textColor"];
    [self.finalArray addObject:finalDict5];
    
    
    
    [self.wagerTableView setBackgroundColor:[UIColor colorWithRed:230.0/255.0f green:230.0/255.0f blue:230.0f/255.0f alpha:1.0]];
    UIColor *uicolor = [UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0];
    CGColorRef color = [uicolor CGColor];
    [self.wagerTableView.layer setBorderWidth:1.0];
    [self.wagerTableView.layer setMasksToBounds:YES];
    [self.wagerTableView.layer setBorderColor:color];
    [self.wagerTableView reloadData];
    
    
    
    NSMutableDictionary *colorDict=[NSMutableDictionary dictionary];
    [colorDict setObject:@"violet.png" forKey:@"ViewBackground"];
    [colorDict setValue:@"Mountaineer" forKey:@"trackName"];
    [colorDict setValue:@"Race: 8" forKey:@"raceNumber"];
    [colorDict setObject:[UIColor redColor] forKey:@"color"];
    [colorDict  setValue:@"JUL 24" forKey:@"month"];
    [colorDict setValue:@"2013" forKey:@"year"];
    [colorDict setValue:@"MTP\n3" forKey:@"MTPValue" ];
    [colorDict setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict setValue:@"13" forKey:@"result"];
    [colorDict setValue:@"Total Amount" forKey:@"total"];
    [colorDict setValue:@"$40" forKey:@"amount"];
    [colorDict setValue:@"Bet Amount" forKey:@"betAmountlbl"];
    [colorDict setValue:@"$40" forKey:@"BetAmout"];
    [colorDict setValue:@"Winnings" forKey:@"WinLable"];
    [colorDict setValue:@"$40" forKey:@"WinDolar"];
    [self.ColorViews_array addObject:colorDict];
    
    NSMutableDictionary *colorDict1=[NSMutableDictionary dictionary];
    [colorDict1 setObject:@"blue.png" forKey:@"ViewBackground"];
    [colorDict1 setValue:@"Harrington Raseway" forKey:@"trackName"];
    [colorDict1 setValue:@"Race: 7" forKey:@"raceNumber"];
    [colorDict1 setObject:[UIColor redColor] forKey:@"color"];
    [colorDict1  setValue:@"JUL 23" forKey:@"month"];
    [colorDict1 setValue:@"2013" forKey:@"year"];
    [colorDict1 setValue:@"MTP\n1" forKey:@"MTPValue" ];
    [colorDict1 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict1 setValue:@"13" forKey:@"result"];
    [colorDict1 setValue:@"Total Amount" forKey:@"total"];
    [colorDict1 setValue:@"$54" forKey:@"amount"];
    [colorDict1 setValue:@"Bet Amount" forKey:@"betAmountlbl"];
    [colorDict1 setValue:@"$39" forKey:@"BetAmout"];
    [colorDict1 setValue:@"Winnings" forKey:@"WinLable"];
    [colorDict1 setValue:@"$34" forKey:@"WinDolar"];
    [self.ColorViews_array addObject:colorDict1];
    
    NSMutableDictionary *colorDict2=[NSMutableDictionary dictionary];
    [colorDict2 setObject:@"green.png" forKey:@"ViewBackground"];
    [colorDict2 setValue:@"Belmont " forKey:@"trackName"];
    [colorDict2 setValue:@"Race: 1" forKey:@"raceNumber"];
    [colorDict2 setObject:[UIColor redColor] forKey:@"color"];
    [colorDict2 setValue:@"MTP\n3" forKey:@"MTPValue" ];
    [colorDict2 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict2 setValue:@"10" forKey:@"result"];
    [colorDict2 setValue:@"Total Amount" forKey:@"total"];
    [colorDict2 setValue:@"$54" forKey:@"amount"];
    [colorDict2  setValue:@"JUL 22" forKey:@"month"];
    [colorDict2 setValue:@"2013" forKey:@"year"];
    [colorDict2 setValue:@"Bet Amount" forKey:@"betAmountlbl"];
    [colorDict2 setValue:@"10" forKey:@"BetAmout"];
    [colorDict2 setValue:@"Winnings" forKey:@"WinLable"];
    [colorDict2 setValue:@"$34" forKey:@"WinDolar"];

    [self.ColorViews_array addObject:colorDict2];
    
    NSMutableDictionary *colorDict3=[NSMutableDictionary dictionary];
    [colorDict3 setObject:@"orange.png" forKey:@"ViewBackground"];
    [colorDict3 setValue:@"Parx Racing" forKey:@"trackName"];
    [colorDict3 setValue:@"Race: 2" forKey:@"raceNumber"];
    [colorDict3 setObject:[UIColor redColor] forKey:@"color"];
    [colorDict3 setValue:@"MTP\n7" forKey:@"MTPValue" ];
    [colorDict3 setValue:@"Total Bets" forKey:@"betsTotal"];
    [colorDict3 setValue:@"18" forKey:@"result"];
    [colorDict3 setValue:@"Total Amount" forKey:@"total"];
    [colorDict3 setValue:@"$54" forKey:@"amount"];
    [colorDict3  setValue:@"JUL 21" forKey:@"month"];
    [colorDict3 setValue:@"2013" forKey:@"year"];
    [colorDict1 setValue:@"Bet Amount" forKey:@"betAmountlbl"];
    [colorDict1 setValue:@"$39" forKey:@"BetAmout"];
    [colorDict1 setValue:@"Winnings" forKey:@"WinLable"];
    [colorDict1 setValue:@"$34" forKey:@"WinDolar"];


    [self.ColorViews_array addObject:colorDict3];
    
}


-(IBAction)wager_Clicked:(id)sender{
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
    
}
-(IBAction)video_Clicked:(id)sender
{
    [self.backGroundView setBackgroundColor:[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0]];
    UIColor *uicolor = [UIColor colorWithRed:214.0/255.0 green:214.0/255.0 blue:214.0/255.0 alpha:1.0];
    CGColorRef color = [uicolor CGColor];
    [self.backGroundView.layer setBorderWidth:1.0];
    [self.backGroundView.layer setMasksToBounds:YES];
    [self.backGroundView.layer setBorderColor:color];
    [self.view addSubview:self.backGroundView];
    
    [self.wagerTableView removeFromSuperview];
    [self.video_Button setImage:[UIImage imageNamed:@"video_selectbutton2.png"] forState:UIControlStateNormal] ;
    NSString *urlStr = @"http://www.ebookfrenzy.com/ios_book/movie/movie.mov";
    NSURL *url = [NSURL URLWithString:urlStr];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        [ self.moviePlayer play];
    self.moviePlayer.view.frame = CGRectMake(self.wagerTableView.frame.origin.x+15, self.wagerTableView.frame.origin.y+15, self.wagerTableView.frame.size.width-30, self.wagerTableView.frame.size.height-30);
    UIColor *uicolor1 = [UIColor blackColor];
    CGColorRef color1 = [uicolor1 CGColor];
    [self.moviePlayer.view.layer setBorderWidth:1.0];
    [self.moviePlayer.view.layer setMasksToBounds:YES];
    [self.moviePlayer.view.layer setBorderColor:color1];
    [self.view addSubview: self.moviePlayer.view];
    
    
}
-(IBAction)cancel_Clicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardLogOut]forKey:@"viewNumber"]];
}

-(IBAction)back_Clicked:(id)sender{
   // [self.navigationController popViewControllerAnimated:YES];
     [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardHome]forKey:@"viewNumber"]];
}

-(IBAction)settings_Clicked:(id)sender{
    
}

-(IBAction)playClicked:(UIButton *)button{
    NSLog(@"button tag is %d",button.tag);
        if(button.tag==1)
        {
            
            
          
            self.inPlayBtn.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
            self.finalBtn.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
            
           [self.finalBtn setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];
             [self.inPlayBtn setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
            
            [self.inPlayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.finalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            
            
            self.tagButton.tag=button.tag;
            [self.wagerTableView reloadData];
            [self.wagerBtn setHidden: NO];
            [self.video_Button setHidden:NO];
            [self.cancel_Button setHidden:NO];
            [self.resultButton setHidden:YES];
            [self.replayButton setHidden:YES];
            [self colorViewData];
        NSLog(@"111");
         }
        else
        {
            
            self.inPlayBtn.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
            self.finalBtn.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
          
             [self.inPlayBtn setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];
           
              [self.finalBtn setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
            //[self.inPlayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.inPlayBtn.titleLabel setTextColor:[UIColor blackColor]];
            [self.finalBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
           
            [self.resultButton setHidden:NO];
            [self.wagerBtn setHidden: YES];
            [self.video_Button setHidden:YES];
            [self.cancel_Button setHidden:YES];
            [self.replayButton setHidden:NO];
            self.tagButton.tag=button.tag;
            [self.wagerTableView reloadData];
            [self colorViewData];
        NSLog(@"222");
    }
}

#pragma mark -
#pragma mark UITableViewDelegate Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return [self.wagerArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    self.currentBetCustomCell  = (ZLCurrentBetCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.currentBetCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLCurrentBetCustomCell" owner:self options:nil];
        
    }
    
    [self.currentBetCustomCell.contentView setBackgroundColor:[UIColor colorWithRed:230.0/255.0f green:230.0/255.0f blue:230.0f/255.0f alpha:1.0]];
    
     
        NSMutableDictionary *dic = [self.wagerArray objectAtIndex:indexPath.row];
        
        [self.currentBetCustomCell.amountDollar_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [self.currentBetCustomCell.amountDollar_Label setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
        [self.currentBetCustomCell.betDollar_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [self.currentBetCustomCell.betDollar_Label setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
        
        [self.currentBetCustomCell.betType_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
        [self.currentBetCustomCell.betType_Label setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
        
        self.currentBetCustomCell.betDollar_Label.text=[dic valueForKey:@"dollar"];
        self.currentBetCustomCell.betType_Label.text=[dic valueForKey:@"betType"];
        self.currentBetCustomCell.amountDollar_Label.text=[dic valueForKey:@"betamount"];;
    self.currentBetCustomCell.close_Button.hidden=NO;


    if (self.tagButton.tag==2)
    {
         NSMutableDictionary *dic = [self.finalArray objectAtIndex:indexPath.row];
        [self.currentBetCustomCell.amountDollar_Label setFrame:CGRectMake(265, 10, 35, 26)];
        self.currentBetCustomCell.close_Button.hidden=YES;
       
        self.currentBetCustomCell.betDollar_Label.text=[dic valueForKey:@"dollar"] ;
        self.currentBetCustomCell.betType_Label.text=[dic valueForKey:@"betType"];
         self.currentBetCustomCell.amountDollar_Label.text=[dic valueForKey:@"betamount"];
        if ([[dic valueForKey:@"betamount"] isEqualToString:@"-$20"]) {
            [self.currentBetCustomCell.amountDollar_Label setFrame:CGRectMake(250, 8, 50, 30)];
        }
        [self.currentBetCustomCell.amountDollar_Label setBackgroundColor:[dic valueForKey:@"betBGClr"]];
        [self.currentBetCustomCell.amountDollar_Label setTextColor:[dic valueForKey:@"textColor"]];
        [self.currentBetCustomCell.amountDollar_Label setTextAlignment:NSTextAlignmentCenter];
}
    
    
    
    return self.currentBetCustomCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

-(void)viewDidUnload{
    self.wagerTableView=nil;
    self.moviePlayer=nil;
    self.wagerArray=nil;
   // self.currentbet_Label=nil;
    self.ColorViews_array=nil;
    //self.currentbet_Label=nil;
    self.backGroundView=nil;
    self.wager_Button=nil;
    self.video_Button=nil;
    self.cancel_Button=nil;
    self.currentBetCustomCell=nil;
    self.back_Button=nil;
    self.settings_Button=nil;
    self._scrollView=nil;
    self.ColorViews_array=nil;
   // self.currentbet_Label=nil;
    self.amountButton=nil;
    self.backGroundView=nil;
    self.currentBetCustomCell.amountDollar_Label=nil;
    self.currentBetCustomCell.betDollar_Label=nil;
    self.currentBetCustomCell.betType_Label=nil;
    self.currentBetCustomCell.close_Button=nil;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
