//
//  ZLCurrentBetCustomCell.h
//  WarHorse
//
//  Created by Sparity on 7/11/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLCurrentBetCustomCell : UITableViewCell
@property(nonatomic,retain) IBOutlet UILabel *betDollar_Label;
@property(nonatomic,retain) IBOutlet UILabel *betType_Label;
@property(nonatomic,retain) IBOutlet UILabel *amountDollar_Label;
@property(nonatomic,retain) IBOutlet UIButton *close_Button;

@end
