//
//  ZLCustomAmountView.m
//  CustomAmoutPicker
//
//  Created by Sparity on 29/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLCustomAmountView.h"
#import <QuartzCore/QuartzCore.h>

#define BORDER_PADDING 5
#define BUTTON_BG_COLOR [UIColor colorWithRed:207.0/255 green:220.0/255 blue:226.0/255 alpha:1.0]

#define TEXT_COLOR [UIColor colorWithRed:36.0/255 green:36.0/255 blue:36.0/255 alpha:1.0]

#define BORDER_COLOR [UIColor colorWithRed:146.0/255 green:159.0/255 blue:165.0/255 alpha:1.0]

@implementation ZLCustomAmountView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setUpSubView];
        
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder

{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setUpSubView];
    }
    return self;
}

- (void) setUpSubView
{
    amoutTextField = [[UITextField alloc] initWithFrame:CGRectMake(BORDER_PADDING, BORDER_PADDING, self.frame.size.width - BORDER_PADDING*2, 30)];
    [amoutTextField setTextAlignment:NSTextAlignmentRight];
    [amoutTextField setBackgroundColor:[UIColor colorWithRed:86.0/255 green:86.0/255 blue:94.0/255 alpha:1.0]];
    [amoutTextField setAutoresizesSubviews:YES];
    //[amoutTextField setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [amoutTextField setText:@"$"];
    [amoutTextField setContentMode:UIViewContentModeCenter];
    [amoutTextField setTextColor:[UIColor whiteColor]];
    [amoutTextField setBorderStyle:UITextBorderStyleLine];
    [self addSubview:amoutTextField];
    
    CGFloat buttonWidth = (self.frame.size.width - (BORDER_PADDING * 2))/3;
    
    CGFloat buttonStartingYValue = amoutTextField.frame.size.height + amoutTextField.frame.origin.y + 10;
    
    int count = 9;
    CGFloat y ;
    for (int i = 0; i <3 ; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            CGFloat x = self.frame.size.width -  BORDER_PADDING - buttonWidth - ((j %3) * buttonWidth);
            y = buttonStartingYValue + (i *buttonWidth);
            [self addButtonWithFrame:CGRectMake(x,y,buttonWidth,buttonWidth) tag:count image:nil title:[NSString stringWithFormat:@"%i",count]];
            count--;
        }
    }
    y += buttonWidth;
    [self addButtonWithFrame:CGRectMake(BORDER_PADDING, y,buttonWidth,buttonWidth) tag:10 image:nil title:@"."];
    
    [self addButtonWithFrame:CGRectMake(BORDER_PADDING + buttonWidth, y,buttonWidth,buttonWidth) tag:0 image:nil title:@"0"];

    [self addButtonWithFrame:CGRectMake(BORDER_PADDING + (buttonWidth *2), y,buttonWidth,buttonWidth) tag:11 image:[UIImage imageNamed:@"crossbutton.png"] title:nil];
    
    
    UIButton *goButtonClicked = [UIButton buttonWithType:UIButtonTypeCustom];
    [goButtonClicked setTitle:@"GO" forState:UIControlStateNormal];
    [goButtonClicked setBackgroundColor:[UIColor colorWithRed:47.0/255 green:57.0/255 blue:66.0/255 alpha:1.0]];
    [goButtonClicked setFrame:CGRectMake(BORDER_PADDING, self.frame.size.height - BORDER_PADDING - 29, self.frame.size.width - (2 * BORDER_PADDING), 29)];
    [goButtonClicked setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goButtonClicked addTarget:self action:@selector(goButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    //[goButtonClicked setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];

    [self addSubview:goButtonClicked];

}

- (void) addButtonWithFrame:(CGRect)rect tag:(int)tag image:(UIImage *)image title:(NSString *)title
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [button setFrame:rect];
    button.layer.borderWidth = 0.5;
    button.layer.borderColor = BORDER_COLOR.CGColor;
    [button setTitleColor:TEXT_COLOR forState:UIControlStateNormal];
    [button setBackgroundColor:BUTTON_BG_COLOR];
    [button.titleLabel setFont:[UIFont systemFontOfSize:36]];
    button.tag = tag;
    if (title) {
        [button setTitle:title forState:UIControlStateNormal];
    }
    
    if (image)
    {
        [button setImage:image forState:UIControlStateNormal];
    }
    //[button setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight)];
    [button.titleLabel setShadowOffset:CGSizeMake(1, 1)];
    [button setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:40]];

    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
    
}

- (void) buttonClicked: (UIButton *) button
{
    
    NSArray *array = [amoutTextField.text componentsSeparatedByString:@"."];
    if (array.count > 1) {
        NSString *string = [array objectAtIndex:1];
        if (string.length ==2) {
            return;
        }
    }
    
    if (button.tag == 10) {
        NSRange textRange;
        textRange =[amoutTextField.text rangeOfString:@"."];
        
        if(textRange.location == NSNotFound)
        {
            
            [amoutTextField setText:[NSString stringWithFormat:@"%@.",amoutTextField.text]];
        }
    }
    else if (button.tag == 11) {
        NSString *string = [amoutTextField text];
        if ( [string length] > 1)
        {
            string = [string substringToIndex:[string length] - 1];
            amoutTextField.text = string;
        }
    }
    else
    {
        [amoutTextField setText:[NSString stringWithFormat:@"%@%i",amoutTextField.text,button.tag]];
    }
}

- (void) goButtonClicked
{
    //[self removeFromSuperview];
    
    [self.delegate selectedAmount:amoutTextField.text];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
