//
//  ZLWagerViewController.m
//  WarHorse
//
//  Created by Sparity on 7/5/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLWagerViewController.h"
#import "ZLLeftSideMenuViewController.h"
#import "ZLSideMenu.h"
#import "ZLSelectedValues.h"
#import "ZLRunnersViewController.h"
#import "ZLConformationViewController.h"
#import "ZLTrackViewController.h"
#import "ZLRaceViewController.h"
#import "ZLAmountViewController.h"
#import "ZLRunnersViewController.h"
#import "ZLBetTypeViewController.h"
#import "ZLAmountView.h"
#import "ZLPlaceWagerResultsViewController.h"
#import "ZLOddsBoardViewController.h"

#define SIDE_MENU_WIDTH 82.5
@interface ZLWagerViewController ()

@end

@implementation ZLWagerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (!self) return nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(HandleTopRightButtons:)
                                                 name:@"HandleTopRightButtons"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadWagerView:)
                                                 name:@"LoadWagerView"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadOddsView:)
                                                 name:@"loadOddsView"
                                               object:nil];
    
    return self;
}

- (void) loadOddsView:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"loadOddsView"])
    {
        ZLOddsBoardViewController *objOdds = [[ZLOddsBoardViewController alloc] init];
        [self.navigationController presentViewController:objOdds animated:YES completion:nil];
        objOdds = nil;
    }
}

- (void) loadWagerView:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"LoadWagerView"])
    {
        NSNumber *viewIndex = [[notification userInfo] objectForKey:@"currentLoadedIndex"];
        [self loadViewInWager:[viewIndex integerValue]];
        viewIndex=nil;
    }
}

- (void) loadViewInWager:(Wager)index
{
    [self.sideMenu nextViewLoaded];

    
    UIViewController *viewController = nil;
    index ++;
    switch (index)
    {
        case WagerTrack:
            viewController = [[ZLTrackViewController alloc] init];
            break;
            
        case WagerRace:
            viewController = [[ZLRaceViewController alloc] init];
            break;
            
        case WagerAmount:
            viewController = [[ZLAmountViewController alloc] init];
            break;
            
        case WagerBetType:
            viewController = [[ZLBetTypeViewController alloc] init];
            break;
            
        case WagerRunners:
            viewController = [[ZLRunnersViewController alloc] init];
            break;
            
        default:
            break;
    }
    
    if (viewController)
    {
        [self.wagerNavigationCoontroller pushViewController:viewController animated:NO];
    }
}


- (void) HandleTopRightButtons:(NSNotification *) notification
{
    if (self.listGridButton.hidden)
    {
        self.listGridButton.hidden = NO;
        self.homeButton.hidden = NO;
        self.amountButton.hidden = YES;
    }
    else
    {
        self.listGridButton.hidden = YES;
        self.homeButton.hidden = YES;
        self.amountButton.hidden = NO;
    }
}

//- (void) loadSideMenu:(NSNotification *) notification
//{
//    if (self.listGridButton.hidden)
//    {
//        self.listGridButton.hidden = NO;
//        self.homeButton.hidden = NO;
//        self.amountButton.hidden = YES;
//    }
//    else
//    {
//        self.listGridButton.hidden = YES;
//        self.homeButton.hidden = YES;
//        self.amountButton.hidden = NO;
//    }
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.amountsArray = [[NSMutableArray alloc] init];
    
    
    [self clearSelectedValuesWithIndex:-1];
    
    
    self.navigationController.navigationBarHidden = YES;
    
    [self prepareTopView];
    
    [self.topLeftImageView setFrame:CGRectMake(0, 0, self.view.frame.size.width - SIDE_MENU_WIDTH, 46)];
    [self.topRightImageView setFrame:CGRectMake(self.topLeftImageView.frame.size.width, 0, SIDE_MENU_WIDTH, 46)];

    
    [self.wagerNavigationCoontroller.view setFrame:CGRectMake(0, 46, self.view.frame.size.width - SIDE_MENU_WIDTH, self.view.frame.size.height - 46)];
    [self.view addSubview:self.wagerNavigationCoontroller.view];
    
    
    self.sideMenu = [[ZLSideMenu alloc] initWithFrame:CGRectMake(self.view.frame.size.width - SIDE_MENU_WIDTH, 46, SIDE_MENU_WIDTH, self.view.frame.size.height-46)];
    [self.sideMenu setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
    self.sideMenu.delegate = self;
    [self.view addSubview:self.sideMenu];
    
    [self loadData];
}
- (void) prepareTopView
{
    
    UIButton *backButton = [[UIButton alloc] init];
    [backButton setFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"toggle.png"] forState:UIControlStateNormal];
    [backButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton = nil;
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(45, 11, 50, 21)];
    [title setText:@"Wager"];
    [title setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [title setTextColor:[UIColor whiteColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:title];
    title=nil;
    
    
    self.amountButton = [[UIButton alloc] initWithFrame:CGRectMake(240, 0, 44, 46)];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"symbol.png"] forState:UIControlStateNormal];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"balancebg.png"] forState:UIControlStateSelected];
    [self.amountButton setTitle:@"" forState:UIControlStateNormal];
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.amountButton.titleLabel setLineBreakMode:NSLineBreakByCharWrapping];
    [self.amountButton addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.amountButton];
    
}

- (void)amountButtonClicked:(id)sender
{
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        [self.amountButton setFrame:CGRectMake(240, 1, 44, 44)];
        
    }
    else{
        [self.amountButton setSelected:YES];
        [self.amountButton setFrame:CGRectMake(245, 1, 71, 44)];
        //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}

- (void)timerMethod:(NSTimer *)timer
{
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        [self.amountButton setFrame:CGRectMake(240, 1, 44, 44)];
    }
}

- (void) loadData
{
    self.sideMenuDetailsArray = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"TRACK" forKey:@"title"];
    [dic setValue:@"track.png" forKey:@"imageName"];
    [dic setValue:@"track_selected.png" forKey:@"imageSelected"];
    [dic setObject:[NSNumber numberWithInt:WagerTrack] forKey:@"tag"];
    [self.sideMenuDetailsArray addObject:dic];
    
    
    NSMutableDictionary *dic1 = [NSMutableDictionary dictionary];
    [dic1 setValue:@"RACE" forKey:@"title"];
    [dic1 setValue:@"race.png" forKey:@"imageName"];
    [dic1 setValue:@"race_selected.png" forKey:@"imageSelected"];
    [dic1 setObject:[NSNumber numberWithInt:WagerRace] forKey:@"tag"];
    [self.sideMenuDetailsArray addObject:dic1];
    
    NSMutableDictionary *dic2 = [NSMutableDictionary dictionary];
    [dic2 setValue:@"AMOUNT" forKey:@"title"];
    [dic2 setValue:@"Amount.png" forKey:@"imageName"];
    [dic2 setValue:@"Amount_selected.png" forKey:@"imageSelected"];
    [dic2 setObject:[NSNumber numberWithInt:WagerAmount] forKey:@"tag"];
    [self.sideMenuDetailsArray addObject:dic2];
    
    NSMutableDictionary *dic3 = [NSMutableDictionary dictionary];
    [dic3 setValue:@"BET TYPE" forKey:@"title"];
    [dic3 setValue:@"betype.png" forKey:@"imageName"];
    [dic3 setValue:@"betType_selected.png" forKey:@"imageSelected"];
    [dic3 setObject:[NSNumber numberWithInt:WagerBetType] forKey:@"tag"];
    [self.sideMenuDetailsArray addObject:dic3];
    
    NSMutableDictionary *dic4 = [NSMutableDictionary dictionary];
    [dic4 setValue:@"RUNNERS" forKey:@"title"];
    [dic4 setValue:@"runners.png" forKey:@"imageName"];
    [dic4 setValue:@"runners_selected.png" forKey:@"imageSelected"];
    [dic4 setObject:[NSNumber numberWithInt:WagerRunners] forKey:@"tag"];
    [self.sideMenuDetailsArray addObject:dic4];
    
    
    [self sortItemsArray];
}

- (void) sortItemsArray
{
    [self.sideMenuDetailsArray sortUsingComparator:
     ^(id obj1, id obj2)
     {
         int value1 = [[obj1 objectForKey:@"tag"] integerValue];
         int value2 = [[obj2 objectForKey:@"tag"] integerValue];
         if (value1 > value2)
         {
             return (NSComparisonResult)NSOrderedDescending;
         }
         
         if (value1 < value2)
         {
             return (NSComparisonResult)NSOrderedAscending;
         }
         return (NSComparisonResult)NSOrderedSame;
     }];
}


- (void)tapDetectingView:(id)view
{
    ZLSideMenuTile *tile = (ZLSideMenuTile *)view;
    if (tile.tag == WagerRunners && [self isRunnersSelected])
    {
        self.objZLConformationViewController = [[ZLConformationViewController alloc]init];
        self.objZLConformationViewController.view.frame = self.view.bounds;
        self.objZLConformationViewController.delegate = self;
        [self.view addSubview:self.objZLConformationViewController.view];
    }
    else if(tile.tag == WagerAmount && [[ZLSelectedValues sharedInstance] selectedAmount] && ![[ZLSelectedValues sharedInstance] isAmountCustomSelected])
    {
      
        ZLAmountViewController *amountViewController =  [self.wagerNavigationCoontroller.viewControllers objectAtIndex:WagerAmount];
        self.amountsArray = [NSMutableArray arrayWithArray:amountViewController.amount_Array];

        if([self isAmoutViewLoaded])
        {
            [amountView removeFromSuperview];
            [[ZLSelectedValues sharedInstance] setSelectedAmount:[[self.amountsArray objectAtIndex:amountView.selectRow] valueForKey:@"amountNumber"]];
        }
        else
        {
            [self clearRunnerSelection];
            amountView = [[ZLAmountView alloc] initWithFrame:self.wagerNavigationCoontroller.view.bounds delegate:self];
            [amountView setSelectRow:[self indexOfSelectedAmount]];
            [self.wagerNavigationCoontroller.view addSubview:amountView];
        }
    }
    else
    {
        [amountView removeFromSuperview];
        UIViewController *viewController = [self.wagerNavigationCoontroller.viewControllers objectAtIndex:tile.tag];
        [self.wagerNavigationCoontroller popToViewController:viewController animated:NO];
    }
    


}

-(void)clearRunnerSelection
{
    [[ZLSelectedValues sharedInstance]setSelectedRunnersArray:nil];
    ZLRunnersViewController *runners=[[self.wagerNavigationCoontroller viewControllers] objectAtIndex:WagerRunners];
    [runners reloadViews];
    
}

- (CGFloat) yValueToStart
{
    if (self.wagerNavigationCoontroller.view.frame.origin.y <=0) {
        return ((self.wagerNavigationCoontroller.view.frame.size.height + 45)/5) * WagerAmount;
    }
    else{
        return (self.wagerNavigationCoontroller.view.frame.size.height/5) * WagerAmount;
    }
}

- (BOOL) isAmoutViewLoaded
{
    NSArray *suvViewArray = [self.wagerNavigationCoontroller.view subviews];
    if([suvViewArray containsObject:amountView]) {
        return YES;
    }
    else{
        return NO;
    }
}

- (NSUInteger) indexOfSelectedAmount
{
    int counter = 0;
    for (NSMutableDictionary *dic in self.amountsArray) {
        NSString *amount = [dic valueForKey:@"amountNumber"];
        NSString *selectedAmount = [[ZLSelectedValues sharedInstance] selectedAmount];
        if ([selectedAmount isEqualToString:amount]) {
            return counter;
        }
        counter ++;
    }
    return 0;
}

#pragma mark -  Amount View Delegate Methods

- (NSUInteger) numberOfItems
{
    return self.amountsArray.count;
}

- (NSString *) titleForItem:(int) index
{
    return [[self.amountsArray objectAtIndex:index] valueForKey:@"amountNumber"];
}

- (void) selectedRowWithIndex:(int)index
{
}

#pragma mark - Conformation delegate methods
- (void) confirmWager:(id)sender
{    
    self.objResult = [[ZLPlaceWagerResultsViewController alloc] init];
    [self.objResult setDelegate:self];
    self.objResult.view.frame = self.view.bounds;
    [self.view addSubview:self.objResult.view];
}

#pragma mark - Place Wager Result delegate methods

- (void) repeatWager
{
}

- (void) newWager
{
    [self.wagerNavigationCoontroller.view setFrame:CGRectMake(0, 46, self.view.frame.size.width - SIDE_MENU_WIDTH, self.view.frame.size.height - 46)];

    [self removeAllData];
}

- (void) removeAllData
{
    [self clearSelectedValuesWithIndex:-1];
    
    [self.sideMenu clearAllData];
    [self.wagerNavigationCoontroller popToRootViewControllerAnimated:NO];
}

- (void) loadCurrentBets
{
    [self.wagerNavigationCoontroller popToRootViewControllerAnimated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardMyBets] forKey:@"viewNumber"]];
}
- (NSString *) selectedValuesForIndex:(NSUInteger)index
{
    
    if (WagerRace == index)
    {
        return [[ZLSelectedValues sharedInstance] selectedRace];
    }
    else if(WagerAmount == index && ![self isAmoutViewLoaded])
    {
        return [[ZLSelectedValues sharedInstance] selectedAmount];
    }
    else if(WagerBetType == index){
        return [[ZLSelectedValues sharedInstance] selectedBetType];
    }
    else if(WagerTrack == index){
        return [[ZLSelectedValues sharedInstance] selectedTrack];
    }
    return nil;
}

- (UIImage *) imageForIndex:(NSUInteger)index
{
    if ([self isRunnersSelected] && index == WagerRunners) {
        return nil;
    }
    else if(index == WagerAmount && [self isAmoutViewLoaded]){
        return nil;
    }
    NSMutableDictionary *dic = [self.sideMenuDetailsArray objectAtIndex:index];
    return [UIImage imageNamed:[dic valueForKey:@"imageName"]];
}

- (NSString *) titleForIndex:(NSUInteger)index
{
    NSMutableDictionary *dic = [self.sideMenuDetailsArray objectAtIndex:index];
    NSString *selectedTrack = [[ZLSelectedValues sharedInstance] selectedTrack];
    
    if (WagerTrack == index && selectedTrack)
    {
        return [[ZLSelectedValues sharedInstance] selectedTrack];
    }
    else if ([self isRunnersSelected] && index == WagerRunners) {
        return nil;
    }
    else if(index == WagerAmount && [self isAmoutViewLoaded]){
        return nil;
    }
    return [dic valueForKey:@"title"];
}

- (UIImage *) backgroundImage:(NSUInteger)index
{
    if ([self isRunnersSelected] && index == WagerRunners) {
        return [self imageForPlaceHolder];
    }
    else if(index == WagerAmount && [self isAmoutViewLoaded]){
        return [UIImage imageNamed:@"arrow_black.png"];
    }
    else if (index == self.sideMenu.whichViewLoaded)
    {
        return [UIImage imageNamed:@"selectedsidemenu-4.png"];
    }
    else if (index < self.sideMenu.whichViewLoaded)
    {
        return [UIImage imageNamed:@"selectaafterbg.png"];
    }
    else
    {
        return [UIImage imageNamed:@"sidebar_bg_1field.png"];
    }

}

- (UIImage *)imageForPlaceHolder
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"placewagerbgutton.png"]];
    UIView *view = [[UIView alloc] initWithFrame:imageView.bounds];
    [view addSubview:imageView];
    imageView=nil;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 11, 53, 19)];
    [label setTextColor:[UIColor whiteColor]];
    [label setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextAlignment:NSTextAlignmentRight];
    [label setText:[[ZLSelectedValues sharedInstance] selectedAmount]];
    [view addSubview:label];
    label=nil;

    
    
    CGSize pageSize = view.frame.size;
    UIGraphicsBeginImageContext(pageSize);
        
    CGContextRef resizedContext = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:resizedContext];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (BOOL) isRunnersSelected
{
    for (NSMutableArray *array in [[ZLSelectedValues sharedInstance] selectedRunnersArray]) {
        if (array.count > 0) {
            return YES;
        }
    }
    
    return NO;
}


- (void)clearSelectedValuesWithIndex:(NSInteger)index
{
    ZLSelectedValues *selectedValue = [ZLSelectedValues sharedInstance];
    
    if (index < WagerBetType)
    {
        selectedValue.selectedBetType = nil;
    }
    
    if (index < WagerAmount) {
        selectedValue.selectedAmount = nil;
    }
    
    if (index < WagerRace) {
        selectedValue.selectedRace = nil;
    }
    
    if (index < WagerTrack) {
        selectedValue.selectedTrack = nil;
        selectedValue.selectedMTP = nil;
    }
    
    if (index < WagerRunners) {
        selectedValue.selectedRunnersArray = nil;
    }
}


- (IBAction)switchBetweenListAndGried:(id)sender
{
    if ([self.listGridButton isSelected])
    {
        [self.listGridButton setSelected:NO];
    }
    else{
        [self.listGridButton setSelected:YES];
    }
    
    NSArray *viewControllersArray = self.wagerNavigationCoontroller.viewControllers;
    if (viewControllersArray.count > WagerRunners)
    {
        ZLRunnersViewController *vc = (ZLRunnersViewController *)[viewControllersArray objectAtIndex:WagerRunners];
        [vc switchBetweenListAndGrid];
    }
}

- (IBAction)homeButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardHome] forKey:@"viewNumber"]];
}


- (void) viewDidUnload
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.sideMenu=nil;
    self.sideMenuDetailsArray=nil;
    self.amountButton=nil;
    self.amountsArray=nil;
    self.homeButton=nil;
    self.listGridButton=nil;
    self.objZLConformationViewController=nil;
    self.sideMenu.itemsArray=nil;
    self.sideMenu.tilesArray=nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
