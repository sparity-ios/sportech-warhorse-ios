//
//  ZLBetTypeViewController.m
//  WarHorse
//
//  Created by Sparity on 7/9/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLBetTypeViewController.h"
#import "ZLBetType_CustomCell.h"
#import "ZLRunnersViewController.h"
#import "ZLSelectedValues.h"

@interface ZLBetTypeViewController ()

@end

@implementation ZLBetTypeViewController
@synthesize betType_CollectionView=_betType_CollectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];

    // Do any additional setup after loading the view from its nib.
    
    
    [self.betType_CollectionView registerClass:[ZLBetType_CustomCell class] forCellWithReuseIdentifier:@"MY_CELL"];

    self.singleHorseArray = [[NSMutableArray alloc] init];
    self.multiHorseArray = [[NSMutableArray alloc] init];
    self.multiRaceArray = [[NSMutableArray alloc] init];

    
    [self loadData];
}

- (void)loadData
{
    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setValue:@"WIN" forKey:@"BetType"];
    [dic1 setValue:[NSNumber numberWithBool:YES] forKey:@"isBig"];
    [dic1 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.singleHorseArray addObject:dic1];
    
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setValue:@"PLC" forKey:@"BetType"];
    [dic2 setValue:[NSNumber numberWithBool:YES] forKey:@"isBig"];
    [dic2 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.singleHorseArray addObject:dic2];
    
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setValue:@"SHW" forKey:@"BetType"];
    [dic3 setValue:[NSNumber numberWithBool:YES] forKey:@"isBig"];
    [dic3 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.singleHorseArray addObject:dic3];
    
    NSMutableDictionary *dic4=[NSMutableDictionary dictionary];
    [dic4 setValue:@"WP" forKey:@"BetType"];
    [dic4 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic4 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.singleHorseArray addObject:dic4];
    
    NSMutableDictionary *dic5=[NSMutableDictionary dictionary];
    [dic5 setValue:@"WS" forKey:@"BetType"];
    [dic5 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic5 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.singleHorseArray addObject:dic5];
    
    NSMutableDictionary *dic6=[NSMutableDictionary dictionary];
    [dic6 setValue:@"EXA" forKey:@"BetType"];
    [dic6 setValue:[NSNumber numberWithBool:YES] forKey:@"isBig"];
    [dic6 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic6];
    
    NSMutableDictionary *dic7=[NSMutableDictionary dictionary];
    [dic7 setValue:@"EBX" forKey:@"BetType"];
    [dic7 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic7 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic7];
    
    NSMutableDictionary *dic8=[NSMutableDictionary dictionary];
    [dic8 setValue:@"QNL" forKey:@"BetType"];
    [dic8 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic8 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic8];
    
    NSMutableDictionary *dic9=[NSMutableDictionary dictionary];
    [dic9 setValue:@"QBX" forKey:@"BetType"];
    [dic9 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic9 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic9];
    
    NSMutableDictionary *dic10=[NSMutableDictionary dictionary];
    [dic10 setValue:@"TRI" forKey:@"BetType"];
    [dic10 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic10 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic10];
    
    NSMutableDictionary *dic11=[NSMutableDictionary dictionary];
    [dic11 setValue:@"TBX" forKey:@"BetType"];
    [dic11 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic11 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic11];
    
    NSMutableDictionary *dic12=[NSMutableDictionary dictionary];
    [dic12 setValue:@"DBL" forKey:@"BetType"];
    [dic12 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic12 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    
    [self.multiHorseArray addObject:dic12];
    
    NSMutableDictionary *dic13=[NSMutableDictionary dictionary];
    [dic13 setValue:@"PK3" forKey:@"BetType"];
    [dic13 setValue:[NSNumber numberWithBool:YES] forKey:@"isBig"];
    [dic13 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.multiRaceArray addObject:dic13];
    
    NSMutableDictionary *dic14=[NSMutableDictionary dictionary];
    [dic14 setValue:@"PK4" forKey:@"BetType"];
    [dic14 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic14 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.multiRaceArray addObject:dic14];
    
    NSMutableDictionary *dic15=[NSMutableDictionary dictionary];
    [dic15 setValue:@"CH6" forKey:@"BetType"];
    [dic15 setValue:[NSNumber numberWithBool:NO] forKey:@"isBig"];
    [dic15 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.multiRaceArray addObject:dic15];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark--
#pragma UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return self.singleHorseArray.count;
    }
    else if (section == 1){
        return self.multiHorseArray.count;
    }
    else{
        return self.multiRaceArray.count;
    }
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 3;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZLBetType_CustomCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    
    NSMutableDictionary *dic = nil;
    
    if (indexPath.section == 0)
    {
        dic = [self.singleHorseArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor colorWithRed:69.0/255 green:138.0/255 blue:234.0/255 alpha:1.0];
    }
    else if (indexPath.section == 1){

        dic = [self.multiHorseArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor colorWithRed:92.0/255 green:140.0/255 blue:26.0/255 alpha:1.0];
    }
    else
    {
        dic = [self.multiRaceArray objectAtIndex:indexPath.row];
        cell.backgroundColor = [UIColor colorWithRed:236.0/255 green:106.0/255 blue:62.0/255 alpha:1.0];
    }
    
    if ([[dic valueForKey:@"BetType"] isEqualToString:[[ZLSelectedValues sharedInstance] selectedBetType]]) {
        cell.backgroundColor = [UIColor blackColor];
    }
    
    cell.betType_Label.text = [dic valueForKey:@"BetType"];
    cell.betType_Label.center = CGPointMake(cell.frame.size.width/2, cell.frame.size.height/2);
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dic = nil;
    
    if (indexPath.section == 0) {
        dic = [self.singleHorseArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1){
        dic = [self.multiHorseArray objectAtIndex:indexPath.row];
    }
    else{
        dic = [self.multiRaceArray objectAtIndex:indexPath.row];
    }
    if ([[dic valueForKey:@"isBig"] boolValue]) {
        return CGSizeMake(111.0, 47);
    }
    else{
        return CGSizeMake(53.0, 47);
    }
}



//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(5, 5, 5, 5);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView reloadData];
    
    NSMutableDictionary *dic = nil;
    if (indexPath.section == 0) {
        dic = [self.singleHorseArray objectAtIndex:indexPath.row];
    }
    else if (indexPath.section == 1){
        dic = [self.multiHorseArray objectAtIndex:indexPath.row];
    }
    else{
        dic = [self.multiRaceArray objectAtIndex:indexPath.row];
    }
    [[ZLSelectedValues sharedInstance] setSelectedBetType:[dic valueForKey:@"BetType"]];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerBetType] forKey:@"currentLoadedIndex"]];

    
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


-(void)viewDidUnload{
    self.betType_CollectionView=nil;
    self.titleLabel=nil;
}

@end
