//
//  ZLBetTypeViewController.h
//  WarHorse
//
//  Created by Sparity on 7/9/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLBetTypeViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
@property(strong,nonatomic) IBOutlet UICollectionView *betType_CollectionView;
@property (nonatomic, strong) NSMutableArray *singleHorseArray;
@property (nonatomic, strong) NSMutableArray *multiHorseArray;
@property (nonatomic, strong) NSMutableArray *multiRaceArray;
@property(strong,nonatomic) IBOutlet UILabel *titleLabel;

@end
