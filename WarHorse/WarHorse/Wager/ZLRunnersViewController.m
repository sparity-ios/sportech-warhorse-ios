//
//  ZLTileComponentViewController.m
//  WarHorse
//
//  Created by Sparity on 7/9/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLRunnersViewController.h"
#import "ZLRunners_CustomCell.h"
#import "ZLSelectedValues.h"
#import "ZLOddsBoardViewController.h"

#define SMALL_BOTTOM_VIEW_HEIGHT 46
#define BIG_BOTTOM_VIEW_HEIGHT  223
#define NUMBER_TO_INCREASE_HEIGHT 20
#define NUMBER_TO_INCREASE_CELL (([[UIScreen mainScreen] bounds].size.height-568)?9:12)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
@interface ZLRunnersViewController ()

@end

@implementation ZLRunnersViewController
@synthesize numbers_Array=_numbers_Array;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    [self.sideMenuButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.tile_CollectionView registerClass:[ZLRunners_CustomCell class] forCellWithReuseIdentifier:@"MY_CELL"];
    
    _numbers_Array=[[NSMutableArray alloc]init];
    [self loadData];
    
    NSMutableArray *array = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    for (int i = 0; i < self.numbers_Array.count; i++) {
        [array addObject:[NSMutableArray array]];
    }


    NSString *selectedBetType = [[ZLSelectedValues sharedInstance] selectedBetType];
    
    int numberOfLegs = 0;
    int legsInPage = 0;
    if ([selectedBetType isEqualToString:@"EXA"]) {
        numberOfLegs = 2;
        legsInPage = 6;
    }
    else if([selectedBetType isEqualToString:@"CH6"]){
        numberOfLegs = 6;
        legsInPage = 6;
    }
    else{
        numberOfLegs = 20;
        legsInPage = 10;
    }
    
    CGRect rect = CGRectMake(5, self.navigationController.view.frame.size.height - 117, 226, 32.5);
    if (![selectedBetType isEqualToString:@"WIN"]) {
        
        UISwipeGestureRecognizer *swipeLeft;
        swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(Gest_SwipedLeft:)];
        [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
        [self.view addGestureRecognizer:swipeLeft];
        
        UISwipeGestureRecognizer *swipeRight;
        swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(Gest_SwipedRight:)];
        [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
        [self.view addGestureRecognizer:swipeRight];
        
        
        self.legSelection = [[AKLegSelection alloc] initWithFrame:rect totalnumberOfLegs:numberOfLegs legsPerPage:legsInPage delegate:self];
        [self.legSelection setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
        [self.view addSubview:self.legSelection];
    }

    [self changeNumberOfItems];

    [self handleUIItems];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HandleTopRightButtons" object:self userInfo:nil];

}

- (void) handleUIItems
{
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.boxButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.withButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.allButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.clearButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    
    [self.raceNumberLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.postTimeLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.furlongsLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.weatherLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.amoutLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.betTypeLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.wagerLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.runnersLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

    
    [self.bottomView setFrame:CGRectMake(5, 383, 228, 46)];
    [self.view addSubview:self.bottomView];

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void) loadData{
    
    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setValue:@"1" forKey:@"horseNumber"];
    [dic1 setValue:@"11" forKey:@"oddNumber"];
    [dic1 setValue:@"Top Partner" forKey:@"title"];
    [dic1 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic1 setValue:@"Victor(T)" forKey:@"couchName"];
    [dic1 setValue:@"124lbs" forKey:@"lbs"];
    [dic1 setValue:@"BA" forKey:@"bb"];
    [dic1 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic1 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic1];
    
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setValue:@"2" forKey:@"horseNumber"];
    [dic2 setValue:@"12" forKey:@"oddNumber"];
    [dic2 setValue:@"Ima Speedy Guy" forKey:@"title"];
    [dic2 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic2 setValue:@"Jr, Abel(T)" forKey:@"couchName"];
    [dic2 setValue:@"122lbs" forKey:@"lbs"];
    [dic2 setValue:@"BB" forKey:@"bb"];
    [dic2 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic2 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic2];
    
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setValue:@"3" forKey:@"horseNumber"];
    [dic3 setValue:@"13" forKey:@"oddNumber"];
    [dic3 setValue:@"For TheAges" forKey:@"title"];
    [dic3 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic3 setValue:@"Daniel(T)" forKey:@"couchName"];
    [dic3 setValue:@"124lbs" forKey:@"lbs"];
    [dic3 setValue:@"BA" forKey:@"bb"];
    [dic3 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic3 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic3];
    
    NSMutableDictionary *dic4=[NSMutableDictionary dictionary];
    [dic4 setValue:@"4" forKey:@"horseNumber"];
    [dic4 setValue:@"14" forKey:@"oddNumber"];
    [dic4 setValue:@"Be Rapid" forKey:@"title"];
    [dic4 setValue:@"Rose(J)" forKey:@"jocyName"];
    [dic4 setValue:@"Jermey(T)" forKey:@"couchName"];
    [dic4 setValue:@"123lbs" forKey:@"lbs"];
    [dic4 setValue:@"BA" forKey:@"bb"];
    [dic4 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic4 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic4];
    
    NSMutableDictionary *dic5=[NSMutableDictionary dictionary];
    [dic5 setValue:@"5" forKey:@"horseNumber"];
    [dic5 setValue:@"5/2" forKey:@"oddNumber"];
    [dic5 setValue:@"Feet Included" forKey:@"title"];
    [dic5 setValue:@"Garcia(J)" forKey:@"jocyName"];
    [dic5 setValue:@"Luis(T)" forKey:@"couchName"];
    [dic5 setValue:@"124lbs" forKey:@"lbs"];
    [dic5 setValue:@"BA" forKey:@"bb"];
    [dic5 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic5 setObject:[UIColor colorWithRed:9.0/256 green:253.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic5];
    
    NSMutableDictionary *dic6=[NSMutableDictionary dictionary];
    [dic6 setValue:@"6" forKey:@"horseNumber"];
    [dic6 setValue:@"16" forKey:@"oddNumber"];
    [dic6 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic6 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic6 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic6 setValue:@"126lbs" forKey:@"lbs"];
    [dic6 setValue:@"BA" forKey:@"bb"];
    [dic6 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic6 setObject:[UIColor colorWithRed:0.0/256 green:1.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic6];
    
    NSMutableDictionary *dic7=[NSMutableDictionary dictionary];
    [dic7 setValue:@"7" forKey:@"horseNumber"];
    [dic7 setValue:@"7/2" forKey:@"oddNumber"];
    [dic7 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic7 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic7 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic7 setValue:@"126lbs" forKey:@"lbs"];
    [dic7 setValue:@"BA" forKey:@"bb"];
    [dic7 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic7 setObject:[UIColor colorWithRed:251.0/256 green:135.0/256 blue:4.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic7];
    
    NSMutableDictionary *dic8=[NSMutableDictionary dictionary];
    [dic8 setValue:@"8" forKey:@"horseNumber"];
    [dic8 setValue:@"18" forKey:@"oddNumber"];
    [dic8 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic8 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic8 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic8 setValue:@"126lbs" forKey:@"lbs"];
    [dic8 setValue:@"BA" forKey:@"bb"];
    [dic8 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic8 setObject:[UIColor colorWithRed:247.0/256 green:226.0/256 blue:192.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic8];
    
    NSMutableDictionary *dic9=[NSMutableDictionary dictionary];
    [dic9 setValue:@"9" forKey:@"horseNumber"];
    [dic9 setValue:@"19" forKey:@"oddNumber"];
    [dic9 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic9 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic9 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic9 setValue:@"126lbs" forKey:@"lbs"];
    [dic9 setValue:@"BA" forKey:@"bb"];
    [dic9 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic9 setObject:[UIColor colorWithRed:0.0/256 green:135.0/256 blue:129.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic9];
    
    NSMutableDictionary *dic10=[NSMutableDictionary dictionary];
    [dic10 setValue:@"10" forKey:@"horseNumber"];
    [dic10 setValue:@"5/2" forKey:@"oddNumber"];
    [dic10 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic10 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic10 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic10 setValue:@"126lbs" forKey:@"lbs"];
    [dic10 setValue:@"BA" forKey:@"bb"];
    [dic10 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic10 setObject:[UIColor colorWithRed:130.0/256 green:4.0/256 blue:137.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic10];
    
    NSMutableDictionary *dic11=[NSMutableDictionary dictionary];
    [dic11 setValue:@"11" forKey:@"horseNumber"];
    [dic11 setValue:@"11" forKey:@"oddNumber"];
    [dic11 setValue:@"Top Partner" forKey:@"title"];
    [dic11 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic11 setValue:@"Victor(T)" forKey:@"couchName"];
    [dic11 setValue:@"124lbs" forKey:@"lbs"];
    [dic11 setValue:@"BA" forKey:@"bb"];
    [dic11 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic11 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic11];
    
    NSMutableDictionary *dic12=[NSMutableDictionary dictionary];
    [dic12 setValue:@"12" forKey:@"horseNumber"];
    [dic12 setValue:@"12" forKey:@"oddNumber"];
    [dic12 setValue:@"Ima Speedy Guy" forKey:@"title"];
    [dic12 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic12 setValue:@"Jr, Abel(T)" forKey:@"couchName"];
    [dic12 setValue:@"122lbs" forKey:@"lbs"];
    [dic12 setValue:@"BB" forKey:@"bb"];
    [dic12 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic12 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic12];
    
    NSMutableDictionary *dic13=[NSMutableDictionary dictionary];
    [dic13 setValue:@"13" forKey:@"horseNumber"];
    [dic13 setValue:@"13" forKey:@"oddNumber"];
    [dic13 setValue:@"For TheAges" forKey:@"title"];
    [dic13 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic13 setValue:@"Daniel(T)" forKey:@"couchName"];
    [dic13 setValue:@"124lbs" forKey:@"lbs"];
    [dic13 setValue:@"BA" forKey:@"bb"];
    [dic13 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic13 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic13];
    
    NSMutableDictionary *dic14=[NSMutableDictionary dictionary];
    [dic14 setValue:@"14" forKey:@"horseNumber"];
    [dic14 setValue:@"14" forKey:@"oddNumber"];
    [dic14 setValue:@"Be Rapid" forKey:@"title"];
    [dic14 setValue:@"Rose(J)" forKey:@"jocyName"];
    [dic14 setValue:@"Jermey(T)" forKey:@"couchName"];
    [dic14 setValue:@"123lbs" forKey:@"lbs"];
    [dic14 setValue:@"BA" forKey:@"bb"];
    [dic14 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic14 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic14];
    
    NSMutableDictionary *dic15=[NSMutableDictionary dictionary];
    [dic15 setValue:@"15" forKey:@"horseNumber"];
    [dic15 setValue:@"5/2" forKey:@"oddNumber"];
    [dic15 setValue:@"Feet Included" forKey:@"title"];
    [dic15 setValue:@"Garcia(J)" forKey:@"jocyName"];
    [dic15 setValue:@"Luis(T)" forKey:@"couchName"];
    [dic15 setValue:@"124lbs" forKey:@"lbs"];
    [dic15 setValue:@"BA" forKey:@"bb"];
    [dic15 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic15 setObject:[UIColor colorWithRed:9.0/256 green:253.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic15];
    
    NSMutableDictionary *dic16=[NSMutableDictionary dictionary];
    [dic16 setValue:@"16" forKey:@"horseNumber"];
    [dic16 setValue:@"16" forKey:@"oddNumber"];
    [dic16 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic16 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic16 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic16 setValue:@"126lbs" forKey:@"lbs"];
    [dic16 setValue:@"BA" forKey:@"bb"];
    [dic16 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic16 setObject:[UIColor colorWithRed:0.0/256 green:1.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic16];
    
    NSMutableDictionary *dic17=[NSMutableDictionary dictionary];
    [dic17 setValue:@"17" forKey:@"horseNumber"];
    [dic17 setValue:@"7/2" forKey:@"oddNumber"];
    [dic17 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic17 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic17 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic17 setValue:@"126lbs" forKey:@"lbs"];
    [dic17 setValue:@"BA" forKey:@"bb"];
    [dic17 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic17 setObject:[UIColor colorWithRed:251.0/256 green:135.0/256 blue:4.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic17];
    
    NSMutableDictionary *dic18=[NSMutableDictionary dictionary];
    [dic18 setValue:@"18" forKey:@"horseNumber"];
    [dic18 setValue:@"18" forKey:@"oddNumber"];
    [dic18 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic18 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic18 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic18 setValue:@"126lbs" forKey:@"lbs"];
    [dic18 setValue:@"BA" forKey:@"bb"];
    [dic18 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic18 setObject:[UIColor colorWithRed:247.0/256 green:226.0/256 blue:192.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic18];
    
    NSMutableDictionary *dic19=[NSMutableDictionary dictionary];
    [dic19 setValue:@"19" forKey:@"horseNumber"];
    [dic19 setValue:@"19" forKey:@"oddNumber"];
    [dic19 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic19 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic19 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic19 setValue:@"126lbs" forKey:@"lbs"];
    [dic19 setValue:@"BA" forKey:@"bb"];
    [dic19 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic19 setObject:[UIColor colorWithRed:0.0/256 green:135.0/256 blue:129.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic19];
    
    NSMutableDictionary *dic20=[NSMutableDictionary dictionary];
    [dic20 setValue:@"20" forKey:@"horseNumber"];
    [dic20 setValue:@"5/2" forKey:@"oddNumber"];
    [dic20 setValue:@"Broad Moon(L)" forKey:@"title"];
    [dic20 setValue:@"Curatolo(J)" forKey:@"jocyName"];
    [dic20 setValue:@"Ryan(T)" forKey:@"couchName"];
    [dic20 setValue:@"126lbs" forKey:@"lbs"];
    [dic20 setValue:@"BA" forKey:@"bb"];
    [dic20 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic20 setObject:[UIColor colorWithRed:130.0/256 green:4.0/256 blue:137.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic20];
    
    NSMutableDictionary *dic21=[NSMutableDictionary dictionary];
    [dic21 setValue:@"21" forKey:@"horseNumber"];
    [dic21 setValue:@"11" forKey:@"oddNumber"];
    [dic21 setValue:@"Top Partner" forKey:@"title"];
    [dic21 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic21 setValue:@"Victor(T)" forKey:@"couchName"];
    [dic21 setValue:@"124lbs" forKey:@"lbs"];
    [dic21 setValue:@"BA" forKey:@"bb"];
    [dic21 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic21 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic21];
    
    NSMutableDictionary *dic22=[NSMutableDictionary dictionary];
    [dic22 setValue:@"22" forKey:@"horseNumber"];
    [dic22 setValue:@"12" forKey:@"oddNumber"];
    [dic22 setValue:@"Ima Speedy Guy" forKey:@"title"];
    [dic22 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic22 setValue:@"Jr, Abel(T)" forKey:@"couchName"];
    [dic22 setValue:@"122lbs" forKey:@"lbs"];
    [dic22 setValue:@"BB" forKey:@"bb"];
    [dic22 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic22 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic22];
    
    NSMutableDictionary *dic23=[NSMutableDictionary dictionary];
    [dic23 setValue:@"23" forKey:@"horseNumber"];
    [dic23 setValue:@"13" forKey:@"oddNumber"];
    [dic23 setValue:@"For TheAges" forKey:@"title"];
    [dic23 setValue:@"Carrasco(J)" forKey:@"jocyName"];
    [dic23 setValue:@"Daniel(T)" forKey:@"couchName"];
    [dic23 setValue:@"124lbs" forKey:@"lbs"];
    [dic23 setValue:@"BA" forKey:@"bb"];
    [dic23 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic23 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic23];
    
    NSMutableDictionary *dic24=[NSMutableDictionary dictionary];
    [dic24 setValue:@"24" forKey:@"horseNumber"];
    [dic24 setValue:@"14" forKey:@"oddNumber"];
    [dic24 setValue:@"Be Rapid" forKey:@"title"];
    [dic24 setValue:@"Rose(J)" forKey:@"jocyName"];
    [dic24 setValue:@"Jermey(T)" forKey:@"couchName"];
    [dic24 setValue:@"123lbs" forKey:@"lbs"];
    [dic24 setValue:@"BA" forKey:@"bb"];
    [dic24 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic24 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.numbers_Array addObject:dic24];
}



- (void) handleHeightOfGridOrTableView
{
    if (!IS_IPHONE5) {
        if (self.numberOfItems > NUMBER_TO_INCREASE_HEIGHT) {
            [self.navigationController.view setFrame:CGRectMake(0, 0, self.navigationController.view.frame.size.width, 460)];
            
            self.legSelection.frame = CGRectMake(self.legSelection.frame.origin.x, self.navigationController.view.frame.size.height - 117, self.legSelection.frame.size.width, self.legSelection.frame.size.height);
            self.sideMenuButton.hidden = NO;
            CGRect rect = self.titleLabel.frame;
            rect.origin.x = 37.5;
            self.titleLabel.frame = rect;
        }
        else{
            [self.navigationController.view setFrame:CGRectMake(0, 46, self.navigationController.view.frame.size.width, 460 - 46)];
            self.legSelection.frame = CGRectMake(self.legSelection.frame.origin.x, self.navigationController.view.frame.size.height - 117, self.legSelection.frame.size.width, self.legSelection.frame.size.height);
            self.sideMenuButton.hidden = YES;
            
            CGRect rect = self.titleLabel.frame;
            rect.origin.x = 8;
            self.titleLabel.frame = rect;
        }

    }
}

- (IBAction)sideMenuButtonClicked:(id)sender
{
    
}


- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
//    if (self.numbers_Array.count > NUMBER_TO_INCREASE_HEIGHT)
//    {
//        [self.navigationController.view setFrame:CGRectMake(0, 46, self.navigationController.view.frame.size.width, self.view.frame.size.height - 46)];
//    }
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"HandleTopRightButtons" object:self userInfo:nil];
}

- (NSUInteger) totalNumberOfLegs
{
    return 20;
}
- (NSUInteger) numberOfLegsInPage
{
    return 10;
}


-(void)Gest_SwipedLeft:(UISwipeGestureRecognizer *) sender
{
    if (self.legSelection.currentLeg < self.legSelection.totalLegs)
    {
        self.legSelection.currentLeg =  self.legSelection.currentLeg + 1;
    }
    
    [self changeNumberOfItems];

}

-(void)Gest_SwipedRight:(UISwipeGestureRecognizer *) sender
{
    if (self.legSelection.currentLeg > 0)
    {
        self.legSelection.currentLeg =  self.legSelection.currentLeg - 1;
    }
    
    [self changeNumberOfItems];
}

- (void) LegSelection:(id)LegSelection didSelect:(int)selectedLeg
{
    [self changeNumberOfItems];
}

- (void) changeNumberOfItems
{
    NSString *selectedBetType = [[ZLSelectedValues sharedInstance] selectedBetType];
    
    if ([selectedBetType isEqualToString:@"EXA"])
    {
        if (self.legSelection.currentLeg == 0)
        {
            self.numberOfItems = 10;
        }
        else
        {
            self.numberOfItems = 15;
        }
    }
    else if([selectedBetType isEqualToString:@"CH6"])
    {
        if (self.legSelection.currentLeg == 0)
        {
            self.numberOfItems = 15;
        }
        else if (self.legSelection.currentLeg == 1)
        {
            self.numberOfItems = 24;
        }
        else if (self.legSelection.currentLeg == 2)
        {
            self.numberOfItems = 6;
        }
        else if (self.legSelection.currentLeg == 3)
        {
            self.numberOfItems = 12;
        }
        else if (self.legSelection.currentLeg == 4)
        {
            self.numberOfItems = 9;
        }
        else if (self.legSelection.currentLeg == 5)
        {
            self.numberOfItems = 11;
        }
        else
        {
            self.numberOfItems = 15;
        }
    }
    else if ([selectedBetType isEqualToString:@"WIN"])
    {
        self.numberOfItems = 16;
    }
    else
    {
        self.numberOfItems = [self.numbers_Array count];
    }
    
    [self reloadViews];
    [self handleHeightOfGridOrTableView];
}

- (void) reloadViews
{
    if (self.runnersTableView.hidden)
    {
        [self.tile_CollectionView reloadData];
    }
    else
    {
        [self.runnersTableView reloadData];
    }
}


- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.numberOfItems;
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZLRunners_CustomCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];

    NSMutableDictionary *dic = [self.numbers_Array objectAtIndex:indexPath.row];
    cell.horseNum_Label.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"horseNumber"]];
    cell.horseNum_Label.textColor = [dic valueForKey:@"textColor"];
    cell.horseNum_Label.backgroundColor = [dic valueForKey:@"backgroundColor"];
    cell.oddNum_Label.text = [NSString stringWithFormat:@"%@",[dic valueForKey:@"oddNumber"]];

    if ([self isRunnerAvailableInList:[dic valueForKey:@"horseNumber"] leg:self.legSelection.currentLeg])
    {
        cell.backgroundLabel.backgroundColor = [dic valueForKey:@"backgroundColor"];
        cell.backgroundLabel.frame = cell.bounds;

    
        cell.oddNum_Label.frame =  CGRectMake(-4, cell.frame.size.height-14, cell.frame.size.width, 12);
        cell.oddNum_Label.backgroundColor = [UIColor clearColor];
        cell.oddNum_Label.textColor = [dic valueForKey:@"textColor"];
        cell.topImageView.image = [UIImage imageNamed:@"46-check (1).png"];
        cell.topImageView.frame = cell.bounds;
        
        CGRect rect = cell.oddNum_Label.frame;
        rect.origin.x = 5;
        rect.origin.y -= 8;
        rect.size.width -= 5;
        rect.size.height += 6;
        cell.horseNum_Label.frame = rect;
        cell.horseNum_Label.textAlignment = NSTextAlignmentLeft;
    }
    else
    {
        cell.backgroundLabel.backgroundColor = [UIColor clearColor];
        
        cell.horseNum_Label.frame = CGRectMake(0, 0.0, cell.frame.size.width, cell.frame.size.height-13);
        cell.horseNum_Label.textAlignment = NSTextAlignmentCenter;
        
        cell.oddNum_Label.frame =  CGRectMake(0, cell.frame.size.height-12, cell.frame.size.width, 12);
        cell.oddNum_Label.backgroundColor = [UIColor whiteColor];
        cell.oddNum_Label.textColor = [UIColor blackColor];
        cell.topImageView.image = nil;
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.numberOfItems > NUMBER_TO_INCREASE_CELL) {
        return CGSizeMake(52, 46);
    }
    else
    {
        return CGSizeMake(72, 69);
    }
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(6, 6, 6, 6);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZLRunners_CustomCell *cell = (ZLRunners_CustomCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    [self addOrRemoveRunnerFromResultsArray:cell.horseNum_Label.text leg:self.legSelection.currentLeg];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerRunners] forKey:@"currentLoadedIndex"]];
    
    [collectionView reloadData];
    
    [self setTextToTextView];

}

- (void) addOrRemoveRunnerFromResultsArray:(NSString *)runner leg:(int)leg
{
    NSMutableArray *legsArray = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    
    NSMutableArray *array = nil;
    
    if (legsArray.count > leg) {
        array = [legsArray objectAtIndex:leg];
    }
    else
    {
        array = [NSMutableArray array];
    }
    
    if ([self isRunnerAvailableInList:runner leg:leg])
    {
        [array removeObject:runner];
    }
    else
    {
        [array addObject:runner];
    }
    
    if (legsArray.count > leg) {
        [legsArray replaceObjectAtIndex:leg withObject:array];
    }
    else
    {
        [legsArray insertObject:array atIndex:leg];
    }
    [[ZLSelectedValues sharedInstance] setSelectedRunnersArray:legsArray];
}

- (BOOL) isRunnerAvailableInList:(NSString *)runner leg:(int)leg
{
    NSMutableArray *legsArray = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    if (legsArray.count > leg) {
        NSMutableArray *array = [legsArray objectAtIndex:leg];
        if ([array containsObject:runner]) {
            return YES;
        }
        else
        {
            return NO;
        }

    }
    return NO;
    
}


- (IBAction)bottomViewExpanstionOrCompression:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    [UIView beginAnimations:@"AdWhirlIn" context:nil];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    if ([button isSelected])
    {
        [button setSelected:NO];
        
        self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y + 177, self.bottomView.frame.size.width, 46);
        [self.belowResultsView setHidden:YES];
        [self.selectedValuesTextView setHidden:NO];
    }
    else
    {
        [button setSelected:YES];        
        
        self.bottomView.frame = CGRectMake(self.bottomView.frame.origin.x, self.bottomView.frame.origin.y - 177, self.bottomView.frame.size.width, BIG_BOTTOM_VIEW_HEIGHT);
        
        [self.belowResultsView setHidden:NO];
        [self.selectedValuesTextView setHidden:YES];
    }
    
    [UIView commitAnimations];

}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return self.numberOfItems;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.objZLRunnersViewCell  = (ZLRunnersViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.objZLRunnersViewCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLRunnersViewCell" owner:self options:nil];
        
    }
  
    [self.objZLRunnersViewCell.runnerNumberLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
    [self.objZLRunnersViewCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.objZLRunnersViewCell.jocyNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.objZLRunnersViewCell.couchNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.objZLRunnersViewCell.lbsNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.objZLRunnersViewCell.bbLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.objZLRunnersViewCell.oddNum_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

 
    NSMutableDictionary *dic = [self.numbers_Array objectAtIndex:indexPath.row];
    
    [self.objZLRunnersViewCell.runnerNumberLabel setText:[dic valueForKey:@"horseNumber"]];
    [self.objZLRunnersViewCell.runnerNumberLabel setBackgroundColor:[dic valueForKey:@"backgroundColor"]];
    [self.objZLRunnersViewCell.runnerNumberLabel setTextColor:[dic valueForKey:@"textColor"]];


    [self.objZLRunnersViewCell.titleLabel setText:[dic valueForKey:@"title"]];
    [self.objZLRunnersViewCell.jocyNameLabel setText:[dic valueForKey:@"jocyName"]];
    [self.objZLRunnersViewCell.couchNameLabel setText:[dic valueForKey:@"couchName"]];
    [self.objZLRunnersViewCell.lbsNameLabel setText:[dic valueForKey:@"lbs"]];
    [self.objZLRunnersViewCell.bbLabel setText:[dic valueForKey:@"bb"]];

    [self.objZLRunnersViewCell.oddNum_Label setText:[dic valueForKey:@"oddNumber"]];
    
    
    if ([self isRunnerAvailableInList:[dic valueForKey:@"horseNumber"] leg:self.legSelection.currentLeg])
    {
        [self.objZLRunnersViewCell.titleLabel setTextColor:[dic valueForKey:@"textColor"]];
        [self.objZLRunnersViewCell.jocyNameLabel setTextColor:[dic valueForKey:@"textColor"]];
        [self.objZLRunnersViewCell.couchNameLabel setTextColor:[dic valueForKey:@"textColor"]];
        [self.objZLRunnersViewCell.lbsNameLabel setTextColor:[dic valueForKey:@"textColor"]];
        [self.objZLRunnersViewCell.bbLabel setTextColor:[dic valueForKey:@"textColor"]];
        [self.objZLRunnersViewCell.oddNum_Label setTextColor:[dic valueForKey:@"textColor"]];

        [self.objZLRunnersViewCell.checkImageView setImage:[UIImage imageNamed:@"tickmark.png"]];
        [self.objZLRunnersViewCell.background setBackgroundColor:[dic valueForKey:@"backgroundColor"]];
    }
    else
    {
        [self.objZLRunnersViewCell.titleLabel setTextColor:[UIColor colorWithRed:0.247058 green:0.247058 blue:0.247058 alpha:1.0]];
        [self.objZLRunnersViewCell.jocyNameLabel setTextColor:[UIColor colorWithRed:0.549019 green:0.549019 blue:0.549019 alpha:1.0]];
        [self.objZLRunnersViewCell.couchNameLabel setTextColor:[UIColor colorWithRed:0.549019 green:0.549019 blue:0.549019 alpha:1.0]];
        [self.objZLRunnersViewCell.lbsNameLabel setTextColor:[UIColor colorWithRed:0.549019 green:0.549019 blue:0.549019 alpha:1.0]];
        [self.objZLRunnersViewCell.bbLabel setTextColor:[UIColor colorWithRed:0.549019 green:0.549019 blue:0.549019 alpha:1.0]];
        [self.objZLRunnersViewCell.oddNum_Label setTextColor:[UIColor colorWithRed:0.1176470 green:0.1176470 blue:0.1176470 alpha:1.0]];

        [self.objZLRunnersViewCell.checkImageView setImage:nil];
        [self.objZLRunnersViewCell.background setBackgroundColor:[UIColor clearColor]];
    }

    return self.objZLRunnersViewCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 56;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic = [self.numbers_Array objectAtIndex:indexPath.row];

    
    [self addOrRemoveRunnerFromResultsArray:[dic valueForKey:@"horseNumber"] leg:self.legSelection.currentLeg];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerRunners] forKey:@"currentLoadedIndex"]];
    
    [tableView reloadData];

    [self setTextToTextView];
}


- (void) switchBetweenListAndGrid
{
    if (self.tile_CollectionView.hidden)
    {
        self.tile_CollectionView.hidden = NO;
        self.runnersTableView.hidden = YES;
        [self.tile_CollectionView reloadData];
    }
    else
    {
        self.tile_CollectionView.hidden = YES;
        self.runnersTableView.hidden = NO;
        [self.runnersTableView reloadData];
    }
}

- (IBAction)listOrGridButtonClicked:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([button isSelected])
    {
        [button setSelected:NO];
        self.tile_CollectionView.hidden = NO;
        self.runnersTableView.hidden = YES;
        [self.tile_CollectionView reloadData];
    }
    else
    {
        [button setSelected:YES];
        self.tile_CollectionView.hidden = YES;
        self.runnersTableView.hidden = NO;
        [self.runnersTableView reloadData];
    }
}


- (IBAction)allButtonClicked:(id)sender
{
    NSMutableArray *legsArray = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    
    NSMutableArray *array = [legsArray objectAtIndex:self.legSelection.currentLeg];
    [array removeAllObjects];
    
    for (NSMutableDictionary *dic in self.numbers_Array) {
        [array addObject:[dic valueForKey:@"horseNumber"]];
    }
    
    [self reloadViews];

}
- (IBAction)clearButtonClicked:(id)sender
{
    NSMutableArray *legsArray = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    
    NSMutableArray *array = [legsArray objectAtIndex:self.legSelection.currentLeg];
    
    [array removeAllObjects];

    
    [self reloadViews];
}

- (void)setTextToTextView
{
    NSMutableArray *selectedArray = [[ZLSelectedValues sharedInstance] selectedRunnersArray];
    NSMutableString *textString = [NSMutableString string];
    for (NSMutableArray *array in selectedArray)
    {
        for (NSString *string in array)
        {
            [textString appendString:string];
            [textString appendString:@","];
        }
        if (array.count > 0)
        {
            [textString deleteCharactersInRange:NSMakeRange([textString length]-1, 1)];
            [textString appendString:@" / "];
        }
    }
    if (textString.length > 0)
    {
        [textString deleteCharactersInRange:NSMakeRange([textString length]-2, 2)];
    }

    [self.selectedValuesTextView setText:textString];
}


- (IBAction)oddsButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadOddsView" object:self userInfo:nil];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
