//
//  ZLConformationViewController.m
//  WarHorse
//
//  Created by Sparity on 13/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLPlaceWagerResultsViewController.h"
#import "ZLSelectedValues.h"

@interface ZLPlaceWagerResultsViewController ()

@end

@implementation ZLPlaceWagerResultsViewController

@synthesize delegate = _delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75]];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.raceNumberLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:21]];
    [self.dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.trackNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.leftPriseLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.betTypeLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.runnersLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.amountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.numberOfBetsLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.totalAmountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.idLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.bottomDateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];

    [self.buttonRepeat.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.buttonNew.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.buttonBetSameRace.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.buttonGoToTrack.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.buttonVideo.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.buttonCurrentBets.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];

    [self.totalLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

    
    ZLSelectedValues *selected = [ZLSelectedValues sharedInstance];
    
    [self.raceNumberLabel setText:[NSString stringWithFormat:@"Race %@",selected.selectedRace]];
    [self.trackNameLabel setText:selected.selectedTrack];
    [self.leftPriseLabel setText:selected.selectedAmount];
    [self.betTypeLabel setText:selected.selectedBetType];

}

- (IBAction)newWagerButtonClicked:(id)sender
{
    [self.view removeFromSuperview];
    [self.delegate newWager];
}

- (IBAction)repeatWagerButtonClicked:(id)sender
{
    [self.view removeFromSuperview];
    [self.delegate repeatWager];
}
- (IBAction)currentBetsButtonClicked:(id)sender
{
    [self.view removeFromSuperview];
    [self.delegate loadCurrentBets];
}

- (IBAction)betSameRaceButtonClicked:(id)sender
{
    
}
- (IBAction)goToTrackButtonClicked:(id)sender
{
    
}
- (IBAction)videoButtonClicked:(id)sender
{
    
}

-(void)viewDidUnload{
    self.raceNumberLabel=nil;
    self.dateLabel=nil;
    self.trackNameLabel=nil;
    self.leftPriseLabel=nil;
    self.betTypeLabel=nil;
    self.runnersLabel=nil;
    self.amountLabel=nil;
    self.numberOfBetsLabel=nil;
    self.amountLabel=nil;
    self.titleLabel=nil;
    self.buttonRepeat=nil;
    self.buttonNew=nil;
    self.totalAmountLabel=nil;
    
    
 }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
