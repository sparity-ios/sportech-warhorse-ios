//
//  ZLConformationViewController.m
//  WarHorse
//
//  Created by Sparity on 06/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLConformationViewController.h"
#import "ZLSelectedValues.h"

@interface ZLConformationViewController ()

@end

@implementation ZLConformationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.view setBackgroundColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.75]];
    
    [self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.raceNumberLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:21]];
    [self.dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.trackNameLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.leftPriseLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.betTypeLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.runnersLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.amountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.numberOfBetsLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.totalAmountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.buttonConfirm.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.buttonCancel.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.totalLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.bottomDateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

    
    ZLSelectedValues *selected = [ZLSelectedValues sharedInstance];
    
    [self.raceNumberLabel setText:[NSString stringWithFormat:@"Race %@",selected.selectedRace]];
    [self.trackNameLabel setText:selected.selectedTrack];
    [self.leftPriseLabel setText:selected.selectedAmount];
    [self.betTypeLabel setText:selected.selectedBetType];

}

- (IBAction)conformButtonClicked:(id)sender
{
    [self.view removeFromSuperview];
    [self.delegate confirmWager:self];
}
- ( IBAction)cancelButtonClicked:(id)sender
{
    [self.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
