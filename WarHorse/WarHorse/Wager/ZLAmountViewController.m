//
//  ZLAmountViewController.m
//  WarHorse
//
//  Created by Sparity on 7/9/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLAmountViewController.h"
#import "ZLAmount_CustomCell.h"
#import "ZLBetTypeViewController.h"
#import "ZLSelectedValues.h"
#import <QuartzCore/QuartzCore.h>


@interface ZLAmountViewController ()

@end

@implementation ZLAmountViewController
@synthesize amount_Array=_amount_Array;
@synthesize bet_Label=_bet_Label;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _amount_Array=[[NSMutableArray alloc]init];
    
    [self loadData];

    [self.amount_CollectionView registerClass:[ZLAmount_CustomCell class] forCellWithReuseIdentifier:@"MY_CELL"];
    
    int numberOfRows = (self.amount_Array.count%4 == 0) ? self.amount_Array.count/4 : (self.amount_Array.count/4) + 1;
    
    if (numberOfRows > 6)
    {
        numberOfRows = 6;
    }
    
    
    [self.bet_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    
    
    self.customAmountView = [[ZLCustomAmountView alloc] initWithFrame:CGRectMake(0, 28, 236.5, 390)];
    self.customAmountView.hidden = YES;
    self.customAmountView.delegate = self;
    [self.view addSubview:self.customAmountView];
    
    
    self.defaultButton.layer.borderWidth = 1;
    self.defaultButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
    
    self.customButton.layer.borderWidth = 1;
    self.customButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
}




-(void)loadData{
    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setValue:@"$1" forKey:@"amountNumber"];
    [dic1 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.amount_Array addObject:dic1];
    
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setValue:@"$2" forKey:@"amountNumber"];
    [dic2 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.amount_Array addObject:dic2];
    
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setValue:@"$3" forKey:@"amountNumber"];
    [dic3 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic3];
    
    NSMutableDictionary *dic4=[NSMutableDictionary dictionary];
    [dic4 setValue:@"$5" forKey:@"amountNumber"];
    [dic4 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic4];
    
    NSMutableDictionary *dic5=[NSMutableDictionary dictionary];
    [dic5 setValue:@"$10" forKey:@"amountNumber"];
    [dic5 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic5];
    
    NSMutableDictionary *dic6=[NSMutableDictionary dictionary];
    [dic6 setValue:@"$20" forKey:@"amountNumber"];
    [dic6 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic6];
    
    NSMutableDictionary *dic7=[NSMutableDictionary dictionary];
    [dic7 setValue:@"$30" forKey:@"amountNumber"];
    [dic7 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic7];
    
    NSMutableDictionary *dic8=[NSMutableDictionary dictionary];
    [dic8 setValue:@"$50" forKey:@"amountNumber"];
    [dic8 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic8];
    
    NSMutableDictionary *dic9=[NSMutableDictionary dictionary];
    [dic9 setValue:@"$100" forKey:@"amountNumber"];
    [dic9 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic9];
    
    NSMutableDictionary *dic10=[NSMutableDictionary dictionary];
    [dic10 setValue:@"$200" forKey:@"amountNumber"];
    [dic10 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic10];
    
    NSMutableDictionary *dic11=[NSMutableDictionary dictionary];
    [dic11 setValue:@"$250" forKey:@"amountNumber"];
    [dic11 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic11];
    
    NSMutableDictionary *dic12=[NSMutableDictionary dictionary];
    [dic12 setValue:@"$300" forKey:@"amountNumber"];
    [dic12 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic12];
    
    NSMutableDictionary *dic13=[NSMutableDictionary dictionary];
    [dic13 setValue:@"$400" forKey:@"amountNumber"];
    [dic13 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic13];
    
    NSMutableDictionary *dic14=[NSMutableDictionary dictionary];
    [dic14 setValue:@"$450" forKey:@"amountNumber"];
    [dic14 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
    [self.amount_Array addObject:dic14];
    
//    NSMutableDictionary *dic15=[NSMutableDictionary dictionary];
//    [dic15 setValue:@"$500" forKey:@"amountNumber"];
//    [dic15 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];
//
//    [self.amount_Array addObject:dic15];
    
    NSMutableDictionary *dic16=[NSMutableDictionary dictionary];
    [dic16 setValue:@"$1000" forKey:@"amountNumber"];
    [dic16 setObject:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0/255.0f alpha:1.0] forKey:@"backGroundColor"];

    [self.amount_Array addObject:dic16];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark--
#pragma UICollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return [self.amount_Array count];
}
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ZLAmount_CustomCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"MY_CELL" forIndexPath:indexPath];
    
    NSMutableDictionary *dic = [self.amount_Array objectAtIndex:indexPath.row];
    cell.amount_Label.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"amountNumber"]];
    
    NSString *selectedAmount = [[ZLSelectedValues sharedInstance] selectedAmount];
    
    cell.layer.borderWidth = 0.5;
    cell.layer.borderColor = [UIColor colorWithRed:146.0/255 green:159.0/255 blue:165.0/255 alpha:1.0].CGColor;
    if ([selectedAmount isEqualToString:[dic valueForKey:@"amountNumber"]])
    {
        cell.backgroundColor = [UIColor blackColor];
        cell.amount_Label.textColor = [UIColor whiteColor];
    }
    else
    {
        cell.backgroundColor = [UIColor colorWithRed:207.0/255 green:220.0/255 blue:226.0/255 alpha:1.0];
        cell.amount_Label.textColor = [UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0/255.0f alpha:1.0];
    }
    
    
    return cell;
}
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(52, 48);
//}
//
//
//
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(5, 5, 5, 5);
//}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [collectionView reloadData];
    [[ZLSelectedValues sharedInstance] setIsAmountCustomSelected:NO];

    
    //Handling selections
    NSMutableDictionary *dic = [self.amount_Array objectAtIndex:indexPath.row];

    [[ZLSelectedValues sharedInstance] setSelectedAmount:[dic valueForKey:@"amountNumber"]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerAmount] forKey:@"currentLoadedIndex"]];

    
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


-(IBAction)defaultButtonClicked:(id)sender
{
    if (![self.defaultButton isSelected])
    {
        [self.defaultButton setSelected:YES];
        [self.customButton setSelected:NO];
        
        self.defaultButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
        self.customButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
        
        [self.defaultButton setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
        [self.customButton setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];

        self.customAmountView.hidden = YES;
        self.amount_CollectionView.hidden = NO;
    }
    else{
    }
}
-(IBAction)customButtonClicked:(id)sender
{
    if (![self.customButton isSelected])
    {
        [self.defaultButton setSelected:NO];
        [self.customButton setSelected:YES];
        
        self.defaultButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
        self.customButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;

        [self.customButton setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
        [self.defaultButton setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];
        
        self.customAmountView.hidden = NO;
        self.amount_CollectionView.hidden = YES;
    }
    else{

    }
}

#pragma mark -
#pragma CustomAmountDelegate Method

- (void) selectedAmount:(NSString *)amount
{
    
    [[ZLSelectedValues sharedInstance] setIsAmountCustomSelected:YES];
    
    [[ZLSelectedValues sharedInstance] setSelectedAmount:amount];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerAmount] forKey:@"currentLoadedIndex"]];

}

#pragma mark -

-(void)viewDidUnload{
    self.amount_CollectionView=nil;
    self.amount_Array=nil;
    self.bet_Label=nil;
}

@end
