//
//  ZLTrackViewController.m
//  WarHorse
//
//  Created by Sparity on 09/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLTrackViewController.h"
#import "ZLRaceViewController.h"
#import "ZLSelectedValues.h"
@interface ZLTrackViewController ()

@end

@implementation ZLTrackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.selectTrackLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

    [self loadData];
    [self.navigationController setNavigationBarHidden:YES];
    self.isFavourite=YES;
       
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.wagerTableView reloadData];
    
}

-(void)loadData
{
    _wager_Array=[[NSMutableArray alloc]init];
    
    NSMutableDictionary *wagerDict=[NSMutableDictionary dictionary];
    [wagerDict setValue:@"Beulah Park" forKey:@"raceTrackTitle"];
    [wagerDict setValue:@"6 Furlongs I All Weather Track Claiming($50,000-$40,000)" forKey:@"tackInfoLabel"];
    [wagerDict setValue:@"1" forKey:@"raceNumber"];
    [wagerDict setValue:@"OFF" forKey:@"MTP"];
    [wagerDict setObject:[UIColor colorWithRed:245.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict setObject:[UIColor whiteColor] forKey:@"mtpColor"];

    [self.wager_Array addObject:wagerDict];
    
    
    NSMutableDictionary *wagerDict1=[NSMutableDictionary dictionary];
    [wagerDict1 setValue:@"Belmont" forKey:@"raceTrackTitle"];
    [wagerDict1 setValue:@"5 Furlongs I All Weather Track Claiming($40,000)" forKey:@"tackInfoLabel"];
    [wagerDict1 setValue:@"7" forKey:@"raceNumber"];
    [wagerDict1 setValue:@"0" forKey:@"MTP"];
    [wagerDict1 setObject:[UIColor colorWithRed:245.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict1 setObject:[UIColor whiteColor] forKey:@"mtpColor"];

    [self.wager_Array addObject:wagerDict1];
    
    
    NSMutableDictionary *wagerDict2=[NSMutableDictionary dictionary];
    [wagerDict2 setValue:@"Mountaineer" forKey:@"raceTrackTitle"];
    [wagerDict2 setValue:@"5 Furlongs I Grass/Turf Claiming ($50,000)" forKey:@"tackInfoLabel"];
    [wagerDict2 setValue:@"2" forKey:@"raceNumber"];
    [wagerDict2 setValue:@"5" forKey:@"MTP"];
    [wagerDict2 setObject:[UIColor colorWithRed:250.0/255 green:228.0/255 blue:48.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict2 setObject:[UIColor blackColor] forKey:@"mtpColor"];
    [self.wager_Array addObject:wagerDict2];
    
    
    NSMutableDictionary *wagerDict3=[NSMutableDictionary dictionary];
    [wagerDict3 setValue:@"Parx Racing" forKey:@"raceTrackTitle"];
    [wagerDict3 setValue:@"7 Furlongs I Sand and Dirt Claiming ($12,000 - $10,000)" forKey:@"tackInfoLabel"];
    [wagerDict3 setValue:@"3" forKey:@"raceNumber"];
    [wagerDict3 setValue:@"13" forKey:@"MTP"];
    [wagerDict3 setObject:[UIColor colorWithRed:2.0/255 green:143.0/255 blue:26.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict3 setObject:[UIColor whiteColor] forKey:@"mtpColor"];
    [self.wager_Array addObject:wagerDict3];
    
    NSMutableDictionary *wagerDict4=[NSMutableDictionary dictionary];
    [wagerDict4 setValue:@"Remington Park" forKey:@"raceTrackTitle"];
    [wagerDict4 setValue:@"4 Furlongs I Yielding Claiming ($23,000)" forKey:@"tackInfoLabel"];
    [wagerDict4 setValue:@"1" forKey:@"raceNumber"];
    [wagerDict4 setValue:@"92" forKey:@"MTP"];
    [wagerDict4 setObject:[UIColor colorWithRed:2.0/255 green:143.0/255 blue:26.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict4 setObject:[UIColor whiteColor] forKey:@"mtpColor"];
    [self.wager_Array addObject:wagerDict4];
    
    
    NSMutableDictionary *wagerDict5=[NSMutableDictionary dictionary];
    [wagerDict5 setValue:@"Monticello" forKey:@"raceTrackTitle"];
    [wagerDict5 setValue:@"5 Furlongs I Frozen Claiming ($20,000 - $18,000)" forKey:@"tackInfoLabel"];
    [wagerDict5 setValue:@"1" forKey:@"raceNumber"];
    [wagerDict5 setValue:@"92" forKey:@"MTP"];
    [wagerDict5 setObject:[UIColor colorWithRed:2.0/255 green:143.0/255 blue:26.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict5 setObject:[UIColor whiteColor] forKey:@"mtpColor"];
    [self.wager_Array addObject:wagerDict5];
    
    NSMutableDictionary *wagerDict6=[NSMutableDictionary dictionary];
    [wagerDict6 setValue:@"Pocono Downs" forKey:@"raceTrackTitle"];
    [wagerDict6 setValue:@"5 Furlongs I Sloppy Claiming ($20,000 - $18,000)" forKey:@"tackInfoLabel"];
    [wagerDict6 setValue:@"1" forKey:@"raceNumber"];
    [wagerDict6 setValue:@"92" forKey:@"MTP"];
    [wagerDict6 setObject:[UIColor colorWithRed:2.0/255 green:143.0/255 blue:26.0/255 alpha:1.0] forKey:@"mtpBgColor"];
    [wagerDict6 setObject:[UIColor whiteColor] forKey:@"mtpColor"];
    [self.wager_Array addObject:wagerDict6];
    
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.wager_Array count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.wagerCustomCell  = (ZLTrackCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.wagerCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLTrackCustomCell" owner:self options:nil];
    }
    [self.wagerCustomCell.favButton addTarget:self action:@selector(favClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.wagerCustomCell.favButton setTag:indexPath.row];
    
    [self.wagerCustomCell.raceTrack_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.wagerCustomCell.information_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:10]];
    [self.wagerCustomCell.raceNumber_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.wagerCustomCell.mtp_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    
    
    
    NSMutableDictionary *dic = [self.wager_Array objectAtIndex:indexPath.row];
    [self.wagerCustomCell.raceTrack_Label setText:[dic valueForKey:@"raceTrackTitle"]];
    [self.wagerCustomCell.raceNumber_Label setTextColor:[dic valueForKey:@"headingTextColor"]];
    [self.wagerCustomCell.information_Label setText:[dic valueForKey:@"tackInfoLabel"]];
    [self.wagerCustomCell.information_Label setTextColor:[dic valueForKey:@"infoTextColor"]];
    [self.wagerCustomCell.raceNumber_Label setText:[NSString stringWithFormat:@"Race\n%@", [dic valueForKey:@"raceNumber"]]];
    [self.wagerCustomCell.mtp_Label setText:[NSString stringWithFormat:@"MTP\n %@",[dic valueForKey:@"MTP"]]];
    [self.wagerCustomCell.mtp_Label setBackgroundColor:[dic valueForKey:@"mtpBgColor"]];
    [self.wagerCustomCell.mtp_Label setTextColor:[dic valueForKey:@"mtpColor"]];

    NSString *selectedTrack = [[ZLSelectedValues sharedInstance] selectedTrack];
    if([selectedTrack isEqualToString:[dic valueForKey:@"raceTrackTitle"]])
    {
        self.wagerCustomCell.backgroundImage.image = [UIImage imageNamed:@"black_bg.png"];
        self.wagerCustomCell.raceTrack_Label.textColor = [UIColor whiteColor];
        self.wagerCustomCell.raceNumber_Label.textColor = [UIColor whiteColor];
        self.wagerCustomCell.information_Label.textColor = [UIColor whiteColor];
        self.wagerCustomCell.raceNumber_Label.textColor = [UIColor whiteColor];
        [self.wagerCustomCell.favButton setImage:[UIImage imageNamed:@"whitestar.png"] forState:UIControlStateNormal];
        [self.wagerCustomCell.favButton setImage:[UIImage imageNamed:@"star_back_select.png"] forState:UIControlStateHighlighted];

    }
    else{
        [self.wagerCustomCell.favButton setImage:[UIImage imageNamed:@"unselect.png"] forState:UIControlStateNormal];
        [self.wagerCustomCell.favButton setImage:[UIImage imageNamed:@"select.png"] forState:UIControlStateHighlighted];

        self.wagerCustomCell.backgroundImage.image = [UIImage imageNamed:@"Track_Cell_Bg.png"];
    }
    return self.wagerCustomCell;
}

-(void)favClicked:(UIButton *)butt
{
    if ([butt isSelected])
    {
        [butt setSelected:NO];
    }
    else
    {
        [butt setSelected:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 73;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadData];

    NSMutableDictionary *dic = [self.wager_Array objectAtIndex:indexPath.row];
    [[ZLSelectedValues sharedInstance] setSelectedRace:[dic valueForKey:@"raceNumber"]];
    [[ZLSelectedValues sharedInstance] setSelectedTrack:[dic valueForKey:@"raceTrackTitle"]];
    [[ZLSelectedValues sharedInstance] setSelectedMTP:[dic valueForKey:@"MTP"]];

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerTrack] forKey:@"currentLoadedIndex"]];
}

-(void)viewDidUnload{
    self.wagerTableView=nil;
    self.wagerCustomCell=nil;
    self.wager_Array=nil;
    self.wagerCustomCell.raceTrack_Label=nil;
    self.wagerCustomCell.backgroundImage=nil;
    self.wagerCustomCell.raceNumber_Label=nil;
    self.wagerCustomCell.backgroundImage=nil;
    self.wagerCustomCell.information_Label=nil;
    self.wagerCustomCell.raceNumber_Label=nil;
    self.wagerCustomCell.favButton=nil;
    self.wagerCustomCell.mtp_Label=nil;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
