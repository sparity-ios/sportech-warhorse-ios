//
//  ZLRaceViewController.m
//  WarHorse
//
//  Created by Sparity on 7/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLRaceViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ZLAmountViewController.h"
#import "ZLSelectedValues.h"
@interface ZLRaceViewController ()

@end

@implementation ZLRaceViewController
@synthesize raceCustomCell=_raceCustomCell;
@synthesize raceDetailsArray=_raceDetailsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.selectRaceLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];

    [self loadData];
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    [self.raceTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

}

-(void)loadData{
    
    _raceDetailsArray=[[NSMutableArray alloc]init];
    
    NSArray *array5 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P4", nil];
    NSMutableDictionary *raceDict5=[NSMutableDictionary dictionary];
    [raceDict5 setValue:@"1" forKey:@"raceNumber"];
    [raceDict5 setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict5 setValue:@"Finished" forKey:@"MTP"];
    [raceDict5 setObject:array5 forKey:@"P1"];
    [raceDict5 setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict5 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    [raceDict5 setValue:@"6 Furlongs" forKey:@"FurlongKey"];
    [raceDict5 setValue:@"claiming($50,000-$40,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict5];
    
    NSArray *array16 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3", nil];
    NSMutableDictionary *raceDict16=[NSMutableDictionary dictionary];
    [raceDict16 setValue:@"2" forKey:@"raceNumber"];
    [raceDict16 setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict16 setValue:@"Finished" forKey:@"MTP"];
    [raceDict16 setObject:array16 forKey:@"P1"];
    [raceDict16 setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict16 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    [raceDict16 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict16 setValue:@"claiming($5,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict16];
    
    
    
    NSArray *array27 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P4", nil];
    NSMutableDictionary *raceDict27=[NSMutableDictionary dictionary];
    [raceDict27 setValue:@"3" forKey:@"raceNumber"];
    [raceDict27 setValue:@"Claiming" forKey:@"raceTrack"];
    [raceDict27 setValue:@"Finished" forKey:@"MTP"];
    [raceDict27 setObject:array27 forKey:@"P1"];
    [raceDict27 setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict27 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    
    [raceDict27 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict27 setValue:@"claiming($5,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict27];
    
    NSArray *array38 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3", nil];
    NSMutableDictionary *raceDict38=[NSMutableDictionary dictionary];
    [raceDict38 setValue:@"4" forKey:@"raceNumber"];
    [raceDict38 setValue:@"Turif" forKey:@"raceTrack"];
    [raceDict38 setValue:@"Finished" forKey:@"MTP"];
    [raceDict38 setObject:array38 forKey:@"P1"];
    [raceDict38 setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict38 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    
    [raceDict38 setValue:@"6 1/2 Furlongs" forKey:@"FurlongKey"];
    [raceDict38 setValue:@"Allownace" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict38];
    
    NSArray *array49 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P5", nil];
    NSMutableDictionary *raceDict49=[NSMutableDictionary dictionary];
    [raceDict49 setValue:@"5" forKey:@"raceNumber"];
    [raceDict49 setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict49 setValue:@"Finished" forKey:@"MTP"];
    [raceDict49 setObject:array49 forKey:@"P1"];
    [raceDict49 setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict49 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    
    [raceDict49 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict49 setValue:@"Starter Optional Claiming" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict49];


    
    NSArray *array = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P4", nil];
    NSMutableDictionary *raceDict=[NSMutableDictionary dictionary];
    [raceDict setValue:@"6" forKey:@"raceNumber"];
    [raceDict setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict setValue:@"Finished" forKey:@"MTP"];
    [raceDict setObject:array forKey:@"P1"];
    [raceDict setObject:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    [raceDict setValue:@"6 Furlongs" forKey:@"FurlongKey"];
    [raceDict setValue:@"claiming($50,000-$40,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict];
    
    NSArray *array1 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3", nil];
    NSMutableDictionary *raceDict1=[NSMutableDictionary dictionary];
    [raceDict1 setValue:@"7" forKey:@"raceNumber"];
    [raceDict1 setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict1 setValue:@"MTP-0" forKey:@"MTP"];
    [raceDict1 setObject:array1 forKey:@"P1"];
    [raceDict1 setObject:[UIColor colorWithRed:255.0/255.0f green:0.0/255.0f blue:0.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict1 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    [raceDict1 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict1 setValue:@"claiming($5,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict1];

    
    
    NSArray *array2 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P4", nil];
    NSMutableDictionary *raceDict2=[NSMutableDictionary dictionary];
    [raceDict2 setValue:@"8" forKey:@"raceNumber"];
    [raceDict2 setValue:@"Claiming" forKey:@"raceTrack"];
    [raceDict2 setValue:@"MTP-5" forKey:@"MTP"];
    [raceDict2 setObject:array2 forKey:@"P1"];
    [raceDict2 setObject:[UIColor colorWithRed:248.0/255.0f green:226.0/255.0f blue:46.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict2 setObject:[UIColor blackColor] forKey:@"MTPColor"];

    [raceDict2 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict2 setValue:@"claiming($5,000)" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict2];
    
    NSArray *array3 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3", nil];
    NSMutableDictionary *raceDict3=[NSMutableDictionary dictionary];
    [raceDict3 setValue:@"9" forKey:@"raceNumber"];
    [raceDict3 setValue:@"Turif" forKey:@"raceTrack"];
    [raceDict3 setValue:@"MTP-33" forKey:@"MTP"];
    [raceDict3 setObject:array3 forKey:@"P1"];
    [raceDict3 setObject:[UIColor colorWithRed:6.0/255.0f green:141.0/255.0f blue:28.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict3 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    
    [raceDict3 setValue:@"6 1/2 Furlongs" forKey:@"FurlongKey"];
    [raceDict3 setValue:@"Allownace" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict3];
    
    NSArray *array4 = [NSArray arrayWithObjects:@"BB",@"EX",@"TR",@"SF",@"P3",@"P5", nil];
    NSMutableDictionary *raceDict4=[NSMutableDictionary dictionary];
    [raceDict4 setValue:@"10" forKey:@"raceNumber"];
    [raceDict4 setValue:@"All Weather Track" forKey:@"raceTrack"];
    [raceDict4 setValue:@"MTP-99" forKey:@"MTP"];
    [raceDict4 setObject:array4 forKey:@"P1"];
    [raceDict4 setObject:[UIColor colorWithRed:6.0/255.0f green:141.0/255.0f blue:28.0f/255.0f alpha:1.0] forKey:@"MTPBgColor"];
    [raceDict4 setObject:[UIColor whiteColor] forKey:@"MTPColor"];
    
    [raceDict4 setValue:@"5 Furlongs" forKey:@"FurlongKey"];
    [raceDict4 setValue:@"Starter Optional Claiming" forKey:@"amount"];
    [self.raceDetailsArray addObject:raceDict4];
  
  
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.raceDetailsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.raceCustomCell  = (ZLRaceCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.raceCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLRaceCustomCell" owner:self options:nil];
        
    }
    
    NSMutableDictionary *dic = [self.raceDetailsArray objectAtIndex:indexPath.row];
    NSString *mtpString = [dic valueForKey:@"MTP"];
    NSString *selectedRace = [[ZLSelectedValues sharedInstance] selectedRace];
    if ([mtpString isEqualToString:@"Finished"])
    {
        self.raceCustomCell.backgroundImage.image = [UIImage imageNamed:@"bg_cross.png"];
    }
    else if([selectedRace isEqualToString:[dic valueForKey:@"raceNumber"]])
    {
        self.raceCustomCell.backgroundImage.image = [UIImage imageNamed:@"bg_black.png"];
        self.raceCustomCell.raceNumberLabel.textColor = [UIColor whiteColor];
        self.raceCustomCell.trackLabel.textColor = [UIColor whiteColor];
        self.raceCustomCell.mtpLabel.textColor = [UIColor whiteColor];
        self.raceCustomCell.furlongLabel.textColor = [UIColor whiteColor];
        self.raceCustomCell.amountLabel.textColor = [UIColor whiteColor];
    }
    else{
        self.raceCustomCell.backgroundImage.image = [UIImage imageNamed:@"bg_white.png"];
    }
    
    int i=13;
    int j=49;
    
    for (NSUInteger k = 0; k<[[[self.raceDetailsArray objectAtIndex:indexPath.row] valueForKey:@"P1"] count]; k++)
    {
        UILabel *betType_label=[[UILabel alloc] init];
        [betType_label setFrame:CGRectMake(i, j, 25, 20)];
        
        if([selectedRace isEqualToString:[dic valueForKey:@"raceNumber"]])
        {
            [betType_label setBackgroundColor:[UIColor colorWithRed:41.0/255.0f green:39.0/255.0f blue:43.0/255.0f alpha:1.0]];
            [betType_label setTextColor:[UIColor colorWithRed:214.0/255.0f green:214.0/255.0f blue:214.0/255.0f alpha:1.0]];

            betType_label.layer.borderColor = [UIColor colorWithRed:84.0/255.0f green:82.0/255.0f blue:85.0/255.0f alpha:1.0].CGColor;
        }
        else
        {
            [betType_label setBackgroundColor:[UIColor whiteColor]];
            [betType_label setTextColor:[UIColor colorWithRed:102.0/255.0f green:102.0/255.0f blue:102.0/255.0f alpha:1.0]];
            betType_label.layer.borderColor = [UIColor colorWithRed:224.0/255.0f green:224.0/255.0f blue:224.0/255.0f alpha:1.0].CGColor;
        }
        
        [betType_label setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
        betType_label.layer.borderWidth = 1.0;
        [betType_label setTextAlignment:NSTextAlignmentCenter];
        [betType_label setText:[[[self.raceDetailsArray objectAtIndex:indexPath.row] valueForKey:@"P1"] objectAtIndex:k]];
        [self.raceCustomCell addSubview:betType_label];
        i += 30;
    }
      
    
    [self.raceCustomCell.raceNumberLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.raceCustomCell.trackLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.raceCustomCell.mtpLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [self.raceCustomCell.furlongLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];
    [self.raceCustomCell.amountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:11]];

    
    self.raceCustomCell.raceNumberLabel.text=[NSString stringWithFormat:@"Race %@",[dic valueForKey:@"raceNumber"]];
    
    self.raceCustomCell.trackLabel.text=[dic valueForKey:@"raceTrack"];
    
    self.raceCustomCell.mtpLabel.text=[dic valueForKey:@"MTP"];
    [self.raceCustomCell.mtpLabel setTextColor:[dic valueForKey:@"MTPColor"]];
    [self.raceCustomCell.mtpLabel setBackgroundColor:[dic valueForKey:@"MTPBgColor"]];

    self.raceCustomCell.furlongLabel.text=[dic valueForKey:@"FurlongKey"];
    
    self.raceCustomCell.amountLabel.text=[dic valueForKey:@"amount"];
        
    [self.raceCustomCell.statusLabel setBackgroundColor:[dic valueForKey:@"MTPBgColor"]];

    return self.raceCustomCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 77;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSMutableDictionary *dic = [self.raceDetailsArray objectAtIndex:indexPath.row];

    if (![[dic valueForKey:@"MTP"] isEqualToString:@"Finished"]) {
        [tableView reloadData];
        
        [[ZLSelectedValues sharedInstance] setSelectedRace:[dic valueForKey:@"raceNumber"]];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadWagerView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:WagerRace] forKey:@"currentLoadedIndex"]];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Under Progress" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        alert = nil;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
