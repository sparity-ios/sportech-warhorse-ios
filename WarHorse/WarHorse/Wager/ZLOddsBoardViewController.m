//
//  ZLViewController.m
//  OddsMatrix
//
//  Created by Sparity on 08/08/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLOddsBoardViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ZLOddsBoardViewController ()

@end

@implementation ZLOddsBoardViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.oddsPoolButton.layer.borderWidth = 1;
    self.oddsPoolButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
    
    self.probablesButton.layer.borderWidth = 1;
    self.probablesButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
    
    [self loadData];
    
    self.probablesmatrix = [[ZLMatrixView alloc] initWithFrame:self.flippingView.bounds delegate:self tag:222];
    self.probablesmatrix.autoresizesSubviews = YES;
    self.probablesmatrix.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth);
    //self.probablesmatrix.hidden = YES;
    [self.flippingView addSubview:self.probablesmatrix];

    self.oddsmatrix = [[ZLMatrixView alloc] initWithFrame:self.flippingView.bounds delegate:self tag:111];
    self.oddsmatrix.autoresizesSubviews = YES;
    self.oddsmatrix.autoresizingMask = (UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth);
    [self.flippingView addSubview:self.oddsmatrix];
    
    
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
}

-(void) loadData{
    self.runnersArray = [[NSMutableArray alloc] init];

    NSMutableDictionary *dic1=[NSMutableDictionary dictionary];
    [dic1 setValue:@"1" forKey:@"horseNumber"];
    [dic1 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic1 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic1];
    
    NSMutableDictionary *dic2=[NSMutableDictionary dictionary];
    [dic2 setValue:@"2" forKey:@"horseNumber"];
    [dic2 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic2 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic2];
    
    NSMutableDictionary *dic3=[NSMutableDictionary dictionary];
    [dic3 setValue:@"3" forKey:@"horseNumber"];
    [dic3 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic3 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic3];
    
    NSMutableDictionary *dic4=[NSMutableDictionary dictionary];
    [dic4 setValue:@"4" forKey:@"horseNumber"];
    [dic4 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic4 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic4];
    
    NSMutableDictionary *dic5=[NSMutableDictionary dictionary];
    [dic5 setValue:@"5" forKey:@"horseNumber"];
    [dic5 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic5 setObject:[UIColor colorWithRed:9.0/256 green:253.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic5];
    
    NSMutableDictionary *dic6=[NSMutableDictionary dictionary];
    [dic6 setValue:@"6" forKey:@"horseNumber"];
    [dic6 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic6 setObject:[UIColor colorWithRed:0.0/256 green:1.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic6];
    
    NSMutableDictionary *dic7=[NSMutableDictionary dictionary];
    [dic7 setValue:@"7" forKey:@"horseNumber"];
    [dic7 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic7 setObject:[UIColor colorWithRed:251.0/256 green:135.0/256 blue:4.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic7];
    
    NSMutableDictionary *dic8=[NSMutableDictionary dictionary];
    [dic8 setValue:@"8" forKey:@"horseNumber"];
    [dic8 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic8 setObject:[UIColor colorWithRed:247.0/256 green:226.0/256 blue:192.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic8];
    
    NSMutableDictionary *dic9=[NSMutableDictionary dictionary];
    [dic9 setValue:@"9" forKey:@"horseNumber"];
    [dic9 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic9 setObject:[UIColor colorWithRed:0.0/256 green:135.0/256 blue:129.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic9];
    
    NSMutableDictionary *dic10=[NSMutableDictionary dictionary];
    [dic10 setValue:@"10" forKey:@"horseNumber"];
    [dic10 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic10 setObject:[UIColor colorWithRed:130.0/256 green:4.0/256 blue:137.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic10];
    
    NSMutableDictionary *dic11=[NSMutableDictionary dictionary];
    [dic11 setValue:@"11" forKey:@"horseNumber"];
    [dic11 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic11 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic11];
    
    NSMutableDictionary *dic12=[NSMutableDictionary dictionary];
    [dic12 setValue:@"12" forKey:@"horseNumber"];
    [dic12 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic12 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic12];
    
    NSMutableDictionary *dic13=[NSMutableDictionary dictionary];
    [dic13 setValue:@"13" forKey:@"horseNumber"];
    [dic13 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic13 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic13];
    
    NSMutableDictionary *dic14=[NSMutableDictionary dictionary];
    [dic14 setValue:@"14" forKey:@"horseNumber"];
    [dic14 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic14 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic14];
    
    NSMutableDictionary *dic15=[NSMutableDictionary dictionary];
    [dic15 setValue:@"15" forKey:@"horseNumber"];
    [dic15 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic15 setObject:[UIColor colorWithRed:9.0/256 green:253.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic15];
    
    NSMutableDictionary *dic16=[NSMutableDictionary dictionary];
    [dic16 setValue:@"16" forKey:@"horseNumber"];
    [dic16 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic16 setObject:[UIColor colorWithRed:0.0/256 green:1.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic16];
    
    NSMutableDictionary *dic17=[NSMutableDictionary dictionary];
    [dic17 setValue:@"17" forKey:@"horseNumber"];
    [dic17 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic17 setObject:[UIColor colorWithRed:251.0/256 green:135.0/256 blue:4.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic17];
    
    NSMutableDictionary *dic18=[NSMutableDictionary dictionary];
    [dic18 setValue:@"18" forKey:@"horseNumber"];
    [dic18 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic18 setObject:[UIColor colorWithRed:247.0/256 green:226.0/256 blue:192.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic18];
    
    NSMutableDictionary *dic19=[NSMutableDictionary dictionary];
    [dic19 setValue:@"19" forKey:@"horseNumber"];
    [dic19 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic19 setObject:[UIColor colorWithRed:0.0/256 green:135.0/256 blue:129.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic19];
    
    NSMutableDictionary *dic20=[NSMutableDictionary dictionary];
    [dic20 setValue:@"20" forKey:@"horseNumber"];
    [dic20 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic20 setObject:[UIColor colorWithRed:130.0/256 green:4.0/256 blue:137.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic20];
    
    NSMutableDictionary *dic21=[NSMutableDictionary dictionary];
    [dic21 setValue:@"21" forKey:@"horseNumber"];
    [dic21 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic21 setObject:[UIColor colorWithRed:255.0/256 green:0.0/256 blue:0.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic21];
    
    NSMutableDictionary *dic22=[NSMutableDictionary dictionary];
    [dic22 setValue:@"22" forKey:@"horseNumber"];
    [dic22 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic22 setObject:[UIColor colorWithRed:255.0/256 green:255.0/256 blue:255.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic22];
    
    NSMutableDictionary *dic23=[NSMutableDictionary dictionary];
    [dic23 setValue:@"23" forKey:@"horseNumber"];
    [dic23 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [dic23 setObject:[UIColor colorWithRed:0.0/256 green:3.0/256 blue:251.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic23];
    
    NSMutableDictionary *dic24=[NSMutableDictionary dictionary];
    [dic24 setValue:@"24" forKey:@"horseNumber"];
    [dic24 setObject:[UIColor blackColor] forKey:@"textColor"];
    [dic24 setObject:[UIColor colorWithRed:250.0/256 green:250.0/256 blue:37.0/256 alpha:1.0] forKey:@"backgroundColor"];
    [self.runnersArray addObject:dic24];
    
    
    self.oddsPoolArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.runnersArray.count; i++) {
        NSMutableArray *array = [NSMutableArray array];
        for (int j = 0; j < self.runnersArray.count; j++) {
            NSString *title = [NSString stringWithFormat:@"%d",arc4random()%1000];
            [array addObject:title];
        }
        [self.oddsPoolArray addObject:array];
    }
    
    self.probablesArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.runnersArray.count; i++) {
        NSMutableArray *array = [NSMutableArray array];
        for (int j = 0; j < self.runnersArray.count; j++) {
            NSString *title = [NSString stringWithFormat:@"$%d",arc4random()%1000];
            [array addObject:title];
        }
        [self.probablesArray addObject:array];
    }
}

- (IBAction)closeButtonClicked:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)oddsPoolButtonClicked:(id)sender
{
    self.probablesmatrix.hidden = NO;

    if (![self.oddsPoolButton isSelected])
    {
        [self.oddsPoolButton setSelected:YES];
        [self.probablesButton setSelected:NO];
        
        self.oddsPoolButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
        self.probablesButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
        
        [self.oddsPoolButton setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
        [self.probablesButton setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];
        
        [self flipViews];
    }
    else{
    }

}
-(IBAction)probablesButtonClicked:(id)sender
{
    self.probablesmatrix.hidden = NO;

    if (![self.probablesButton isSelected])
    {
        [self.oddsPoolButton setSelected:NO];
        [self.probablesButton setSelected:YES];
        
        self.oddsPoolButton.layer.borderColor = [UIColor colorWithRed:99.0/255 green:99.0/255 blue:99.0/255 alpha:1.0].CGColor;
        self.probablesButton.layer.borderColor = [UIColor colorWithRed:2.0/255 green:55.0/255 blue:84.0/255 alpha:1.0].CGColor;
        
        [self.probablesButton setBackgroundColor:[UIColor colorWithRed:35.0/255 green:108.0/255 blue:142.0/255 alpha:1.0]];
        [self.oddsPoolButton setBackgroundColor:[UIColor colorWithRed:224.0/255 green:224.0/255 blue:224.0/255 alpha:1.0]];
        
        [self flipViews];
    }
    else{
        
    }

}


-(void)flipViews
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.flippingView cache:YES];
    
    if ([self.oddsmatrix superview])
    {
        [self.oddsmatrix removeFromSuperview];
        [self.flippingView addSubview:self.probablesmatrix];
        [self.flippingView sendSubviewToBack:self.oddsmatrix];
    }
    else
    {
        [self.probablesmatrix removeFromSuperview];
        [self.flippingView addSubview:self.oddsmatrix];
        [self.flippingView sendSubviewToBack:self.probablesmatrix];
    }
    
    [UIView commitAnimations];
}

-(CGFloat) widthForCellInMatrixView:(id)matrixView
{
    return 42;
}

-(CGFloat) heightForCellInMatrixView:(id)matrixView
{
    return 36;
}
-(NSUInteger) numberOfRowsInMatrixView:(id)matrixView
{
    return self.runnersArray.count;
}
-(NSUInteger) numberOfColumnsInMatrixView:(id)matrixView
{
    return self.runnersArray.count;
}

- (NSString *) matrixView:(id)matrixView titleForRow:(NSInteger)row forColumn:(NSInteger)column
{
    NSMutableArray *array = nil;
    if (((ZLMatrixView *)matrixView).tag == 111) {
        array = [self.oddsPoolArray objectAtIndex:row];
    }
    else{
        array = [self.probablesArray objectAtIndex:row];
    }
    
    return [array objectAtIndex:column];
}

- (NSString *) matrixView:(id)matrixView headerForColumn:(NSInteger)column
{
    NSMutableDictionary *dic = [self.runnersArray objectAtIndex:column];
    return [dic objectForKey:@"horseNumber"];
}
- (NSString *) matrixView:(id)matrixView headerForRow:(NSInteger)row
{
    NSMutableDictionary *dic = [self.runnersArray objectAtIndex:row];
    return [dic objectForKey:@"horseNumber"];
}

- (UIColor *) backgroundColorCellIndex:(NSUInteger)index
{
    NSMutableDictionary *dic = [self.runnersArray objectAtIndex:index];
    return [dic objectForKey:@"backgroundColor"];
}
- (UIColor *) textColorCellIndex:(NSUInteger)index
{
    NSMutableDictionary *dic = [self.runnersArray objectAtIndex:index];
    return [dic objectForKey:@"textColor"];
}

-(void) tapedCellWithRow:(NSUInteger) row andWIthColumn:(NSUInteger)column
{
    NSLog(@"row = %d , column = %d",row,column);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
