//
//  ZLResetPasswordViewController.h
//  WarHorse
//
//  Created by Sparity on 8/6/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLResetPasswordViewController : UIViewController<UITextFieldDelegate>
@property(nonatomic,retain) IBOutlet UILabel *resetLabel;
@property(nonatomic,retain) IBOutlet UITextField *Pwd_TF;
@property(nonatomic,retain) IBOutlet UITextField *confirmPwd_TF;
@property(nonatomic,retain) IBOutlet UIButton *submitBtn;
@property(nonatomic,retain) IBOutlet UIButton *backButton;

-(IBAction)backClicked:(id)sender;
-(IBAction)submitClicked:(id)sender;
-(IBAction)BackGroundClicked;

@end
