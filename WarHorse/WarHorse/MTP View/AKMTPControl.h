//
//  AKMTPControl.h
//  MTPComponent
//
//  Created by Sparity on 05/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AKMTPScaleView.h"
@interface AKMTPControl : UIScrollView
{
    AKMTPScaleView *scaleView;
}

@property (nonatomic, assign) NSInteger minValue;
@property (nonatomic, assign) NSInteger maxValue;
@property (nonatomic, assign) NSInteger interval;
@property (nonatomic, assign) NSInteger numberOfPages;

@end
