//
//  SPCarryOversViewController.m
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "SPCarryOversViewController.h"
#import "SPCarryOver.h"

@interface SPCarryOversViewController ()

@end

@implementation SPCarryOversViewController
@synthesize carryOverTableView = _carryOverTableView;
@synthesize raceParkArray = _raceParkArray;
@synthesize customCarryOverCell = _customCarryOverCell;
@synthesize headerImageView = _headerImageView;
@synthesize carryOver = _carryOver;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UILabel *dateLabel = [[UILabel alloc]initWithFrame:CGRectMake(103, 58, 300, 16)];
    [dateLabel setBackgroundColor:[UIColor clearColor]];
    [dateLabel setText:@"Friday Aug 19,2013"];
    [dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
    [dateLabel setTextColor:[UIColor colorWithRed:14.0/255.0f green:78.0/255.0f   blue:113.0/255.0f alpha:1.0]];
    [self.view addSubview:dateLabel];
    UILabel *carryoverText = [[UILabel alloc]initWithFrame:CGRectMake(47, 14, 300, 19)];
    [carryoverText setBackgroundColor:[UIColor clearColor]];
    [carryoverText setText:@"Carryovers"];
    [carryoverText setFont:[UIFont fontWithName:@"Roboto-Medium" size:16]];
    [carryoverText setTextColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f   blue:255.0/255.0f alpha:1.0]];
    [self.headerImageView addSubview:carryoverText];
    [self.carryOverTableView setSeparatorColor:[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0]];
    
    [self loadData];
}
-(void)loadData
{
    _raceParkArray=[[NSMutableArray alloc]init];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Arlington Park";
    self.carryOver.trackpriceLabel = @"$163,680";
    self.carryOver.date = @"Pick 9 Jackpot - Aug 22, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:235.0/255.0f green:234.0/255.0f blue:234/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Calder";
    self.carryOver.trackpriceLabel = @"$163,680";
    self.carryOver.date = @"Pick 6 Jackpot - Aug 19, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:243.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Arlington Park";
    self.carryOver.trackpriceLabel = @"$87,824";
    self.carryOver.date = @"Pick 6 Jackpot - Aug 22, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:235.0/255.0f green:234.0/255.0f blue:234/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Louisiana Downs";
    self.carryOver.trackpriceLabel = @"$22,377";
    self.carryOver.date = @"Pick 5 Jackpot - Aug 22, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:243.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Golde Gate";
    self.carryOver.trackpriceLabel = @"$7,671";
    self.carryOver.date = @"Pick 6 - Aug 22, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:235.0/255.0f green:234.0/255.0f blue:234/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];

    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Fort Erie";
    self.carryOver.trackpriceLabel = @"$6,823";
    self.carryOver.date = @"Pick 5 - Aug 20, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:243.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Parx Racing";
    self.carryOver.trackpriceLabel = @"$2,158";
    self.carryOver.date = @"Pick 6 - Aug 19, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:235.0/255.0f green:234.0/255.0f blue:234/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Golde Gate";
    self.carryOver.trackpriceLabel = @"$2,158";
    self.carryOver.date = @"Pick 6 - Aug 19, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:243.0/255.0f green:243.0/255.0f blue:243.0/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];
    
    self.carryOver = [[SPCarryOver alloc] init];
    self.carryOver.raceTrackTitle = @"Arlington Park";
    self.carryOver.trackpriceLabel = @"$87,824";
    self.carryOver.date = @"Pick 6 Jackpot - Aug 22, 2013";
    self.carryOver.mtpBgColor = [UIColor colorWithRed:235.0/255.0f green:234.0/255.0f blue:234/255.0f alpha:1.0];
    [self.raceParkArray addObject:self.carryOver];

}
- (IBAction)toggleOnClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.raceParkArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.customCarryOverCell  = (SPCarryOverCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  
    if (self.customCarryOverCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SPCarryOverCustomCell" owner:self options:nil];
    }
    SPCarryOver *carryOverObj = [self.raceParkArray objectAtIndex:indexPath.row];
    
    NSLog(@"Carry Over Object is --------- %@", carryOverObj);
    
    [self.customCarryOverCell setCarryOver:carryOverObj];
    [self.customCarryOverCell updateView];
    return self.customCarryOverCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 56;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCarryOverTableView:nil];
    [self setCustomCarryOverCell:nil];
    self.raceParkArray=nil;
//    self.customCarryOverCell.parkTitle=nil;
//    self.customCarryOverCell.dateLbel=nil;
//    self.customCarryOverCell.priceLabel=nil;
    

    [self setHeaderImageView:nil];
    [super viewDidUnload];
}

@end
