//
//  SPPromotionTableCell.h
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface SPPromotionTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet AsyncImageView *promotionImage;


@end
