//
//  ZLRegisterViewController.m
//  WarHorse
//
//  Created by Sparity on 8/6/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLRegisterViewController.h"

@interface ZLRegisterViewController ()

@end

@implementation ZLRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.userName_TF setPlaceholder:@" User name"];
    [self.userName_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.pwd_TF setPlaceholder:@" Password"];
    [self.pwd_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    
    [self.confirmPwd_TF setPlaceholder:@" Confirm Password"];
    [self.confirmPwd_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.enterPin_TF setPlaceholder:@" Enter 4 digit PIN number"];
    [self.enterPin_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.confirmPin_TF setPlaceholder:@" Confirm PIN number"];
    [self.confirmPin_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.createActLabel setText:@"Create new account"];
    [self.createActLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
    
    [self.accountLabel setText:@"Already have an account?"];
    [self.accountLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];

    

    
}

-(IBAction)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)registerClicked:(id)sender{
    
}
-(IBAction)loginClicked:(id)sender{
    
}
-(IBAction)BackGroundClicked{
    [self.userName_TF resignFirstResponder];
    [self.pwd_TF resignFirstResponder];
    [self.confirmPwd_TF resignFirstResponder];
    [self.enterPin_TF resignFirstResponder];
    [self.confirmPin_TF resignFirstResponder];
}
#pragma mark-
#pragma mark- TextField Delegate Method.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	CGRect textViewRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGFloat bottomEdge = textViewRect.origin.y + textViewRect.size.height;
	if (bottomEdge >= 250) {//250
        CGRect viewFrame = self.view.frame;
        self.shiftForKeyboard = bottomEdge - 200;
        viewFrame.origin.y -= self.shiftForKeyboard;
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationBeginsFromCurrentState:YES];
		[UIView setAnimationDuration:0.3];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
		
	} else {
		self.shiftForKeyboard = 0.0f;
	}
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if (self.view.frame.origin.y == 0) {
        [textField resignFirstResponder];
    }
    else{
     	// Resign first responder
        [textField resignFirstResponder];
        
        
        // Make a CGRect for the view (which should be positioned at 0,0 and be 320px wide and 480px tall)
        CGRect viewFrame = self.view.frame;
        if(viewFrame.origin.y!=0)
        {
            // Adjust the origin back for the viewFrame CGRect
            viewFrame.origin.y += self.shiftForKeyboard;
            // Set the shift value back to zero
            self.shiftForKeyboard = 0.0f;
            
            // As above, the following animation setup just makes it look nice when shifting
            // Again, we don't really need the animation code, but we'll leave it in here
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.3];
            // Apply the new shifted vewFrame to the view
            [self.view setFrame:viewFrame];
            // More animation code
            [UIView commitAnimations];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
