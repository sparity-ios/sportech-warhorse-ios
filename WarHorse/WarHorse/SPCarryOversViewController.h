//
//  SPCarryOversViewController.h
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPCarryOverCustomCell.h"
@class SPCarryOver;

@interface SPCarryOversViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *carryOverTableView;
@property (strong, nonatomic) IBOutlet SPCarryOverCustomCell *customCarryOverCell;
@property (strong, nonatomic) IBOutlet UIImageView *headerImageView;
@property (strong, nonatomic) SPCarryOver *carryOver;
- (IBAction)toggleOnClick:(id)sender;
@property (nonatomic, strong) NSMutableArray *raceParkArray;
@end
