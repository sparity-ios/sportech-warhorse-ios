//
//  ZLHomeScreenViewController.h
//  WarHorse
//
//  Created by Sparity on 7/4/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//


////home screen
#import <UIKit/UIKit.h>


@interface ZLHomeScreenViewController : UIViewController<UITextFieldDelegate>
@property(nonatomic,retain) IBOutlet UIButton *loginButton;
@property(nonatomic,retain) IBOutlet UIButton *accountButton;
@property(nonatomic,retain) IBOutlet UILabel *accountLabel;
@property(nonatomic,retain) IBOutlet UILabel *termsConditonsLabel;
-(IBAction)login_Clicked:(id)sender;
-(IBAction)account_Clicked:(id)sender;

@end
