//
//  ZLSideMenu.m
//  WarHorse
//
//  Created by Sparity on 08/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLSideMenu.h"
#import "ZLSelectedValues.h"
//static int whichViewLoaded;

@implementation ZLSideMenu

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.whichViewLoaded = 0;
        
        self.itemsArray = [[NSMutableArray alloc] init];
        self.tilesArray = [[NSMutableArray alloc] init];
        // Initialization code
        self.numberOfItems = 5;
        [self setBackgroundColor:[UIColor colorWithRed:66.0/256 green:63.0/256 blue:70.0/256 alpha:1.0]];
        [self prepareTilesWithFrame:frame];
    }
    return self;
}

- (void) prepareTilesWithFrame:(CGRect)frame
{
    float cellWidth = frame.size.width;
    float cellHeight = frame.size.height/5;
    for (int i = 0; i < self.numberOfItems; i++)
    {
        ZLSideMenuTile *tile = [[ZLSideMenuTile alloc] initWithFrame:CGRectMake(0, i * cellHeight, cellWidth, cellHeight)];
        [tile setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];
        tile.tag = i;
        tile.delegate = self;
        [self.tilesArray addObject:tile];
        [self addSubview:tile];
    }
}

- (void)tapDetectingView:(id)view
{
    ZLSideMenuTile *tile = (ZLSideMenuTile *)view;
    [self.delegate clearSelectedValuesWithIndex:tile.tag];

    if (tile.tag <= self.whichViewLoaded)
    {
        [self.delegate tapDetectingView:view];
        [self reloadTilesWithIndex:tile.tag];
    }
    
}

- (void) nextViewLoaded
{
    self.whichViewLoaded++;
    [self reloadTilesWithIndex:self.whichViewLoaded];
}

- (void) reloadTilesWithIndex:(NSUInteger)index
{
    self.whichViewLoaded = index;

    for (int i = 0; i< self.numberOfItems; i++)
    {
        ZLSideMenuTile *tile = [self.tilesArray objectAtIndex:i];
        [tile setNeedsDisplay];
    }
}

- (void) clearAllData
{
    [self reloadTilesWithIndex:0];
}


- (NSString *) selectedValuesForIndex:(NSUInteger)index
{
    return [self.delegate selectedValuesForIndex:index];
}

- (UIImage *) imageForIndex:(NSUInteger)index
{
    return [self.delegate imageForIndex:index];
}

- (NSString *) titleForIndex:(NSUInteger)index
{
    return [self.delegate titleForIndex:index];
}

- (UIImage *) backgroundImage:(NSUInteger)index
{
    return [self.delegate backgroundImage:index];
}

- (UIColor *) lineColor:(NSUInteger)index
{
    if (index == self.whichViewLoaded) {
        return [UIColor colorWithRed:231.0/256 green:136.0/256 blue:66.0/256 alpha:1.0];
    }
    else
    {
        return [UIColor clearColor];
    }
}

- (UIView *) viewForSideTileIndex:(NSUInteger)index tile:(id)tile;
{
    ZLSideMenuTile *sideTile = [self.tilesArray objectAtIndex:index];
    CGRect titleRect ;
    UIFont *titleFont;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, sideTile.frame.size.width, 60)];
    [contentView setBackgroundColor:[UIColor redColor]];
    [contentView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
    
    id result = [self selectedValuesForIndex:self.tag];
    if (!result)
        
    {
        UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((sideTile.frame.size.width - 43)/2, 0, 43,36)];
        
        [logoImageView setImage:[self imageForIndex:self.tag]];
        [contentView addSubview:logoImageView];
        
        titleRect = CGRectMake(0, 39, self.frame.size.width, 21);
        titleFont = [UIFont fontWithName:@"Roboto-Medium" size:14];
    }
    else
    {
        titleRect = CGRectMake(0, 0, self.frame.size.width, 30);
        titleFont = [UIFont fontWithName:@"Roboto-Medium" size:13];
        
        UILabel *resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentView.frame.size.height - 30, self.frame.size.width, 21)];
        [resultLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
        [resultLabel setText:[self selectedValuesForIndex:self.tag]];
        [resultLabel setTextColor:[UIColor whiteColor]];
        [resultLabel setBackgroundColor:[UIColor clearColor]];
        [resultLabel setTextAlignment:NSTextAlignmentCenter];
        [contentView addSubview:resultLabel];
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleRect];
    [titleLabel setText:[self titleForIndex:self.tag]];
    [titleLabel setFont:titleFont];
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [contentView addSubview:titleLabel];
    
    return contentView;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
