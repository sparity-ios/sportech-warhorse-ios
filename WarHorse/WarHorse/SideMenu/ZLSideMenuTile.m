//
//  ZLSideMenuTile.m
//  WarHorse
//
//  Created by Sparity on 08/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLSideMenuTile.h"
#import "ZLSelectedValues.h"
@implementation ZLSideMenuTile

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        tapGesture.numberOfTapsRequired=1;
        [self addGestureRecognizer:tapGesture];
        

    }
    return self;
}

-(void) handleTapGesture:(UITapGestureRecognizer *)sender{
    [self.delegate tapDetectingView:self];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [backgroundImage setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];
    [backgroundImage setImage:[self.delegate backgroundImage:self.tag]];
    [self addSubview:backgroundImage];
    
    UIView *contentView = [self viewForSideTileIndex:self.tag tile:self];
    [contentView setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

    [self addSubview:contentView];
    
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 3, self.frame.size.width, 3)];
    [line setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

    [line setAutoresizingMask:UIViewAutoresizingFlexibleBottomMargin];
    [line setBackgroundColor:[self.delegate lineColor:self.tag]];
    [self addSubview:line];
}

- (UIView *) viewForSideTileIndex:(NSUInteger)index tile:(id)tile
{
    CGRect titleRect ;
    UIFont *titleFont;
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 60)];
    [contentView setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

    [contentView setBackgroundColor:[UIColor clearColor]];
    [contentView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
    
    id result = [self.delegate selectedValuesForIndex:self.tag];
    if (!result)
        
    {
        UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - 43)/2, 0, 43,36)];
        [logoImageView setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

        [logoImageView setImage:[self.delegate imageForIndex:self.tag]];
        [contentView addSubview:logoImageView];
        
        titleRect = CGRectMake(0, 39, self.frame.size.width, 21);
        titleFont = [UIFont fontWithName:@"Roboto-Medium" size:14];
    }
    else
    {
        titleRect = CGRectMake(0, 0, self.frame.size.width, 21);
        titleFont = [UIFont fontWithName:@"Roboto-Medium" size:13];
        
        
        if (WagerTrack == self.tag)
        {
            UILabel *raceLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, contentView.frame.size.height - 30, 30, 30)];
            [raceLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [raceLabel setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

            NSString *raceString = [[ZLSelectedValues sharedInstance] selectedRace];
            if (raceString) {
                [raceLabel setText:[NSString stringWithFormat:@"Race\n %@",raceString]];
            }
            else
            {
                [raceLabel setText:[NSString stringWithFormat:@"Race\n "]];
            }
            [raceLabel setNumberOfLines:2];
            [raceLabel setTextColor:[UIColor whiteColor]];
            [raceLabel setBackgroundColor:[UIColor clearColor]];
            [raceLabel setTextAlignment:NSTextAlignmentCenter];
            [contentView addSubview:raceLabel];
            
            UILabel *MTPLabel = [[UILabel alloc] initWithFrame:CGRectMake(49, contentView.frame.size.height - 30, 28, 15)];
            [MTPLabel setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

            [MTPLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [MTPLabel setText:@"MTP"];
            [MTPLabel setTextColor:[UIColor whiteColor]];
            [MTPLabel setBackgroundColor:[UIColor clearColor]];
            [MTPLabel setTextAlignment:NSTextAlignmentCenter];
            [contentView addSubview:MTPLabel];
            
            UILabel *MTPValue = [[UILabel alloc] initWithFrame:CGRectMake(49, contentView.frame.size.height - 15, 26, 15)];
            [MTPValue setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

            [MTPValue setFont:[UIFont fontWithName:@"Roboto-Medium" size:13]];
            [MTPValue setText:[[ZLSelectedValues sharedInstance] selectedMTP]];
            [MTPValue setTextColor:[UIColor whiteColor]];
            [MTPValue setBackgroundColor:[UIColor redColor]];
            [MTPValue setTextAlignment:NSTextAlignmentCenter];
            [contentView addSubview:MTPValue];
        }
        else
        {
            UILabel *resultLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, contentView.frame.size.height - 30, self.frame.size.width, 21)];
            [resultLabel setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

            [resultLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:18]];
            [resultLabel setText:[self.delegate selectedValuesForIndex:self.tag]];
            [resultLabel setTextColor:[UIColor whiteColor]];
            [resultLabel setBackgroundColor:[UIColor clearColor]];
            [resultLabel setTextAlignment:NSTextAlignmentCenter];
            [contentView addSubview:resultLabel];

        }
        
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleRect];
    [titleLabel setAutoresizingMask:(UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin)];

    [titleLabel setText:[self.delegate titleForIndex:self.tag]];
    [titleLabel setFont:titleFont];
    [titleLabel setMinimumFontSize:10];
    [titleLabel setAdjustsFontSizeToFitWidth:YES];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [contentView addSubview:titleLabel];

    
       
    return contentView;
}


@end
