//
//  ZLResetPasswordViewController.m
//  WarHorse
//
//  Created by Sparity on 8/6/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLResetPasswordViewController.h"

@interface ZLResetPasswordViewController ()

@end

@implementation ZLResetPasswordViewController
@synthesize Pwd_TF=_Pwd_TF;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.Pwd_TF setPlaceholder:@" New Password"];
    [self.Pwd_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.confirmPwd_TF setPlaceholder:@" Confirm Password"];
    [self.confirmPwd_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];

}
-(IBAction)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)submitClicked:(id)sender{
    
}
-(IBAction)BackGroundClicked{
    [self.confirmPwd_TF resignFirstResponder];
    [self.Pwd_TF resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
