//
//  SPPromotionViewController.m
//  WarHorse
//
//  Created by Ramya on 8/20/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "SPPromotionViewController.h"
#import "Communication.h"
#import "ZLMainScreenViewController.h"
#import "Reachability.h"
#import "LeveyHUD.h"
#import "SPApplicationConstants.h"

@interface SPPromotionViewController ()

@end

@implementation SPPromotionViewController
@synthesize tableview =_tableview;
@synthesize customTableViewCell =_customTableViewCell;
@synthesize imageArray = _imageArray;
@synthesize datePicker = _datePicker;
@synthesize actionSheet = _actionSheet;
@synthesize pickerToolBar = _pickerToolBar;
@synthesize headerImageView = _headerImageView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
       _imageArray = [[NSMutableArray alloc]init];
       UILabel *promotionLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 18, 100, 12)];
       [promotionLabel setBackgroundColor:[UIColor clearColor]];
       [promotionLabel setText:@"Promotions"];
       [promotionLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
       [promotionLabel setTextColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f   blue:255.0f/255.0f alpha:1.0]];
      [self.headerImageView addSubview:promotionLabel];
     Reachability *reachability = [Reachability reachabilityForInternetConnection] ;
     NetworkStatus netWorkStatus = [reachability currentReachabilityStatus] ;
     if (netWorkStatus == ReachableViaWWAN || netWorkStatus == ReachableViaWiFi)
    {
        [[LeveyHUD sharedHUD] appearWithText:@"Please Wait...."];
    
        Communication *objCommunication = [[Communication alloc] init];
        objCommunication.delegate = self;
       
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://dev.sparity.com/warhorse/promos.php"]];
        [objCommunication makeAsynchronousRequestWithUrl:url withBodyString:nil andWithMethod:@"get"];
   objCommunication = nil;
    }    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:Aleart_Title message:@"The Internet connection appears to be offline" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
            
        }
    
}

- (IBAction)toggleOnClick:(id)sender {
  
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)calenderOnClick:(id)sender {
   
    _actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select time" delegate:nil cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    _pickerToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.pickerToolBar.barStyle = UIBarStyleBlackOpaque;
    [self.pickerToolBar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    UIBarButtonItem *cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datecancelbuttonClicked:)];
    [barItems addObject:cancelBtn];
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donebuttonClicked:)];
    [barItems addObject:flexSpace];
    [barItems addObject:doneBtn];
    [self.pickerToolBar setItems:barItems animated:YES];
    
    if(_datePicker)
    {
        _datePicker=nil;
    }
    _datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 44, 320, 260)];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:0];
    [self.actionSheet addSubview:self.pickerToolBar];
    [self.actionSheet addSubview:self.datePicker];
    [self.actionSheet showInView:self.view];
    [self.actionSheet setBounds:CGRectMake(0,0,320, 464)];
   
    
}
-(void)donebuttonClicked:(id) sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSString *str = [dateFormatter stringFromDate:self.datePicker.date];
    NSLog(@"str %@", str);
    [self.actionSheet dismissWithClickedButtonIndex:2 animated:YES];
}
-(void)datecancelbuttonClicked:(id) sender
{
    [self.actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)responseData:(id)data andWithServiceName:(NSString *)serviceName
{
    
    [self.imageArray addObjectsFromArray:[data valueForKey:@"url"]];
    [self.tableview reloadData];
    [[LeveyHUD sharedHUD]disappear];

       NSLog(@"imagearray %@",self.imageArray);
}
-(void)failedToGetDataWithError:(NSString *)error andWithServiceName:(NSString *)serviceName
{
    [[LeveyHUD sharedHUD] delayDisappear:0.2f withText:@"Please Wait...."];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    // Return the number of rows in the section.
    
    return [self.imageArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.customTableViewCell  = (SPPromotionTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.customTableViewCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"SPPromotionTableCell" owner:self options:nil];
    }
   
    NSString* imageURL =[NSString stringWithFormat:@"%@",[self.imageArray  objectAtIndex:indexPath.row]];
    
    NSURL *URL=[NSURL URLWithString:imageURL];
    
    [self.customTableViewCell.promotionImage loadImageFromURL:URL];
    return self.customTableViewCell;
}
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 100;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setTableview:nil];
    [self setCustomTableViewCell:nil];
    [self setHeaderImageView:nil];
    [super viewDidUnload];
}

@end
