//
//  ZLAddCardsViewController.h
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLAddCardsCustomCell.h"

@interface ZLAddCardsViewController : UIViewController
@property(nonatomic,retain) IBOutlet UITableView *cardsTableView;
@property(nonatomic,retain) IBOutlet ZLAddCardsCustomCell *addCardsCustomCell;
@property(nonatomic,retain) UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;
@property(nonatomic,retain) IBOutlet UIButton *backButton;
@property(nonatomic,retain) IBOutlet UILabel *cardLabel;
@property(nonatomic,retain) IBOutlet UIButton *addCardBtn;
@property(nonatomic,retain) IBOutlet UIButton *updateBtn;

@property(nonatomic,retain) NSMutableArray *cardsArray;

-(IBAction)addCardClicked:(id)sender;
-(IBAction)updateCardClicked:(id)sender;
- (IBAction)wagerButtonClicked:(id)sender;
-(IBAction)backClicked:(id)sender;
@end
