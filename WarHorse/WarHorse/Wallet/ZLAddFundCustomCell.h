//
//  ZLAddFundCustomCell.h
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLAddFundCustomCell : UITableViewCell
@property(nonatomic,retain) IBOutlet UIView *cardsView;
@property(nonatomic,retain) IBOutlet UIImageView *cardImageView;
@property(nonatomic,retain) IBOutlet UILabel *cardNameLAbel;
@property(nonatomic,retain) IBOutlet UILabel *accountNumLabel;
@property(nonatomic,retain) IBOutlet UILabel *dateLabel;

@end
