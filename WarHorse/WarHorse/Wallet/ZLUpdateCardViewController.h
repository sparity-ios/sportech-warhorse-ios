//
//  ZLUpdateCardViewController.h
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLUpdateCardCustomCell.h"

@interface ZLUpdateCardViewController : UIViewController
@property(nonatomic,retain) IBOutlet UITableView *updateTableView;
@property(nonatomic,retain) IBOutlet ZLUpdateCardCustomCell *updateCardCustomCell;
@property(nonatomic,retain) NSMutableArray *cardsArray;
@property(nonatomic,retain) UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;
@property(nonatomic,retain) IBOutlet UIButton *backButton;
@property(nonatomic,retain) IBOutlet UILabel *updateLabel;
-(IBAction)backClicked:(id)sender;
- (IBAction)wagerButtonClicked:(id)sender;


@end
