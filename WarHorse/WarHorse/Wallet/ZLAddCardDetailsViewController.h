//
//  ZLAddCardDetailsViewController.h
//  WarHorse
//
//  Created by Sparity on 8/9/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLAddCardDetailsViewController : UIViewController<UITextFieldDelegate>


@property(nonatomic,retain) IBOutlet UILabel *cardNum_Label;
@property(nonatomic,retain) IBOutlet UILabel *expires_Label;
@property(nonatomic,retain) IBOutlet UILabel *name_Label;
@property(nonatomic,retain) IBOutlet UILabel *cardCode_Label;

@property(nonatomic,retain) IBOutlet UITextField *cardNumber_TF;
@property(nonatomic,retain) IBOutlet UITextField *month_TF;
@property(nonatomic,retain) IBOutlet UITextField *year_TF;
@property(nonatomic,retain) IBOutlet UITextField *name_TF;
@property(nonatomic,retain) IBOutlet UITextField *code_TF;
@property(nonatomic,retain) IBOutlet UIButton *addCardBtn;
@property(nonatomic,retain) IBOutlet UIButton *helpBtn;
@property(nonatomic,retain) IBOutlet UIButton *backBtn;

@property(nonatomic,retain) UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;
@property	CGFloat shiftForKeyboard;
-(IBAction)addCardClicked:(id)sender;
-(IBAction)helpClicked:(id)sender;
-(IBAction)backClicked:(id)sender;
-(IBAction)backGroundClicked:(id)sender;
@end
