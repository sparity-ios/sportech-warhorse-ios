//
//  ZLAddFundsViewController.h
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLAddFundCustomCell.h"

@interface ZLAddFundsViewController : UIViewController
@property(nonatomic,retain) IBOutlet UITableView *fundsTableView;
@property(nonatomic,retain) IBOutlet ZLAddFundCustomCell *addFundCustomCell;
@property(nonatomic,retain) NSMutableArray *cardsArray;
@property(nonatomic,retain) UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;
@property(nonatomic,retain) IBOutlet UIButton *backButton;
@property(nonatomic,retain) IBOutlet UITextField *amount_TF;
@property(nonatomic,retain) IBOutlet UIButton *continueBtn;
@property(nonatomic,retain) IBOutlet UILabel *addFundLabel;
@property(nonatomic,retain) IBOutlet UILabel *amountLabel;
@property(nonatomic,retain) IBOutlet UILabel *selectLabel;

//popUPView
@property(nonatomic,retain) IBOutlet UIView *transparantView;
@property(nonatomic,retain) IBOutlet UIView *fundsView;
@property(nonatomic,retain) IBOutlet UIButton *fundCloseBtn;
@property(nonatomic,retain) IBOutlet UIView *insideFundsView;
@property(nonatomic,retain) IBOutlet UIButton *confirmBtn;
@property(nonatomic,retain) IBOutlet UIButton *cancelBtn;
@property(nonatomic,retain) IBOutlet UILabel *adngFunds_Label;
@property(nonatomic,retain) IBOutlet UILabel *amountLable;
@property(nonatomic,retain) IBOutlet UILabel *cardLabel;
@property(nonatomic,retain) IBOutlet UILabel *fundAmountLabel;
@property(nonatomic,retain) IBOutlet UILabel *cardNameLabel;
@property(nonatomic,retain) IBOutlet UILabel *accountNumLabel;



-(IBAction)fundCloseClicked:(id)sender;
-(IBAction)continueClicked:(id)sender;
- (IBAction)wagerButtonClicked:(id)sender;
-(IBAction)backClicked:(id)sender;
-(IBAction)confirmClicked:(id)sender;
-(IBAction)cancelClicked:(id)sender;


@end
