//
//  ZLUpdateCardCustomCell.m
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLUpdateCardCustomCell.h"

@implementation ZLUpdateCardCustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
