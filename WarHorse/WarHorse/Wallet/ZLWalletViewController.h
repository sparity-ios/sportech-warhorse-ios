//
//  ZLWalletViewController.h
//  WarHorse
//
//  Created by Sparity on 18/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZLWalletCustomCell.h"
#import "ZUUIRevealController.h"

@interface ZLWalletViewController : UIViewController<ZUUIRevealControllerDelegate>

@property(nonatomic,retain) UIButton *amountButton;
@property(nonatomic,retain) IBOutlet UIButton *wager_Button;

@property(nonatomic,retain) IBOutlet UIView *currentBalanceView;
@property(nonatomic,retain) IBOutlet UIView *cardsView;
@property(nonatomic,retain) IBOutlet UITableView *walletTableView;
@property(nonatomic,retain) IBOutlet ZLWalletCustomCell *walletCustomCell;
@property(nonatomic,retain) IBOutlet UILabel *balanceLabel;
@property(nonatomic,retain) IBOutlet UILabel *amountLabel;
@property(nonatomic,retain) IBOutlet UIButton *AddFundsBtn;
@property(nonatomic,retain) IBOutlet UIButton *withDrawalBtn;
@property(nonatomic,retain) IBOutlet UIButton *addCardsBtn;
@property(nonatomic,retain) IBOutlet UIButton *updateBtn;
@property(nonatomic,retain) IBOutlet UILabel *accountActivityLabel;
@property(nonatomic,retain) IBOutlet UIButton *viewBtn;
@property(nonatomic,retain) NSMutableArray *walletArray;


-(IBAction)viewClicked:(id)sender;
-(IBAction)addFundsClicked:(id)sender;
-(IBAction)withDrawalClicked:(id)sender;
-(IBAction)addCardsClicked:(id)sender;
-(IBAction)updateCardsClicked:(id)sender;
-(IBAction)amountButtonClicked:(id)sender;
- (IBAction)wagerButtonClicked:(id)sender;

@end
