//
//  ZLAddFundsViewController.m
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLAddFundsViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ZLAddFundsViewController ()

@end

@implementation ZLAddFundsViewController
@synthesize cardsArray=_cardsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _cardsArray=[[NSMutableArray alloc]init];
    [self loadCardsData];
    [self prepareTopView];
    
    [self.continueBtn setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.continueBtn setTitle:@"CONTINUE" forState:UIControlStateNormal];
    [self.continueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.continueBtn.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.addFundLabel setText:@"Add funds to your Account"];
    [self.addFundLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [self.addFundLabel setTextColor:[UIColor blackColor]];
    [self.addFundLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.selectLabel setText:@"Select one"];
    [self.selectLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:15]];
    [self.selectLabel setTextColor:[UIColor blackColor]];
    [self.selectLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.amountLabel setText:@"Amount"];
    [self.amountLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:15]];
    [self.amountLabel setTextColor:[UIColor blackColor]];
    [self.amountLabel setBackgroundColor:[UIColor clearColor]];




}

-(void)loadCardsData
{
    
    NSMutableDictionary *cardDict=[NSMutableDictionary dictionary];
    [cardDict setValue:@"Green Dot Credit Card" forKey:@"cardName"];
    [cardDict setValue:@"xxxx-xxxx-xxxx-1234" forKey:@"accountNum"];
    [cardDict setValue:@"Expiry 09/17" forKey:@"ExpDate"];
    [cardDict setObject:[UIColor whiteColor] forKey:@"textColor"];
    [cardDict setObject:[UIColor colorWithRed:4.0/255.0f green:157.0/255.0f blue:75.0/255.0f alpha:1.0] forKey:@"viewColor"];
    [self.cardsArray addObject:cardDict];
    
    NSMutableDictionary *cardDict1=[NSMutableDictionary dictionary];
    [cardDict1 setValue:@"VISA Credit Card" forKey:@"cardName"];
    [cardDict1 setValue:@"xxxx-xxxx-xxxx-7654" forKey:@"accountNum"];
    [cardDict1 setValue:@"Expiry 12/19" forKey:@"ExpDate"];
    [cardDict1 setObject:[UIColor whiteColor] forKey:@"textColor"];
    
    [cardDict1 setObject:[UIColor colorWithRed:0.0/255.0f green:74.0/255.0f blue:145.0/255.0f alpha:1.0] forKey:@"viewColor"];
    
    [self.cardsArray addObject:cardDict1];
    
    NSMutableDictionary *cardDict2=[NSMutableDictionary dictionary];
    [cardDict2 setValue:@"Master Card" forKey:@"cardName"];
    [cardDict2 setValue:@"xxxx-xxxx-xxxx-1234" forKey:@"accountNum"];
    [cardDict2 setValue:@"Expiry 11/18" forKey:@"ExpDate"];
    [cardDict2 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [cardDict2 setObject:[UIColor colorWithRed:239.0/255.0f green:137.0/255.0f blue:0.0/255.0f alpha:1.0] forKey:@"viewColor"];
    
    [self.cardsArray addObject:cardDict2];
}

- (void) prepareTopView
{
    
    UIButton *backButton = [[UIButton alloc] init];
    [backButton setFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"toggle.png"] forState:UIControlStateNormal];
    [backButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton = nil;
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(45, 11, 100, 21)];
    [title setText:@"Wallet"];
    [title setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [title setTextColor:[UIColor whiteColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:title];
    title=nil;
    
    
    self.amountButton = [[UIButton alloc] initWithFrame:CGRectMake(245, 0, 44, 46)];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"symbol.png"] forState:UIControlStateNormal];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"balancebg.png"] forState:UIControlStateSelected];
    [self.amountButton setTitle:@"" forState:UIControlStateNormal];
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.amountButton.titleLabel setLineBreakMode:NSLineBreakByCharWrapping];
    [self.amountButton addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.amountButton];
    
}

- (void)amountButtonClicked:(id)sender
{
    CGRect rect = ((UIButton *)sender).frame;
    
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        
        rect.origin.x += 30;
        rect.size.width -= 30;
        [self.amountButton setFrame:rect];
        
    }
    else{
        [self.amountButton setSelected:YES];
        
        rect.origin.x -= 30;
        rect.size.width += 30;
        [self.amountButton setFrame:rect];
        //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}
- (IBAction)wagerButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
}

-(IBAction)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)continueClicked:(id)sender
{
    [self.amountLable setText:@"Amount"];
    [self.cardLabel setText:@"Card"];
    [self.view addSubview:self.transparantView];
}
-(IBAction)fundCloseClicked:(id)sender
{
      [self.transparantView removeFromSuperview];
}
-(IBAction)confirmClicked:(id)sender
{
  
}
-(IBAction)cancelClicked:(id)sender
{
    [self.transparantView removeFromSuperview];

}

#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.cardsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.addFundCustomCell  = (ZLAddFundCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.addFundCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLAddFundCustomCell" owner:self options:nil];
        
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.addFundCustomCell.cardsView.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.addFundCustomCell.cardsView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.addFundCustomCell.cardsView.layer.mask = maskLayer;
    
    [self.addFundCustomCell.cardNameLAbel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.addFundCustomCell.accountNumLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.addFundCustomCell.dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    
    NSMutableDictionary *dict=[self.cardsArray objectAtIndex:indexPath.row];
    [self.addFundCustomCell.cardsView setBackgroundColor:[dict valueForKey:@"viewColor"]];
    [self.addFundCustomCell.cardNameLAbel setTextColor:[dict valueForKey:@"textColor"]];
    [self.addFundCustomCell.accountNumLabel setTextColor:[dict valueForKey:@"textColor"]];
    [self.addFundCustomCell.dateLabel setTextColor:[dict valueForKey:@"textColor"]];
    
    self.addFundCustomCell.cardNameLAbel.text=[dict valueForKey:@"cardName"];
    self.addFundCustomCell.accountNumLabel.text=[dict valueForKey:@"accountNum"];
    self.addFundCustomCell.dateLabel.text=[dict valueForKey:@"ExpDate"];
    
    
    
    
    
    return self.addFundCustomCell;
}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 62;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
      self.addFundCustomCell = (ZLAddFundCustomCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    
}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
