//
//  ZLAddCardsViewController.m
//  WarHorse
//
//  Created by Sparity on 8/8/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLAddCardsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ZLAddCardDetailsViewController.h"
@interface ZLAddCardsViewController ()

@end

@implementation ZLAddCardsViewController
@synthesize cardsTableView=_cardsTableView;
@synthesize cardsArray=_cardsArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _cardsArray=[[NSMutableArray alloc]init];
    
    [self.addCardBtn setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.addCardBtn setTitle:@"ADD CARDS" forState:UIControlStateNormal];
    [self.addCardBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addCardBtn.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.updateBtn setBackgroundColor:[UIColor colorWithRed:47.0/255.0f green:58.0/255.0f blue:65.0/255.0f alpha:1.0]];
    [self.updateBtn setTitle:@"UPDATE CARDS" forState:UIControlStateNormal];
    [self.updateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.updateBtn.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    
    [self loadCardsData];
    
    [self prepareTopView];
}

- (void) prepareTopView
{
    
    UIButton *backButton = [[UIButton alloc] init];
    [backButton setFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"toggle.png"] forState:UIControlStateNormal];
    [backButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton = nil;
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(45, 11, 100, 21)];
    [title setText:@"Wallet"];
    [title setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [title setTextColor:[UIColor whiteColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:title];
    title=nil;
    
    
    self.amountButton = [[UIButton alloc] initWithFrame:CGRectMake(245, 0, 44, 46)];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"symbol.png"] forState:UIControlStateNormal];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"balancebg.png"] forState:UIControlStateSelected];
    [self.amountButton setTitle:@"" forState:UIControlStateNormal];
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.amountButton.titleLabel setLineBreakMode:NSLineBreakByCharWrapping];
    [self.amountButton addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.amountButton];
    
}

- (void)amountButtonClicked:(id)sender
{
    CGRect rect = ((UIButton *)sender).frame;
    
    if ([self.amountButton isSelected])
    {
        [self.amountButton setSelected:NO];
        
        rect.origin.x += 30;
        rect.size.width -= 30;
        [self.amountButton setFrame:rect];
        
    }
    else{
        [self.amountButton setSelected:YES];
        
        rect.origin.x -= 30;
        rect.size.width += 30;
        [self.amountButton setFrame:rect];
        //[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}
- (IBAction)wagerButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
}




-(void)loadCardsData{
    
    NSMutableDictionary *cardDict=[NSMutableDictionary dictionary];
    [cardDict setValue:@"Green Dot Credit Card" forKey:@"cardName"];
    [cardDict setValue:@"xxxx-xxxx-xxxx-1234" forKey:@"accountNum"];
    [cardDict setValue:@"Expiry 09/17" forKey:@"ExpDate"];
    [cardDict setObject:[UIColor whiteColor] forKey:@"textColor"];
    [cardDict setObject:[UIColor colorWithRed:4.0/255.0f green:157.0/255.0f blue:75.0/255.0f alpha:1.0] forKey:@"viewColor"];
    [self.cardsArray addObject:cardDict];
    
    NSMutableDictionary *cardDict1=[NSMutableDictionary dictionary];
    [cardDict1 setValue:@"VISA Credit Card" forKey:@"cardName"];
    [cardDict1 setValue:@"xxxx-xxxx-xxxx-7654" forKey:@"accountNum"];
    [cardDict1 setValue:@"Expiry 12/19" forKey:@"ExpDate"];
    [cardDict1 setObject:[UIColor whiteColor] forKey:@"textColor"];

    [cardDict1 setObject:[UIColor colorWithRed:0.0/255.0f green:74.0/255.0f blue:145.0/255.0f alpha:1.0] forKey:@"viewColor"];

    [self.cardsArray addObject:cardDict1];
    
    NSMutableDictionary *cardDict2=[NSMutableDictionary dictionary];
    [cardDict2 setValue:@"Master Card" forKey:@"cardName"];
    [cardDict2 setValue:@"xxxx-xxxx-xxxx-1234" forKey:@"accountNum"];
    [cardDict2 setValue:@"Expiry 11/18" forKey:@"ExpDate"];
    [cardDict2 setObject:[UIColor whiteColor] forKey:@"textColor"];
    [cardDict2 setObject:[UIColor colorWithRed:239.0/255.0f green:137.0/255.0f blue:0.0/255.0f alpha:1.0] forKey:@"viewColor"];

    [self.cardsArray addObject:cardDict2];
}

-(IBAction)backClicked:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)addCardClicked:(id)sender{
    ZLAddCardDetailsViewController *addCardDetailViewController=[[ZLAddCardDetailsViewController alloc]init];
    [self.navigationController pushViewController:addCardDetailViewController animated:YES];
}
-(IBAction)updateCardClicked:(id)sender{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.cardsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.addCardsCustomCell  = (ZLAddCardsCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.addCardsCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLAddCardsCustomCell" owner:self options:nil];
        
    }
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.addCardsCustomCell.cardsView.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight                                                         cornerRadii:CGSizeMake(5.0, 5.0)];
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.addCardsCustomCell.cardsView.bounds;
    maskLayer.path = maskPath.CGPath;
    self.addCardsCustomCell.cardsView.layer.mask = maskLayer;
    
     [self.addCardsCustomCell.cardNameLAbel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
      [self.addCardsCustomCell.accountNumLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.addCardsCustomCell.dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    
    NSMutableDictionary *dict=[self.cardsArray objectAtIndex:indexPath.row];
    [self.addCardsCustomCell.cardsView setBackgroundColor:[dict valueForKey:@"viewColor"]];
    [self.addCardsCustomCell.cardNameLAbel setTextColor:[dict valueForKey:@"textColor"]];
    [self.addCardsCustomCell.accountNumLabel setTextColor:[dict valueForKey:@"textColor"]];
    [self.addCardsCustomCell.dateLabel setTextColor:[dict valueForKey:@"textColor"]];

    self.addCardsCustomCell.cardNameLAbel.text=[dict valueForKey:@"cardName"];
    self.addCardsCustomCell.accountNumLabel.text=[dict valueForKey:@"accountNum"];
    self.addCardsCustomCell.dateLabel.text=[dict valueForKey:@"ExpDate"];
    
    
    
    
    
    return self.addCardsCustomCell;
}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 62;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
