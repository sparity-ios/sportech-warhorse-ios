//
//  ZLWalletViewController.m
//  WarHorse
//
//  Created by Sparity on 18/07/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLWalletViewController.h"
#import "ZLAddCardsViewController.h"
#import "ZLAddFundsViewController.h"
#import "ZLUpdateCardViewController.h"
#import "ZUUIRevealController.h"

@interface ZLWalletViewController ()

@end

@implementation ZLWalletViewController
@synthesize walletArray=_walletArray;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    self.navigationController.navigationBarHidden = YES;
    [self prepareTopView];
    
    _walletArray=[[NSMutableArray alloc]init];
    [self loadWalletTableData];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
//    tap.delegate = (id<UIGestureRecognizerDelegate>)self;
//    [self.view addGestureRecognizer:tap];
////
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealToggle:) ];
    tap.delegate = (id<UIGestureRecognizerDelegate>)self;
    [self.view addGestureRecognizer:tap];
   // [tap setEnabled:NO];
   // _tap = tap;

    
}
//- (void)tap:(UITapGestureRecognizer*)gesture {
//    
////    [gesture setEnabled:NO];
////    [self showRootController:YES];
//    
//    ZUUIRevealController *promotionPopUpView = [[ZUUIRevealController alloc]init];
//    promotionPopUpView.delegate=self;
//    [promotionPopUpView revealToggle:gesture];
//   // [promotionPopUpView ];
//   // [self.navigationController.parentViewController action:@selector(revealToggle:) ];
//
//    
//}

//- (void)revealToggle:(UITapGestureRecognizer*)gesture {
//    NSLog(@"tap");
//   // [gesture setEnabled:NO];
//       
//      }
-(void)loadWalletTableData{
    
    NSMutableDictionary *walletDict=[NSMutableDictionary dictionary];
    [walletDict setValue:@"23JUL\n2013" forKey:@"Date"];
    [walletDict setValue:@"CASH" forKey:@"cashKey"];
    [walletDict setObject:[UIColor blueColor] forKey:@"cashColor"];
    [walletDict setObject:[UIColor whiteColor] forKey:@"cashTextClr"];
    [walletDict setValue:@"Cash Successfully \n deposited" forKey:@"titleLabel"];
    [walletDict setValue:@"$200.00" forKey:@"amount"];
    [walletDict setObject:[UIColor greenColor] forKey:@"amountColor"];
    [self.walletArray addObject:walletDict];
    
    NSMutableDictionary *walletDict1=[NSMutableDictionary dictionary];
    [walletDict1 setValue:@"22JUL\n2013" forKey:@"Date"];
    [walletDict1 setValue:@"BETS" forKey:@"cashKey"];
    [walletDict1 setObject:[UIColor whiteColor] forKey:@"cashTextClr"];
    [walletDict1 setObject:[UIColor purpleColor] forKey:@"cashColor"];

    [walletDict1 setValue:@"Arlington Park" forKey:@"titleLabel"];
    [walletDict1 setValue:@"Race 7 - WIN - $5.00" forKey:@"detailText"];
    [walletDict1 setValue:@"-$5.00" forKey:@"amount"];
     [walletDict1 setObject:[UIColor redColor] forKey:@"amountColor"];
    [self.walletArray addObject:walletDict1];
    
    
    NSMutableDictionary *walletDict2=[NSMutableDictionary dictionary];
    [walletDict2 setValue:@"21JUL\n2013" forKey:@"Date"];
    [walletDict2 setValue:@"BETS" forKey:@"cashKey"];
    [walletDict2 setObject:[UIColor purpleColor] forKey:@"cashColor"];
    [walletDict2 setObject:[UIColor whiteColor] forKey:@"cashTextClr"];
    [walletDict2 setValue:@"Louisiana Downs" forKey:@"titleLabel"];
    [walletDict2 setValue:@"Race 3 - PICK 5 - $100.00" forKey:@"detailText"];
    [walletDict2 setValue:@"$1,500.00" forKey:@"amount"];
    [walletDict2 setObject:[UIColor redColor] forKey:@"amountColor"];
    [self.walletArray addObject:walletDict2];


    NSMutableDictionary *walletDict3=[NSMutableDictionary dictionary];
    [walletDict3 setValue:@"21JUL\n2013" forKey:@"Date"];
    [walletDict3 setValue:@"BETS" forKey:@"cashKey"];
    [walletDict3 setObject:[UIColor purpleColor] forKey:@"cashColor"];
    [walletDict3 setObject:[UIColor whiteColor] forKey:@"cashTextClr"];
    [walletDict3 setValue:@"Del Mar" forKey:@"titleLabel"];
    [walletDict3 setValue:@"Race 2 - WIN  - $30.00" forKey:@"detailText"];
    [walletDict3 setValue:@"-$30.00" forKey:@"amount"];
    [walletDict3 setObject:[UIColor redColor] forKey:@"amountColor"];
    [self.walletArray addObject:walletDict3];

    
    
    
}

- (IBAction)wagerButtonClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadView" object:self userInfo:[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:DashBoardWager]forKey:@"viewNumber"]];
}


- (void) prepareTopView
{
    
    UIButton *backButton = [[UIButton alloc] init];
    [backButton setFrame:CGRectMake(0, 0, 44, 44)];
    [backButton setImage:[UIImage imageNamed:@"toggle.png"] forState:UIControlStateNormal];
    [backButton addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton = nil;
    
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(45, 11, 100, 21)];
    [title setText:@"Wallet"];
    [title setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    [title setTextColor:[UIColor whiteColor]];
    [title setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:title];
    title=nil;
    
    
    self.amountButton = [[UIButton alloc] initWithFrame:CGRectMake(245, 0, 44, 46)];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"symbol.png"] forState:UIControlStateNormal];
    [self.amountButton setBackgroundImage:[UIImage imageNamed:@"balancebg.png"] forState:UIControlStateSelected];
    [self.amountButton setTitle:@"" forState:UIControlStateNormal];
    [self.amountButton setTitle:@"BALANCE \n $999.00" forState:UIControlStateSelected];
    [self.amountButton.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    [self.amountButton.titleLabel setLineBreakMode:NSLineBreakByCharWrapping];
    [self.amountButton addTarget:self action:@selector(amountButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.amountButton];
    
}

- (void)amountButtonClicked:(id)sender
{
    if ([self.amountButton isSelected]) {
        [self.amountButton setSelected:NO];
        [self.amountButton setFrame:CGRectMake(245, 1, 44, 44)];
        
    }
    else{
        [self.amountButton setSelected:YES];
        [self.amountButton setFrame:CGRectMake(200, 1, 71, 44)];
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerMethod:) userInfo:nil repeats:NO];
    }
}

- (void)timerMethod:(NSTimer *)timer
{
    if ([self.amountButton isSelected]) {
        [self.amountButton setSelected:NO];
        [self.amountButton setFrame:CGRectMake(245, 1, 44, 44)];
    }
}


-(IBAction)viewClicked:(id)sender{
    
}

-(IBAction)addFundsClicked:(id)sender
{
    ZLAddFundsViewController *addFundsViewController=[[ZLAddFundsViewController alloc]init];
    [self.navigationController pushViewController:addFundsViewController animated:YES];
    
}
-(IBAction)withDrawalClicked:(id)sender
{
    
}
-(IBAction)addCardsClicked:(id)sender
{
    ZLAddCardsViewController *cardsViewController=[[ZLAddCardsViewController alloc]init];
    [self.navigationController pushViewController:cardsViewController animated:YES];
    
}
-(IBAction)updateCardsClicked:(id)sender
{
    ZLUpdateCardViewController *updateViewController=[[ZLUpdateCardViewController alloc]init];
    [self.navigationController pushViewController:updateViewController animated:YES];
}


#pragma mark -
#pragma mark UITableViewDelegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    
    return [self.walletArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    self.walletCustomCell  = (ZLWalletCustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (self.walletCustomCell == nil)
    {
        [[NSBundle mainBundle] loadNibNamed:@"ZLWalletCustomCell" owner:self options:nil];
        
    }
    
     // [self.walletCustomCell.contentView setBackgroundColor:[UIColor colorWithRed:222.0/255.0 green:222.0/255.0 blue:222.0/255.0 alpha:1.0]];
    [self.walletCustomCell.detailLabel setTextColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
    [self.walletCustomCell.dateLabel setTextColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];


    
    [self.walletCustomCell.dateLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
      [self.walletCustomCell.cashLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.walletCustomCell.titleLable setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
     [self.walletCustomCell.detailLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.walletCustomCell.amountLable setFont:[UIFont fontWithName:@"Roboto-Medium" size:12]];
    [self.walletCustomCell.titleLable setNumberOfLines:2];
    [self.walletCustomCell.dateLabel setNumberOfLines:2];

    
    NSMutableDictionary *dic = [self.walletArray objectAtIndex:indexPath.row];
    if ([[dic valueForKey:@"cashKey"] isEqualToString:@"CASH"]) {
        [self.walletCustomCell.titleLable setFrame:CGRectMake(96, 4, 163, 42)];
        [self.walletCustomCell.titleLable setNumberOfLines:2];
    }
    [self.walletCustomCell.dateLabel setText:[dic valueForKey:@"Date"]];
    [self.walletCustomCell.cashLabel setText:[dic valueForKey:@"cashKey"]];
    [self.walletCustomCell.cashLabel setTextColor:[dic valueForKey:@"cashTextClr"]];
    [self.walletCustomCell.cashLabel setBackgroundColor:[dic valueForKey:@"cashColor"]];
    [self.walletCustomCell.titleLable setText:[dic valueForKey:@"titleLabel"]];
    [self.walletCustomCell.detailLabel setText:[dic valueForKey:@"detailText"]];
    [self.walletCustomCell.amountLable setText:[dic valueForKey:@"amount"]];
    [self.walletCustomCell.amountLable setTextColor:[dic valueForKey:@"amountColor"]];
    
    
    
//    
//    [self.mainTableCustomCell.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
//    [self.mainTableCustomCell.titleLabel setTextColor:[UIColor colorWithRed:30.0/255.0f green:30.0/255.0f blue:30.0f/255.0f alpha:1.0]];
//    self.mainTableCustomCell.titleLabel.text=[dic valueForKey:@"title"];
//    self.mainTableCustomCell.iconImages.image=[UIImage imageNamed:[dic valueForKey:@"iconImages"]];
    
    return self.walletCustomCell;
}





- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       
}







-(void)viewDidUnload{
    self.amountButton=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
