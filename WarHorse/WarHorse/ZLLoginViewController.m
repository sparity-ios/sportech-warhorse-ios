//
//  ZLLoginViewController.m
//  WarHorse
//
//  Created by Sparity on 7/4/13.
//  Copyright (c) 2013 Sparity. All rights reserved.
//

#import "ZLLoginViewController.h"
#import "ZLMainGridViewController.h"
#import "ZLResetPasswordViewController.h"
@interface ZLLoginViewController ()

@end

@implementation ZLLoginViewController
@synthesize login_Label=_login_Label;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.userName_TF setPlaceholder:@" User name"];
    [self.userName_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.password_TF setPlaceholder:@" Password"];
    [self.password_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.pinNumber_TF setPlaceholder:@" Enter 4 digit PIN"];
    [self.pinNumber_TF setFont:[UIFont fontWithName:@"Roboto-Medium" size:15]];
    
    [self.login_Label setText:@"      Login to existing account"];
    [self.login_Label setTextColor:[UIColor colorWithRed:255.0/255.0f green:255.0/255.0f blue:255.0f/255.0f alpha:1.0]];
    [self.login_Label setFont:[UIFont fontWithName:@"Roboto-Medium" size:20]];
}

-(IBAction)login_Clicked:(id)sender{
    ZLMainGridViewController *mainGridViewController=[[ZLMainGridViewController alloc]init];
    [self.navigationController pushViewController:mainGridViewController animated:YES];
    mainGridViewController=nil;
    
}
-(IBAction)fogotPWD_Clicked:(id)sender{
    ZLResetPasswordViewController *resetViewController=[[ZLResetPasswordViewController alloc]init];
    [self.navigationController pushViewController:resetViewController animated:YES];
}

-(IBAction)back_Clicked:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)BackGroundClicked{
    [self.userName_TF resignFirstResponder];
    [self.password_TF resignFirstResponder];
    [self.pinNumber_TF resignFirstResponder];
    
}



#pragma mark-
#pragma mark- TextField Delegate Method.
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
	CGRect textViewRect = [self.view.window convertRect:textField.bounds fromView:textField];
	CGFloat bottomEdge = textViewRect.origin.y + textViewRect.size.height;
	if (bottomEdge >= 250) {//250
        CGRect viewFrame = self.view.frame;
        self.shiftForKeyboard = bottomEdge - 200;
        viewFrame.origin.y -= self.shiftForKeyboard;
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationBeginsFromCurrentState:YES];
		[UIView setAnimationDuration:0.3];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
		
	} else {
		self.shiftForKeyboard = 0.0f;
	}
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if (self.view.frame.origin.y == 0) {
        [textField resignFirstResponder];
    }
    else{
     	// Resign first responder
        [textField resignFirstResponder];
        
        
        // Make a CGRect for the view (which should be positioned at 0,0 and be 320px wide and 480px tall)
        CGRect viewFrame = self.view.frame;
        if(viewFrame.origin.y!=0)
        {
            // Adjust the origin back for the viewFrame CGRect
            viewFrame.origin.y += self.shiftForKeyboard;
            // Set the shift value back to zero
            self.shiftForKeyboard = 0.0f;
            
            // As above, the following animation setup just makes it look nice when shifting
            // Again, we don't really need the animation code, but we'll leave it in here
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationDuration:0.3];
            // Apply the new shifted vewFrame to the view
            [self.view setFrame:viewFrame];
            // More animation code
            [UIView commitAnimations];
        }
    }
}


-(void)viewDidUnload{
    self.userName_TF=nil;
    self.password_TF=nil;
    self.pinNumber_TF=nil;
    self.login_Button=nil;
    self.login_Label=nil;
    self.forgotPWD_Button=nil;
    self.back_Button=nil;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
